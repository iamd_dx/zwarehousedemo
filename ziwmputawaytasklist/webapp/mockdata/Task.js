sap.ui.define([],

    function () {

        var data = [];
        // DC1
        data.push({
            Warehouse : "DC1",
            Area : "A",
            TaskNo : "000001",
            TaskGroup : "",
            GroupSeq : "",
            TargetPallet : "FK9871",
            TargetBin : "A001"
        });
        data.push({
            Warehouse : "DC1",
            Area : "A",
            TaskNo : "000002",
            TaskGroup : "",
            GroupSeq : "",
            TargetPallet : "FK9872",
            TargetBin : "A002"
        });
        data.push({
            Warehouse : "DC1",
            Area : "B",
            TaskNo : "000003",
            TaskGroup : "",
            GroupSeq : "",
            TargetPallet : "FK9873",
            TargetBin : "B001"
        });
        data.push({
            Warehouse : "DC1",
            Area : "B",
            TaskNo : "000004",
            TaskGroup : "",
            GroupSeq : "",
            TargetPallet : "FK9874",
            TargetBin : "B002"
        });
        data.push({
            Warehouse : "DC1",
            Area : "B",
            TaskNo : "000005",
            TaskGroup : "",
            GroupSeq : "",
            TargetPallet : "FK9875",
            TargetBin : "B002"
        });

        // DC2
        data.push({
            Warehouse : "DC2",
            Area : "A",
            TaskNo : "100001",
            TaskGroup : "",
            GroupSeq : "",
            TargetPallet : "FK9871",
            TargetBin : "A001"
        });
        data.push({
            Warehouse : "DC2",
            Area : "A",
            TaskNo : "100002",
            TaskGroup : "",
            GroupSeq : "",
            TargetPallet : "FK9872",
            TargetBin : "A002"
        });
        data.push({
            Warehouse : "DC2",
            Area : "B",
            TaskNo : "100003",
            TaskGroup : "2310",
            GroupSeq : "1",
            TargetPallet : "FK9873",
            TargetBin : "B002"
        }); 
        data.push({
            Warehouse : "DC2",
            Area : "B",
            TaskNo : "100004",
            TaskGroup : "2310",
            GroupSeq : "2",
            TargetPallet : "FK9874",
            TargetBin : "B002"
        });
        data.push({
            Warehouse : "DC2",
            Area : "B",
            TaskNo : "100005",
            TaskGroup : "2310",
            GroupSeq : "3",
            TargetPallet : "FK9875",
            TargetBin : "B002"
        });

        return data;

    });
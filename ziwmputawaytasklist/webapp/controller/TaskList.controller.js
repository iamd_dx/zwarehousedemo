// @ts-nocheck
sap.ui.define([
    "./BaseController",
    "../mockdata/Task"
],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, TaskData) {
        "use strict";

        return Controller.extend("com.imove.iwm.ziwmputawaytasklist.controller.TaskList", {

            // C_MODEL_NAME_SCREEN : "ScreenModel",
            C_MODEL_NAME_SELWH: "SelWHModel",

            selWHModel: null,
            shareModel: null,
            pageCtx: null,
            oInputScanner: null,
            isPopupOpening: false,


            onInit: function () {

                //init share model & context
                this.shareModel = this.getOwnerComponent().getShareModel();
                this.pageCtx = this.shareModel.getContext("/PageTaskList");

                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getTarget("TaskList").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));

                this.initInputScanner();
                this.initPageModel();

            },

            initInputScanner: function () {
                this.oInputScanner = this.getView().byId("input_scanner");
                this.oInputScanner.onsapfocusleave = function () {
                    if (!this.isPopupOpening) {
                        this.focusScanner();
                    }
                }.bind(this);
            },

            initPageModel: function () {
                this.selWHModel = new sap.ui.model.json.JSONModel();
                this.getView().setModel(this.selWHModel, this.C_MODEL_NAME_SELWH);
            },

            handleRouteMatched: function (oEvent) {
                if (!this.pageCtx.getObject().SelWarehouse) {
                    setTimeout(function () {
                        this.openDialogSelectWarehouse();
                    }.bind(this), 1000);
                }
                setTimeout(function () {
                    this.focusScanner();
                }.bind(this), 1000);

            },

            onAfterRendering: function () {

            },

            onPressEditSelWarehouse: function (oEvent) {
                this.openDialogSelectWarehouse();
            },

            openDialogSelectWarehouse: function () {

                if (!this.oSelectWarehouseDialog) {
                    this.oSelectWarehouseDialog = sap.ui.xmlfragment("com.imove.iwm.ziwmputawaytasklist.view.fragment.DialogSelectWarehouse", this);
                    this.getView().addDependent(this.oSelectWarehouseDialog);
                    this.oSelectWarehouseDialog.attachAfterClose(function (oEvent) {
                        this.isPopupOpening = false;
                        this.focusScanner();
                    }.bind(this));
                    this.oSelectWarehouseDialog.attachBeforeOpen(function (oEvent) {
                        this.isPopupOpening = true;
                    }.bind(this));
                }

                //default value
                var ctxObj = this.pageCtx.getObject();
                this.selWHModel.setData({
                    SelWarehouse: ctxObj.SelWarehouse ? ctxObj.SelWarehouse : "",
                    SelArea: ctxObj.SelArea ? ctxObj.SelArea : ""
                });
                this.oSelectWarehouseDialog.open();

            },

            onPressCancelSelectWarehouse: function (oEvent) {
                this.oSelectWarehouseDialog.close();
            },

            onPressOKSelectWarehouse: function () {
                var oData = this.selWHModel.getData();
                var ctxObj = this.pageCtx.getObject();
                ctxObj.SelWarehouse = oData.SelWarehouse;
                ctxObj.SelArea = oData.SelArea;
                this.shareModel.refresh();
                this.oSelectWarehouseDialog.close();
                this.loadTaskList();


            },

            loadTaskList: function () {
                var ctxObj = this.pageCtx.getObject();
                var aTaskList = this.retriveTaskListFromMockData(ctxObj.SelWarehouse, ctxObj.SelArea);
                ctxObj.TaskList = aTaskList;
                this.shareModel.refresh();
            },

            retriveTaskListFromMockData: function (warehouse, area) {
                var aAllTask = JSON.parse(JSON.stringify(TaskData));
                return aAllTask.filter(function (obj) {
                    if (obj.Warehouse === warehouse && (area ? obj.Area === area : true)) {
                        return true;
                    }
                    return false;
                });
            },

            onChangeInputScanner: function (oEvent) {
                var oInput = oEvent.getSource();
                var newVal = oEvent.getParameters().newValue;
                oInput.setValue(""); // clear
                var oTask = this.findTaskByPallet(newVal);
                if (oTask) {
                    if (this.isCorrectSeqOfTaskGroup(oTask)) {
                        var oNextPageData = this.shareModel.getProperty("/PageConfirmPutAway");
                        oNextPageData.SelTask = $.extend({}, oTask, true);
                        this.shareModel.setProperty("/PageConfirmPutAway", oNextPageData);
                        this.oRouter.navTo("ConfirmPutAway");
                    } else {
                        sap.m.MessageToast.show(this.getText("msgTaskHasPrerequisiteSequence"));
                    }


                } else {
                    sap.m.MessageToast.show(this.getText("msgPalletNotMatchedWithCurrentTask"));
                }

            },

            findTaskByPallet: function (palletNo) {
                return this.pageCtx.getObject().TaskList.filter(function (obj) {
                    if (obj.TargetPallet === palletNo) {
                        return true;
                    }
                    return false;
                }.bind(this))[0];
            },

            isCorrectSeqOfTaskGroup: function (oTask) {

                var aPreviousTask = this.pageCtx.getObject().TaskList.filter(function (obj) {
                    if (obj.TaskGroup === oTask.TaskGroup && parseInt(obj.GroupSeq) < parseInt(oTask.GroupSeq)) {
                        return true;
                    }
                    return false;
                }.bind(this));

                return aPreviousTask.length > 0 ? false : true;

            },

            focusScanner: function () {
                this.oInputScanner.focus();
            },

            onPressHome: function () {
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation"); // get a handle on the global XAppNav service
                oCrossAppNavigator.toExternal({
                    target: {
                        shellHash: "#"
                    }
                }); // navigate to Supplier application
            }

            // >>>>> BEGIN OBSOLETE 
            // onTaskPress: function (oEvent) {
            //     var oBindCtx = oEvent.getSource().getBindingContext("ShareModel");
            //     var selObj = oBindCtx.getObject();
            //     this.palletScanCheck( selObj.TargetPallet ).then(function ( scc ) {

            //         var oNextPageData = this.shareModel.getProperty("/PageConfirmPutAway");
            //         oNextPageData.SelTask = $.extend({}, selObj, true);
            //         this.shareModel.setProperty("/PageConfirmPutAway", oNextPageData);
            //         this.oRouter.navTo("ConfirmPutAway");

            //     }.bind(this), function (err) {

            //     }.bind(this));

            // },

            // palletScanCheck: function (targetPallet) {
            //     return new Promise(function (resolve, reject) {
            //         var oDialog = this.createScannerDialog("Scan Pallet", function (scanResult) {
            //             if (scanResult === targetPallet) {
            //                 resolve();
            //             } else {
            //                 sap.m.MessageToast.show("Pallet not matched.");
            //                 reject();
            //             }
            //         }.bind(this));
            //         oDialog.open();
            //     }.bind(this));
            // }
            // >>>>> END OBSOLETE


        });
    });

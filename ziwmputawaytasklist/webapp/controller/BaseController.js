// @ts-nocheck

sap.ui.define([
    "sap/ui/core/mvc/Controller"
],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller) {
        "use strict";

        return Controller.extend("com.imove.iwm.ziwmputawaytasklist.controller.BaseController", {

            createScannerDialog: function ( title , fnScanListener) {

                var oDialog = new sap.m.Dialog({
                    title : title,
                    endButton : new sap.m.Button({
                        text : "Close",
                        press : function(){
                            oDialog.close();
                        }
                    })
                });
                var oInput = new sap.m.Input({
                    change: function (oEvent) {
                        fnScanListener(oEvent.getParameters().newValue);
                        oDialog.close();
                    }
                });
                oDialog.addContent(oInput);

                oDialog.attachAfterOpen(function (oEvent) {
                    oInput.focus();
                });

                return oDialog;

            },

            getText : function( i18nKey ){
                return this.getView().getModel("i18n").getResourceBundle().getText( i18nKey );
            }

        });
    });

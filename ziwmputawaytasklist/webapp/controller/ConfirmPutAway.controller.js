// @ts-nocheck
sap.ui.define([
    "./BaseController"
],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller) {
        "use strict";

        return Controller.extend("com.imove.iwm.ziwmputawaytasklist.controller.ConfirmPutAway", {

            C_MODEL_NAME_INC_BIN: "IncorrectBinModel",
            C_MODEL_NAME_BIN_DATA: "BIN_DATA",
            C_MODEL_NAME_WALL_DATA: "WALL_DATA",
            C_MODEL_NAME_POINT_LIGHT_DATA: "POINT_LIGHT_DATA",

            incBinModel: null,
            shareModel: null,
            pageCtx: null,
            oInputScanner: null,
            isPopupOpening: false,
            oConfirmIncorrectDestBinDialog: null,
            oBinDataDialog: null,
            binDataModel: null,
            wallDataModel: null,
            pointLightDataModel: null,



            onInit: function () {

                //init share model & context
                this.shareModel = this.getOwnerComponent().getShareModel();
                this.pageCtx = this.shareModel.getContext("/PageConfirmPutAway");

                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getTarget("ConfirmPutAway").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));

                this.initInputScanner();
                this.initPageModel();


            },

            initInputScanner: function () {
                this.oInputScanner = this.getView().byId("input_scanner");
                this.oInputScanner.onsapfocusleave = function () {
                    if (!this.isPopupOpening) {
                        this.focusScanner();
                    }
                }.bind(this);
            },

            initPageModel: function () {
                this.incBinModel = new sap.ui.model.json.JSONModel({
                    OriginalDestBin: "",
                    NewDestBin: ""
                });
                this.getView().setModel(this.incBinModel, this.C_MODEL_NAME_INC_BIN);

                this.binDataModel = new sap.ui.model.json.JSONModel();
                this.getView().setModel(this.binDataModel, this.C_MODEL_NAME_BIN_DATA);

                this.wallDataModel = new sap.ui.model.json.JSONModel();
                this.getView().setModel(this.wallDataModel, this.C_MODEL_NAME_WALL_DATA);

                this.pointLightDataModel = new sap.ui.model.json.JSONModel();
                this.getView().setModel(this.binDataModel, this.C_MODEL_NAME_POINT_LIGHT_DATA);
            },

            handleRouteMatched: function (oEvent) {

            },

            onChangeInputScanner: function (oEvent) {
                var oInput = oEvent.getSource();
                var newVal = oEvent.getParameters().newValue;
                oInput.setValue(""); // clear
                if (this.checkMatchDestBin(newVal)) {
                    this.removePutAwayTask();
                } else {
                    this.openConfirmIncorrectDestBin(newVal);
                }

            },

            removePutAwayTask: function () {

                var oPageData = this.pageCtx.getObject();
                var oPageTaskListData = this.shareModel.getProperty("/PageTaskList");
                var aTask = oPageTaskListData.TaskList;
                // remove
                var ind;
                for (var i in aTask) {
                    if (aTask[i].TaskNo === oPageData.SelTask.TaskNo) {
                        ind = i;
                        break;
                    }
                }
                aTask.splice(ind, 1);
                this.shareModel.setProperty("/PageTaskList", oPageTaskListData);
                this.oRouter.navTo("TaskList");
                setTimeout(function () {
                    sap.m.MessageToast.show(this.getText("msgPutAwaySuccess"), {
                        duration: 3000
                    });
                }.bind(this), 500);



            },

            checkMatchDestBin: function (destBin) {
                var oPageData = this.pageCtx.getObject();
                if (oPageData.SelTask.TargetBin === destBin) {
                    return true;
                }
                return false;

            },


            openConfirmIncorrectDestBin: function (newDestBin) {

                if (!this.oConfirmIncorrectDestBinDialog) {
                    this.oConfirmIncorrectDestBinDialog = sap.ui.xmlfragment("com.imove.iwm.ziwmputawaytasklist.view.fragment.DialogIncorrectDestBin", this);
                    this.getView().addDependent(this.oConfirmIncorrectDestBinDialog);
                    this.oConfirmIncorrectDestBinDialog.attachAfterClose(function (oEvent) {
                        this.isPopupOpening = false;
                        this.focusScanner();
                    }.bind(this));
                    this.oConfirmIncorrectDestBinDialog.attachBeforeOpen(function (oEvent) {
                        this.isPopupOpening = true;
                    }.bind(this));

                }
                ;
                this.incBinModel.setData({
                    OriginalDestBin: this.pageCtx.getObject().SelTask.TargetBin,
                    NewDestBin: newDestBin
                });

                this.oConfirmIncorrectDestBinDialog.open();

            },

            onPressCancelIncorrectDestBin: function (oEvent) {
                this.oConfirmIncorrectDestBinDialog.close();
            },

            onPressConfirmIncorrectDestBin: function () {
                this.removePutAwayTask();
            },

            onOpen3Ddialog: function (oEvent) {

                this.load3DMockData();
                if (!this.oBinDataDialog) {
                    this.oBinDataDialog = sap.ui.xmlfragment("com.imove.iwm.ziwmputawaytasklist.view.fragment.Dialog3D", this);
                    this.getView().addDependent(this.oBinDataDialog);
                    this.oBinDataDialog.attachAfterClose(function (oEvent) {
                        this.isPopupOpening = false;
                        this.focusScanner();
                    }.bind(this));
                    this.oBinDataDialog.attachBeforeOpen(function (oEvent) {
                        this.isPopupOpening = true;
                    }.bind(this));
                }
                this.oBinDataDialog.open();
                sap.m.MessageToast.show("Please rotate to horizontal");
            },

            load3DMockData: function (resetCamera) {

                var aFilter = [new sap.ui.model.Filter("WarehouseNo", "EQ", "WH2")];
                Promise.all([
                    this.loadWarehouseBinVisData(aFilter),
                    this.loadWallData(aFilter),
                    this.loadPointLightData(aFilter)
                ])
                    .then(function (rets) {

                        this.getView().setBusy(false);
                        var laBinVisual = rets[0];
                        var laWall = rets[1];
                        var laPointLight = rets[2];
                        var randomFocusBinId = laBinVisual.results[Math.round(Math.random() * laBinVisual.results.length)].Bin;
                        laBinVisual.results.forEach(function (item) {
                            item.EmptyRatio = Math.random() * 100; // For Demo
                            item.Color = this.generateBinColor(
                                item.EmptyRatio,
                                0, 100,
                                0xFF6151, 0x51FF54);
                            if (item.Color == "") {
                                sap.m.MessageBox.alert(item.Bin);
                            }
                            if (item.Bin == randomFocusBinId) {
                                item.Hilight = true;
                            } else {
                                item.Hilight = false;
                            }
                        }.bind(this));

                        laWall.results.forEach(function (item) {
                            item.Color = this.generateBinColor(
                                100,
                                0, 100,
                                0xFF6151, 0xC0C0C0);
                            // item.opacity = 1.0;
                            item.Hilight = false;
                        }.bind(this));

                        var _tempLightColor = 0;
                        laPointLight.results.forEach(function (item) {
                            if (item.Color != "") {
                                item.Color = parseInt(item.Color);
                                _tempLightColor = item.Color;
                            } else {
                                item.Color = _tempLightColor
                            }

                        }.bind(this));
                        this.binDataModel.setData(laBinVisual.results);
                        this.binDataModel.setSizeLimit(laBinVisual.results.length);
                        this.wallDataModel.setData(laWall.results);
                        this.wallDataModel.setSizeLimit(laWall.results.length);
                        this.pointLightDataModel.setData(laPointLight.results);
                        this.pointLightDataModel.setSizeLimit(laPointLight.results.length);
                        // this.setBinData(laBinVisual.results);
                        // this.setWallData(laWall.results);
                        // this.setPointLightData(laPointLight.results);
                        if (resetCamera) {
                            this.getView().byId("TRDVis").moveCameraToCenterArea();
                        }
                        this.getView().byId("detailList").setBusy(false);
                    }.bind(this))
                    .catch(function (errs) {
                        this.getView().setBusy(false);
                    }.bind(this));
            },

            generateBinColor: function (value, minValue, maxValue, minColor, maxColor) {
                value = parseFloat(value);
                var vMinColorHex = ("000000" + minColor.toString(16)).slice(-6).toUpperCase();
                var vMaxColorHex = ("000000" + maxColor.toString(16)).slice(-6).toUpperCase();
                var vRatio = (value - minValue) / (maxValue - minValue);

                var vMinColorVectorR = parseInt(vMinColorHex.substr(0, 2), 16);
                var vMinColorVectorG = parseInt(vMinColorHex.substr(2, 2), 16);
                var vMinColorVectorB = parseInt(vMinColorHex.substr(4, 2), 16);

                var vMaxColorVectorR = parseInt(vMaxColorHex.substr(0, 2), 16);
                var vMaxColorVectorG = parseInt(vMaxColorHex.substr(2, 2), 16);
                var vMaxColorVectorB = parseInt(vMaxColorHex.substr(4, 2), 16);

                var vColorR = Math.floor(vMinColorVectorR + ((vMaxColorVectorR - vMinColorVectorR) * vRatio));
                var vColorG = Math.floor(vMinColorVectorG + ((vMaxColorVectorG - vMinColorVectorG) * vRatio));
                var vColorB = Math.floor(vMinColorVectorB + ((vMaxColorVectorB - vMinColorVectorB) * vRatio));

                var vColorHex = ("00" + vColorR.toString(16)).slice(-2) +
                    ("00" + vColorG.toString(16)).slice(-2) +
                    ("00" + vColorB.toString(16)).slice(-2);
                var vColor = parseInt(vColorHex, 16);

                return vColor;
            },

            loadWarehouseBinVisData: function (aFilter) {
                return new Promise(function (resolve, reject) {
                    var serviceModel = this.getView().getModel("binData");
                    var entity = "/WarehouseAllEmptyBinVisualizeSet";
                    serviceModel.read(entity, {
                        filters: aFilter,
                        success: function (s) {
                            resolve(s);
                        }.bind(this),
                        error: function (e) {
                            this.getView().byId("detailList").setBusy(false);
                            //reject(e);
                            resolve(e);
                        }.bind(this)
                    });
                }.bind(this));
            },
            loadWallData: function (aFilter) {
                return new Promise(function (resolve, reject) {
                    var serviceModel = this.getView().getModel("binData");
                    var entity = "/WarehouseWallSet";
                    serviceModel.read(entity, {
                        filters: aFilter,
                        success: function (s) {
                            resolve(s);
                        }.bind(this),
                        error: function (e) {
                            this.getView().byId("detailList").setBusy(false);
                            resolve(e);
                        }.bind(this)
                    });
                }.bind(this));
            },
            loadPointLightData: function (aFilter) {
                return new Promise(function (resolve, reject) {
                    var serviceModel = this.getView().getModel("binData");
                    var entity = "/WarehousePLightSet";
                    serviceModel.read(entity, {
                        filters: aFilter,
                        success: function (s) {
                            resolve(s);
                        }.bind(this),
                        error: function (e) {
                            this.getView().byId("detailList").setBusy(false);
                            resolve(e);
                        }.bind(this)
                    });
                }.bind(this));
            },

            onExit3DDialog : function(){
                this.oBinDataDialog.close();
            },


            focusScanner: function () {
                this.oInputScanner.focus();
            }, 
 
            onNavBack: function () {
                // window.history.go(-1);
                this.oRouter.navTo("TaskList");
            }

        });
    });

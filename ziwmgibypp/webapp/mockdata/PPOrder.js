sap.ui.define([
],

    function () {
        var data = [
            {
                "PPNo": "5100001",
                "FGId": "10101",
                "FGDesc": "ซีอิ้วขาว 500 ml.",
                "Qty": "1",
                "Unit": "BOX",
                "ReqMatList": [
                    {
                        "MatId": "960001",
                        "MatDesc": "หัวเชื้อซีอิ้วขาว",
                        "MatType": "RAW",
                        "Qty": "2000",
                        "Unit": "ML."
                    },
                    {
                        "MatId": "960002",
                        "MatDesc": "น้ำส้มสายชูกลั่น",
                        "MatType": "RAW",
                        "Qty": "200",
                        "Unit": "ML."
                    },
                    {
                        "MatId": "970001",
                        "MatDesc": "น้ำตาลทราย",
                        "MatType": "RAW",
                        "Qty": "500",
                        "Unit": "G."
                    },
                    {
                        "MatId": "970002",
                        "MatDesc": "น้ำตาลทรายแดง",
                        "MatType": "RAW",
                        "Qty": "500",
                        "Unit": "G."
                    },
                    {
                        "MatId": "970003",
                        "MatDesc": "เกลือ",
                        "MatType": "RAW",
                        "Qty": "150",
                        "Unit": "G."
                    },
                    {
                        "MatId": "970004",
                        "MatDesc": "โมโนโซเดี้ยมกลูตาเมท",
                        "MatType": "RAW",
                        "Qty": "100",
                        "Unit": "G."
                    },
                    {
                        "MatId": "810001",
                        "MatDesc": "ขวดซีอิ้วพลาสติกใส 500 ml.",
                        "MatType": "PACKAGING",
                        "Qty": "12",
                        "Unit": "BOT"
                    }
                ]
            },
            {
                "PPNo": "5100002",
                "FGId": "10101",
                "FGDesc": "ซีอิ้วขาว 500 ml.",
                "Qty": "32",
                "Unit": "BOX",
                "ReqMatList": [
                    {
                        "MatId": "960001",
                        "MatDesc": "หัวเชื้อซีอิ้วขาว",
                        "MatType": "RAW",
                        "Qty": "64000",
                        "Unit": "ML."
                    },
                    {
                        "MatId": "960002",
                        "MatDesc": "น้ำส้มสายชูกลั่น",
                        "MatType": "RAW",
                        "Qty": "6400",
                        "Unit": "ML."
                    },
                    {
                        "MatId": "970001",
                        "MatDesc": "น้ำตาลทราย",
                        "MatType": "RAW",
                        "Qty": "16000",
                        "Unit": "G."
                    },
                    {
                        "MatId": "970002",
                        "MatDesc": "น้ำตาลทรายแดง",
                        "MatType": "RAW",
                        "Qty": "16000",
                        "Unit": "G."
                    },
                    {
                        "MatId": "970003",
                        "MatDesc": "เกลือ",
                        "MatType": "RAW",
                        "Qty": "4800",
                        "Unit": "G."
                    },
                    {
                        "MatId": "970004",
                        "MatDesc": "โมโนโซเดี้ยมกลูตาเมท",
                        "MatType": "RAW",
                        "Qty": "3200",
                        "Unit": "G."
                    },
                    {
                        "MatId": "810001",
                        "MatDesc": "ขวดซีอิ้วพลาสติกใส 500 ml.",
                        "MatType": "PACKAGING",
                        "Qty": "384",
                        "Unit": "BOT"
                    }
                ]
            },
            {
                "PPNo": "5100003",
                "FGId": "10101",
                "FGDesc": "ซีอิ้วขาว 500 ml.",
                "Qty": "12",
                "Unit": "BOX",
                "ReqMatList": [
                    {
                        "MatId": "960001",
                        "MatDesc": "หัวเชื้อซีอิ้วขาว",
                        "MatType": "RAW",
                        "Qty": "24000",
                        "Unit": "ML."
                    },
                    {
                        "MatId": "960002",
                        "MatDesc": "น้ำส้มสายชูกลั่น",
                        "MatType": "RAW",
                        "Qty": "2400",
                        "Unit": "ML."
                    },
                    {
                        "MatId": "970001",
                        "MatDesc": "น้ำตาลทราย",
                        "MatType": "RAW",
                        "Qty": "6000",
                        "Unit": "G."
                    },
                    {
                        "MatId": "970002",
                        "MatDesc": "น้ำตาลทรายแดง",
                        "MatType": "RAW",
                        "Qty": "6000",
                        "Unit": "G."
                    },
                    {
                        "MatId": "970003",
                        "MatDesc": "เกลือ",
                        "MatType": "RAW",
                        "Qty": "1800",
                        "Unit": "G."
                    },
                    {
                        "MatId": "970004",
                        "MatDesc": "โมโนโซเดี้ยมกลูตาเมท",
                        "MatType": "RAW",
                        "Qty": "1200",
                        "Unit": "G."
                    },
                    {
                        "MatId": "810001",
                        "MatDesc": "ขวดซีอิ้วพลาสติกใส 500 ml.",
                        "MatType": "PACKAGING",
                        "Qty": "144",
                        "Unit": "BOT"
                    }
                ]
            },
            {
                "PPNo": "6100001",
                "FGId": "10201",
                "FGDesc": "ซอสหอยนางรม 600 ml.",
                "Qty": "1",
                "Unit": "BOX",
                "ReqMatList": [
                    {
                        "MatId": "960001",
                        "MatDesc": "หัวเชื้อซีอิ้วขาว",
                        "MatType": "RAW",
                        "Qty": "1000",
                        "Unit": "ML."
                    },
                    {
                        "MatId": "960005",
                        "MatDesc": "หัวเชื้อซีอิ้วดำ",
                        "MatType": "RAW",
                        "Qty": "500",
                        "Unit": "ML."
                    },
                    {
                        "MatId": "970004",
                        "MatDesc": "หัวเชื้อน้ำปลา",
                        "MatType": "RAW",
                        "Qty": "250",
                        "Unit": "ML"
                    },

                    {
                        "MatId": "980001",
                        "MatDesc": "น้ำมันพืช",
                        "MatType": "RAW",
                        "Qty": "450",
                        "Unit": "ML"
                    },

                    {
                        "MatId": "970002",
                        "MatDesc": "น้ำตาลทรายแดง",
                        "MatType": "RAW",
                        "Qty": "1500",
                        "Unit": "G."
                    },
                    {
                        "MatId": "970003",
                        "MatDesc": "เกลือ",
                        "MatType": "RAW",
                        "Qty": "200",
                        "Unit": "G."
                    },
                    {
                        "MatId": "810002",
                        "MatDesc": "ขวดแก้วซอสหอยนางรมย์สีชา 600 ml.",
                        "MatType": "PACKAGING",
                        "Qty": "12",
                        "Unit": "BOT"
                    }
                ]
            },
            {
                "PPNo": "6100002",
                "FGId": "10201",
                "FGDesc": "ซอสหอยนางรม 600 ml.",
                "Qty": "12",
                "Unit": "BOX",
                "ReqMatList": [
                    {
                        "MatId": "960001",
                        "MatDesc": "หัวเชื้อซีอิ้วขาว",
                        "MatType": "RAW",
                        "Qty": "12000",
                        "Unit": "ML."
                    },
                    {
                        "MatId": "960005",
                        "MatDesc": "หัวเชื้อซีอิ้วดำ",
                        "MatType": "RAW",
                        "Qty": "6000",
                        "Unit": "ML."
                    },
                    {
                        "MatId": "970004",
                        "MatDesc": "หัวเชื้อน้ำปลา",
                        "MatType": "RAW",
                        "Qty": "3000",
                        "Unit": "ML"
                    },

                    {
                        "MatId": "980001",
                        "MatDesc": "น้ำมันพืช",
                        "MatType": "RAW",
                        "Qty": "5400",
                        "Unit": "ML"
                    },

                    {
                        "MatId": "970002",
                        "MatDesc": "น้ำตาลทรายแดง",
                        "MatType": "RAW",
                        "Qty": "18000",
                        "Unit": "G."
                    },
                    {
                        "MatId": "970003",
                        "MatDesc": "เกลือ",
                        "MatType": "RAW",
                        "Qty": "2400",
                        "Unit": "G."
                    },
                    {
                        "MatId": "810002",
                        "MatDesc": "ขวดแก้วซอสหอยนางรมย์สีชา 600 ml.",
                        "MatType": "PACKAGING",
                        "Qty": "12",
                        "Unit": "BOT"
                    }
                ]
            }
        ];
        return data;
    });

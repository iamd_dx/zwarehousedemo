// @ts-nocheck
sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"com/imove/iwm/ziwmgibypp/model/models"
], function (UIComponent, Device, models) {
	"use strict";

	return UIComponent.extend("com.imove.iwm.ziwmgibypp.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// enable routing
			this.getRouter().initialize();

			// set the device model
            this.setModel(models.createDeviceModel(), "device");
            
            this.initShareModel();

            this.hideLaunchpadShell();
        },
        
        initShareModel: function () {
            this.shareModel = new sap.ui.model.json.JSONModel({
                PagePPListSelection: {
                    SelWarehouse: "",
                    PPSelectionList : []
                },
                PageSummaryGI : {
                    SummaryMatList : []
                }
            });
            this.setModel(this.shareModel, "ShareModel");
        },

        getShareModel: function () {
            return this.shareModel;
        },

        hideLaunchpadShell: function () {
            if (sap.ushell) {
                var oRenderer = sap.ushell.Container.getRenderer("fiori2");
                oRenderer.setHeaderVisibility(false, true);
            }

        }
	});
});

// @ts-nocheck

sap.ui.define([
    "sap/ui/core/mvc/Controller"
],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller) {
        "use strict";

        return Controller.extend("com.imove.iwm.ziwmgibypp.controller.BaseController", {

            getText: function (i18nKey) {
                return this.getView().getModel("i18n").getResourceBundle().getText(i18nKey);
            },

            /**
             * @param obj
             * obj.title
             * obj.message
             * obj.fnOK
             * obj.fnCancel
             */
            createDialogConfirm: function (obj) {

                var oDialog = new sap.m.Dialog({
                    title: obj.title,
                    type: sap.m.DialogType.Message,
                    icon : "sap-icon://question-mark",
                    content: [
                        new sap.m.Text({
                            text: obj.message
                        })
                    ],
                    beginButton: new sap.m.Button({
                        text: this.getText("lbCancel"),
                        press: function (oEvent) {
                            if (obj.fnCancel) {
                                obj.fnCancel();
                            }
                            oDialog.close();
                        }
                    }),
                    endButton: new sap.m.Button({
                        text: this.getText("lbOK"),
                        type: "Emphasized",
                        press: function (oEvent) {
                            if (obj.fnOK) {
                                obj.fnOK();
                            }
                            oDialog.close();
                        }
                    })

                });

                return oDialog;

            }

        });
    });

// @ts-nocheck
sap.ui.define([
    "./BaseController",
    "../mockdata/PPOrder"
],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, PPOrderData) {
        "use strict";

        return Controller.extend("com.imove.iwm.ziwmgibypp.controller.PPListSelection", {

            C_MODEL_NAME_SELWH: "SelWHModel",

            selWHModel: null,
            shareModel: null,
            pageCtx: null,
            oInputScanner: null,
            isDisableAutoFocus: false,

            onInit: function () {

                //init share model & context
                this.shareModel = this.getOwnerComponent().getShareModel();
                this.pageCtx = this.shareModel.getContext("/PagePPListSelection");

                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getTarget("PPListSelection").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));

                this.getView().bindObject("ShareModel>/PagePPListSelection");
                this.initInputScanner();
                this.initPageModel();

            },

            initInputScanner: function () {
                this.oInputScanner = this.getView().byId("input_scanner");
                this.oInputScanner.onsapfocusleave = function () {
                    if (!this.isDisableAutoFocus) {
                        this.focusScanner();
                    }
                }.bind(this);

            },

            initPageModel: function () {
                this.selWHModel = new sap.ui.model.json.JSONModel();
                this.getView().setModel(this.selWHModel, this.C_MODEL_NAME_SELWH);
            },

            handleRouteMatched: function (oEvent) {

                if (!this.pageCtx.getObject().SelWarehouse) {
                    setTimeout(function () {
                        this.openDialogSelectWarehouse();
                    }.bind(this), 1000);
                }
                setTimeout(function () {
                    this.focusScanner();
                }.bind(this), 1000);

            },

            onPressEditSelWarehouse: function (oEvent) {
                this.openDialogSelectWarehouse();
            },

            openDialogSelectWarehouse: function () {

                if (!this.oSelectWarehouseDialog) {
                    this.oSelectWarehouseDialog = sap.ui.xmlfragment("com.imove.iwm.ziwmgibypp.view.fragment.DialogSelectWarehouse", this);
                    this.getView().addDependent(this.oSelectWarehouseDialog);
                    this.oSelectWarehouseDialog.attachAfterClose(function (oEvent) {
                        this.isDisableAutoFocus = false;
                        this.focusScanner();
                    }.bind(this));
                    this.oSelectWarehouseDialog.attachBeforeOpen(function (oEvent) {
                        this.isDisableAutoFocus = true;
                    }.bind(this));
                }

                //default value
                var ctxObj = this.pageCtx.getObject();
                this.selWHModel.setData({
                    SelWarehouse: ctxObj.SelWarehouse ? ctxObj.SelWarehouse : ""
                });
                this.oSelectWarehouseDialog.open();

            },

            onPressCancelSelectWarehouse: function (oEvent) {
                this.oSelectWarehouseDialog.close();
            },

            onPressOKSelectWarehouse: function () {
                var oData = this.selWHModel.getData();
                var ctxObj = this.pageCtx.getObject();
                ctxObj.SelWarehouse = oData.SelWarehouse;
                this.shareModel.refresh();
                this.oSelectWarehouseDialog.close();
            },

            onChangeInputScanner: function (oEvent) {
                var oInput = oEvent.getSource();
                var inputPP = oEvent.getParameters().newValue;
                oInput.setValue(""); // clear
                var oPPOrder = this.findPPOrder(inputPP);
                if (oPPOrder) {
                    if (!this.checkDuplicateSelectPP(inputPP)) {
                        this.pageCtx.getObject().PPSelectionList.push($.extend(true, {}, oPPOrder));
                        this.shareModel.refresh();
                    }else{
                        sap.m.MessageToast.show(this.getText("msgDuplicateItem"));
                    }

                } else {
                    sap.m.MessageToast.show(this.getText("msgPPOrderDoesNotExist"));
                }

            },

            findPPOrder: function (ppNo) {
                for (var i in PPOrderData) {
                    if (PPOrderData[i].PPNo === ppNo) {
                        return PPOrderData[i];
                    }
                }
                return null;

            },

            checkDuplicateSelectPP: function (ppNo) {
                var aSelectList = this.pageCtx.getObject().PPSelectionList;
                for (var i in aSelectList) {
                    if (aSelectList[i].PPNo === ppNo) {
                        return true;
                    }
                }
                return false;
            },

            onPressNext: function (oEvent) {
                if (this.checkHasSelectedPP()) {
                    this.oRouter.navTo("SummaryGI");
                } else {
                    sap.m.MessageToast.show(this.getText("msgPleaseAddPPOrder"));
                }
            },

            checkHasSelectedPP: function () {
                if (this.pageCtx.getObject().PPSelectionList.length > 0) {
                    return true;
                }
                return false;
            },

            onDeletePP: function (oEvent) {
                var oItem = oEvent.getParameters().listItem;
                var oBindingContext = oItem.getBindingContext("ShareModel");
                var itemInd = oBindingContext.sPath.split("/").pop();
                this.pageCtx.getObject().PPSelectionList.splice(itemInd, 1);
                this.shareModel.refresh();
            },

            focusScanner: function () {
                this.oInputScanner.focus();
            },

            onPressHome: function (oEvent) {
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation"); // get a handle on the global XAppNav service
                oCrossAppNavigator.toExternal({
                    target: {
                        shellHash: "#"
                    }
                });
            }

        });
    });

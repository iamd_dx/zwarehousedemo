// @ts-nocheck
sap.ui.define([
    "./BaseController",
],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller) {
        "use strict";

        return Controller.extend("com.imove.iwm.ziwmgibypp.controller.SummaryGI", {

            shareModel: null,
            pageCtx: null,


            onInit: function () {

                //init share model & context
                this.shareModel = this.getOwnerComponent().getShareModel();
                this.pageCtx = this.shareModel.getContext("/PageSummaryGI");

                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getTarget("SummaryGI").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));

                this.getView().bindObject("ShareModel>/PageSummaryGI");
                this.initPageModel();

            },


            initPageModel: function () {
                
            },

            handleRouteMatched: function (oEvent) {
                this.summaryGIData();
            },

            summaryGIData : function(){
                var aSelection = this.shareModel.getProperty("/PagePPListSelection/PPSelectionList");
                var oPageData = this.pageCtx.getObject();
                var tmpSumObj = {};

                for(var i in aSelection){
                    var oSelItem = aSelection[i];

                    var aReqMat = oSelItem.ReqMatList;
                    for(var j in aReqMat ){
                        var oMat = aReqMat[j];
                        if( oMat.MatType !== "RAW" ){
                            continue;
                        }
                        if( tmpSumObj[oMat.MatId] ){
                            tmpSumObj[oMat.MatId].Qty = parseFloat( tmpSumObj[oMat.MatId].Qty ) + parseFloat( oMat.Qty ) ; 
                        }else{
                            tmpSumObj[oMat.MatId] = $.extend(true , {} , oMat );
                        }

                    }
                }

                //convert to array
                var aGIData = [];
                for(var att in tmpSumObj ){
                    aGIData.push( tmpSumObj[att]);
                }
                oPageData.SummaryMatList = aGIData;
                this.shareModel.refresh();

            },

            onPressConfirm : function( oEvent ){
                var oDialog = this.createDialogConfirm({
                    title: this.getText("lbConfirmGI"),
                    message: this.getText("msgConfirmGI"),
                    fnOK: function () {
                        this.getView().setBusy(true);
                        setTimeout(function () {
                            this.getView().setBusy(false);
                            this.confirmGI();
                            this.oRouter.navTo("PPListSelection");
                            //delay message
                            setTimeout(function(){
                                sap.m.MessageToast.show(this.getText("msgGISuccess"));
                            }.bind(this) , 1500);

                        }.bind(this), 1500);
                    }.bind(this)
                });
                oDialog.open();
            },

            confirmGI : function(){
                var oPageSelectionData = this.shareModel.getProperty("/PagePPListSelection");
                oPageSelectionData.PPSelectionList = [];
                var oPageData = this.pageCtx.getObject();
                oPageData.SummaryMatList = [];
                this.shareModel.refresh();
            },


            onPressBack: function (oEvent) {
                this.oRouter.navTo("PPListSelection");
            }

        });
    });

// @ts-nocheck
sap.ui.define([
        "sap/ui/core/mvc/Controller",
        "sap/m/MessageBox",
        "sap/m/MessageToast"
	],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
	function (Controller, MessageBox, MessageToast) {
		"use strict";
		return Controller.extend("com.iam.ziwmgrfinishgoods.controller.FGList", {
            C_MODEL_SCREEN: "ScreenModel",
            C_MODEL_MOCK: "MockModel",
            C_MODEL_LIST: "ListModel",
            
			onInit: function () {
                this.getView().addStyleClass("sapUiSizeCompact");
                this.I18N = this.getOwnerComponent().getModel("i18n").getResourceBundle();
                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getTarget("FGList").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
                this.initModel();
            },
            initModel: function (oEvent) {
                this.getView().setModel(new sap.ui.model.json.JSONModel(), this.C_MODEL_SCREEN);
                this.getView().setModel(new sap.ui.model.json.JSONModel(), this.C_MODEL_MOCK);
                this.getView().setModel(new sap.ui.model.json.JSONModel(), this.C_MODEL_LIST);
                this.resetScreenModel();
                this.resetMockModel();
                this.resetListModel();
            },
            handleRouteMatched: function (oEvent) {
                //Stay Focus on Pallet ID input
                var $input = $("input", this.getView().byId("idPalletInput").getDomRef());
                $input.on("blur", function (oEvent) {
                    this.getView().byId("idPalletInput").focus();
                }.bind(this));
            },
            resetScreenModel: function () {
                this.getView().getModel(this.C_MODEL_SCREEN).setData({
                    PalletID: "",
                    WarehouseNo: "",
                    WarehouseNoDialog: "",
                    PalletAmt: 0
                });
            },
            resetMockModel: function () {
                var MockModel = this.getView().getModel(this.C_MODEL_MOCK);
			    MockModel.loadData(jQuery.sap.getModulePath("com.iam.ziwmgrfinishgoods", "/MockData/FGPallet.json"), {}, false);
            },
            resetListModel: function () {
                this.getView().getModel(this.C_MODEL_LIST).setData(
                    []
                );
            },
            onCheckPallet: function (oEvent) {
                var ScreenModel = this.getView().getModel(this.C_MODEL_SCREEN);
                var aMockModel = this.getView().getModel(this.C_MODEL_MOCK).getData();
                var ListModel = this.getView().getModel(this.C_MODEL_LIST);
                var aListModel = ListModel.getData();
                // debugger;
                var sPalletID = ScreenModel.getProperty("/PalletID");
                var BeforeAddLen = aListModel.length;
                
                //Check Duplicate
                for(var i in aListModel){
                    if(aListModel[i].PalletId === sPalletID){
                        MessageToast.show(this.I18N.getText("Toast.Duplicate"));
                        return;
                    }
                }

                //Add Pallet
                for(var i in aMockModel){
                    if(aMockModel[i].PalletId === sPalletID){
                        aListModel.push(aMockModel[i]);
                        ListModel.refresh();
                        MessageToast.show(this.I18N.getText("Toast.AddSuccess"));
                        break;
                    }
                }

                //Check Not Found
                if(BeforeAddLen === aListModel.length){
                    MessageToast.show(this.I18N.getText("Toast.NotFound"));
                }
                //Reset Pallet ID
                ScreenModel.setProperty("/PalletID", "");
                //Count Pallet Amount
                ScreenModel.setProperty("/PalletAmt", aListModel.length);
            },
            onPressEditSelWarehouse: function (oEvent) {
                this.openDialogSelectWarehouse();
            },
            openDialogSelectWarehouse: function () {
                if (!this.oSelectWarehouseDialog) {
                    this.oSelectWarehouseDialog = sap.ui.xmlfragment("com.iam.ziwmgrfinishgoods.view.fragment.DialogSelectWarehouse", this);
                    this.getView().addDependent(this.oSelectWarehouseDialog);
                    this.oSelectWarehouseDialog.attachAfterClose(function (oEvent) {
                        this.isPopupOpening = false;
                        this.turnOnListenScan();
                    }.bind(this));
                    this.oSelectWarehouseDialog.attachBeforeOpen(function (oEvent) {
                        this.isPopupOpening = true;
                        setTimeout(function () {
                            sap.ui.getCore().byId("idSelWarehouseNo").focus();
                        }.bind(this), 500);
                    }.bind(this));
                }
                
                var ScreenModel = this.getView().getModel(this.C_MODEL_SCREEN);
                var sWarehouseNo = ScreenModel.getProperty("/WarehouseNo");
                ScreenModel.setProperty("/WarehouseNoDialog", sWarehouseNo);
                this.oSelectWarehouseDialog.open();
                this.turnOffListenScan();
            },
            turnOnListenScan: function () {
                var $input = $("input", this.getView().byId("idPalletInput").getDomRef());
                $input.on("blur", function (oEvent) {
                    this.focusScanner();
                }.bind(this));

                this.focusScanner();
            },
            turnOffListenScan: function () {
                var $input = $("input", this.getView().byId("idPalletInput").getDomRef());
                $input.off("blur");
            },
            focusScanner: function () {
                this.getView().byId("idPalletInput").focus();
            },
            onPressCancelSelectWarehouse: function (oEvent) {
                this.oSelectWarehouseDialog.close();
            },
            onPressOKSelectWarehouse: function () {
                var ScreenModel = this.getView().getModel(this.C_MODEL_SCREEN);
                var sWarehouseNoDialog = ScreenModel.getProperty("/WarehouseNoDialog");
                ScreenModel.setProperty("/WarehouseNo", sWarehouseNoDialog);
                this.oSelectWarehouseDialog.close();
            },
            onFGDetail: function (oEvent) {
                var sPalletID = oEvent.getSource().getTitle();
                this.oRouter.navTo("FGDetail", {
                    palletid: sPalletID
                });
            },
            onSavePress: function (oEvent) {
                this.resetListModel();
                MessageBox.success(this.I18N.getText("MessageBox.SaveSuccess"));
            },
            onNavBack: function () {
                var oHistory = sap.ui.core.routing.History.getInstance();
                var sPreviousHash = oHistory.getPreviousHash();
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                if (sPreviousHash !== undefined || !oCrossAppNavigator.isInitialNavigation()) {
                    history.go(-1);
                } else {
                    oCrossAppNavigator.toExternal({
                        target: { shellHash: "#Shell-home" }
                    });
                }
            }
		});
	});
// @ts-nocheck
sap.ui.define([
        "sap/ui/core/mvc/Controller",
        "sap/m/MessageBox",
        "sap/m/MessageToast",
        "sap/ui/core/routing/History"
	],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
	function (Controller, MessageBox, MessageToast, History) {
		"use strict";
		return Controller.extend("com.iam.ziwmgrfinishgoods.controller.FGDetail", {
            C_MODEL_SCREEN: "ScreenModel",
            C_MODEL_MOCK: "MockModel",
            C_MODEL_LIST: "ListModel",
            
			onInit: function () {
                // this.getView().addStyleClass("sapUiSizeCompact");
                this.I18N = this.getOwnerComponent().getModel("i18n").getResourceBundle();
                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getTarget("FGDetail").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
                this.initModel();
            },
            initModel: function (oEvent) {
                this.getView().setModel(new sap.ui.model.json.JSONModel(), this.C_MODEL_SCREEN);
                this.getView().setModel(new sap.ui.model.json.JSONModel(), this.C_MODEL_MOCK);
                this.getView().setModel(new sap.ui.model.json.JSONModel(), this.C_MODEL_LIST);
                this.resetScreenModel();
                this.resetMockModel();
                this.resetListModel();
            },
            handleRouteMatched: function (oEvent) {
                var ScreenModel = this.getView().getModel(this.C_MODEL_SCREEN);
                var sPalletID = oEvent.getParameter("data").palletid;
                ScreenModel.setProperty("/PalletID", sPalletID);
                this.loadFGDetail();
            },
            resetScreenModel: function () {
                this.getView().getModel(this.C_MODEL_SCREEN).setData({
                    PalletID: ""
                });
            },
            resetMockModel: function () {
                var MockModel = this.getView().getModel(this.C_MODEL_MOCK);
			    MockModel.loadData(jQuery.sap.getModulePath("com.iam.ziwmgrfinishgoods", "/MockData/FGPallet.json"), {}, false);
            },
            resetListModel: function () {
                this.getView().getModel(this.C_MODEL_LIST).setData([]);
            },
            loadFGDetail: function () {
                var ScreenModel = this.getView().getModel(this.C_MODEL_SCREEN);
                var aMockModel = this.getView().getModel(this.C_MODEL_MOCK).getData();
                var ListModel = this.getView().getModel(this.C_MODEL_LIST);
                var sPalletID = ScreenModel.getProperty("/PalletID");
                
                //Find FGDetail
                for(var i in aMockModel){
                    if(aMockModel[i].PalletId === sPalletID){
                        ListModel.setData(aMockModel[i]);
                        break;
                    }
                }
            },
            onNavBack: function () {
                var oHistory = sap.ui.core.routing.History.getInstance();
                var sPreviousHash = oHistory.getPreviousHash();
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                if (sPreviousHash !== undefined || !oCrossAppNavigator.isInitialNavigation()) {
                    history.go(-1);
                } else {
                    oCrossAppNavigator.toExternal({
                        target: { shellHash: "#Shell-home" }
                    });
                }
            }
		});
	});
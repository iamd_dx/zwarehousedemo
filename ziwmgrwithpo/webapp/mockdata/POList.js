sap.ui.define([],

    function () {

        var data = [];
        data.push({
            Warehouse : "MC1",
            PONo : "1900001",
            POItem : "0001",
            MatId : "960001",
            MatDesc : "หัวเชื้อซีอิ้วขาว",
            Qty : "100",
            Unit : "GALLON",
            ReceivedQty : "30"
        });
        data.push({
            Warehouse : "MC1",
            PONo : "1900001",
            POItem : "0002",
            MatId : "960003",
            MatDesc : "หัวเชื้อซีอิ้วดำ",
            Qty : "60",
            Unit : "GALLON",
            ReceivedQty : "0"
        });
        data.push({
            Warehouse : "MC1",
            PONo : "1900002",
            POItem : "0001",
            MatId : "970001",
            MatDesc : "น้ำตาลทราย",
            Qty : "100",
            Unit : "BAG",
            ReceivedQty : "40"
        });
        data.push({
            Warehouse : "MC1",
            PONo : "1900002",
            POItem : "0002",
            MatId : "970002",
            MatDesc : "น้ำตาลทรายแดง",
            Qty : "80",
            Unit : "BAG",
            ReceivedQty : "0"
        });
        data.push({
            Warehouse : "MC1",
            PONo : "1900002",
            POItem : "0003",
            MatId : "970003",
            MatDesc : "เกลือ",
            Qty : "30",
            Unit : "BAG",
            ReceivedQty : "0"
        });

        data.push({
            Warehouse : "MC1",
            PONo : "1900003",
            POItem : "0001",
            MatId : "980001",
            MatDesc : "น้ำมันพืช",
            Qty : "80",
            Unit : "GALLON",
            ReceivedQty : "0"
        });

        return data;

    });
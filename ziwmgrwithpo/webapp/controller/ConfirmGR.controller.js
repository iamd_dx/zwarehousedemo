// @ts-nocheck
sap.ui.define([
    "./BaseController",
    "../mockdata/POList"
],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, POData) {
        "use strict";

        return Controller.extend("com.imove.iwm.ziwmgrwithpo.controller.ConfirmGR", {

            C_MODEL_NAME_SELWH: "SelWHModel",
            C_MODEL_NAME_NEWPALLET: "NewPalletModel",

            selWHModel: null,
            newPalletModel: null,
            shareModel: null,
            pageCtx: null,
            oInputScanner: null,
            isDisableAutoFocus: false,

            onInit: function () {

                //init share model & context
                this.shareModel = this.getOwnerComponent().getShareModel();
                this.pageCtx = this.shareModel.getContext("/PageConfirmGR");

                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getTarget("ConfirmGR").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));

                this.getView().bindObject("ShareModel>/PageConfirmGR");
                this.initInputScanner();
                this.initPageModel();

            },

            initInputScanner: function () {
                this.oInputScanner = this.getView().byId("input_scanner");
                this.oInputScanner.onsapfocusleave = function () {
                    if (!this.isDisableAutoFocus) {
                        this.focusScanner();
                    }
                }.bind(this);

                this.getView().byId("input_grqty").onfocusin = function () {
                    this.isDisableAutoFocus = true;
                }.bind(this);
                this.getView().byId("input_grqty").onsapfocusleave = function () {
                    this.isDisableAutoFocus = false;
                    this.focusScanner();
                }.bind(this);
                this.getView().byId("input_grunit").onfocusin = function () {
                    this.isDisableAutoFocus = true;
                }.bind(this);
                this.getView().byId("input_grunit").onsapfocusleave = function () {
                    this.isDisableAutoFocus = false;
                    this.focusScanner();
                }.bind(this);
            },

            initPageModel: function () {
                this.selWHModel = new sap.ui.model.json.JSONModel();
                this.getView().setModel(this.selWHModel, this.C_MODEL_NAME_SELWH);
                this.newPalletModel = new sap.ui.model.json.JSONModel();
                this.getView().setModel(this.newPalletModel, this.C_MODEL_NAME_NEWPALLET);
            },

            handleRouteMatched: function (oEvent) {

                setTimeout(function () {
                    this.focusScanner();
                }.bind(this), 1000);
                this.setDefaultGRValue();

            },

            setDefaultGRValue: function () {
                var oPageData = this.pageCtx.getObject();
                oPageData.GRQty = parseFloat(oPageData.SelPOItem.Qty) - parseFloat(oPageData.SelPOItem.ReceivedQty);
                oPageData.GRUnit = oPageData.SelPOItem.Unit;
                this.shareModel.refresh();
            },

            onAfterRendering: function () {

            },

            onPressEditSelWarehouse: function (oEvent) {
                this.openDialogSelectWarehouse();
            },

            onChangeInputScanner: function (oEvent) {
                var targetPallet = oEvent.getParameters().newValue;
                oEvent.getSource().setValue(""); // clear 
                if (targetPallet && this.checkTargetPalletExist(targetPallet)) {
                    this.confirmGR(targetPallet);
                    this.processAfterGRSuccess();
                } else {
                    sap.m.MessageToast.show(this.getText("msgPalletTargetNotFound"));
                }

            },

            checkTargetPalletExist: function (targetPallet) {
                var aPallet = this.shareModel.getProperty("/PagePOList/PalletList");
                var aExistPallet = aPallet.filter(function (obj) {
                    if (obj === targetPallet) {
                        return true;
                    }
                    return false;
                }.bind(this));
                if (aExistPallet.length > 0) {
                    return true;
                }
                return false;

            },

            confirmGR: function (targetPallet) {

                var oPageData = this.pageCtx.getObject();
                var oSelPOItem = oPageData.SelPOItem;
                var aPOList = this.shareModel.getProperty("/PagePOList/POList");


                var indPOItem = "";
                for (var i in aPOList) {
                    var oPOItem = aPOList[i];
                    if (oPOItem.PONo === oSelPOItem.PONo && oPOItem.POItem === oSelPOItem.POItem) {
                        this.adjustPOItemQty(oPOItem, oPageData.GRQty);
                        indPOItem = i;
                        break;
                    }
                }

                //check recieve complete item , then remove 
                if (this.isPOItemCompleteGR(aPOList[indPOItem])) {
                    aPOList.splice(indPOItem, 1);
                    this.shareModel.setProperty("/PagePOList/POList", aPOList);
                }

                this.shareModel.refresh();

            },

            processAfterGRSuccess: function () {
                this.oRouter.navTo("POList");
                setTimeout(function () {
                    sap.m.MessageToast.show(this.getText("lbSuccessProcessGR"));
                }.bind(this), 1000);
            },

            adjustPOItemQty: function (oPOItem, grQTY) {

                oPOItem.ReceivedQty = (parseFloat(oPOItem.ReceivedQty) + parseFloat(grQTY)).toFixed(2);

            },

            isPOItemCompleteGR: function (oPOItem) {

                if (parseFloat(oPOItem.ReceivedQty) >= parseFloat(oPOItem.Qty)) {
                    return true;
                }
                return false;

            },

            onConfirmToNewPallet: function (oEvent) {
                var oDialog = this.createDialogConfirm({
                    title: this.getText("lbConfirm"),
                    message: this.getText("lbConfirmToNewPallet"),
                    fnOK: function () {
                        this.getView().setBusy(true);
                        setTimeout(function () {
                            this.confirmGR();
                            this.getView().setBusy(false);
                            this.generateNewPallet();
                        }.bind(this), 1500);
                    }.bind(this)
                });
                oDialog.attachAfterClose(function (oEvent) {
                    this.isDisableAutoFocus = false;
                    this.focusScanner();
                }.bind(this));
                oDialog.attachBeforeOpen(function (oEvent) {
                    this.isDisableAutoFocus = true;
                }.bind(this));
                oDialog.open();
            },

            generateNewPallet: function () {
                if (!this.oGeneratePalletSuccessDialog) {
                    this.oGeneratePalletSuccessDialog = sap.ui.xmlfragment("com.imove.iwm.ziwmgrwithpo.view.fragment.DialogGeneratePalletSuccess", this);
                    this.getView().addDependent(this.oGeneratePalletSuccessDialog);

                    this.oGeneratePalletSuccessDialog.attachAfterClose(function (oEvent) {
                        this.isDisableAutoFocus = false;
                        this.focusScanner();
                    }.bind(this));
                    this.oGeneratePalletSuccessDialog.attachBeforeOpen(function (oEvent) {
                        this.isDisableAutoFocus = true;
                    }.bind(this));
                    this.oGeneratePalletSuccessDialog.attachAfterOpen(function (oEvent) {

                        var pressTimer;
                        var fnActionUp = function () {
                            clearTimeout(pressTimer);
                            // Clear timeout
                            return false;
                        }.bind(this);
                        var fnActionDown = function () {
                            // Set timeout
                            pressTimer = setTimeout(function () {
                                this.oGeneratePalletSuccessDialog.close();
                                this.processAfterGRSuccess();
                            }.bind(this), 1500);
                            return false;
                        }.bind(this);
                        $("#but_close_newpalletdialog").touchend(fnActionUp).touchstart(fnActionDown);
                        $("#but_close_newpalletdialog").mouseup(fnActionUp).mousedown(fnActionDown);

                    }.bind(this));

                }

                this.setNewPalletData();
                this.oGeneratePalletSuccessDialog.open();
                sap.m.MessageToast.show(this.getText("lbPrinting"), {
                    duration: 1500
                });
            },

            setNewPalletData: function () {
                var newPalletId = this.generateNewPalletId();
                this.newPalletModel.setData({
                    NewPalletId: newPalletId,
                    EnableClose: true
                });

                //add new pallet to pallet list

                if (!this.checkTargetPalletExist(newPalletId)) {
                    var aPallet = this.shareModel.getProperty("/PagePOList/PalletList");
                    aPallet.push(newPalletId);
                    this.shareModel.setProperty("/PagePOList/PalletList", aPallet);
                }

            },

            generateNewPalletId: function () {
                return ["FK48"
                    , Math.round(Math.random() * 9)
                    , Math.round(Math.random() * 9)].join("");
            },

            onPrintBarcode: function () {
                sap.m.MessageToast.show(this.getText("lbPrinting"), {
                    duration: 1500
                });
                // setTimeout(function () {
                //     this.newPalletModel.setProperty("/EnableClose", true);
                // }.bind(this), 1500);

            },

            focusScanner: function () {
                this.oInputScanner.focus();
            },

            onNavBack: function () {
                this.oRouter.navTo("POList");
            }

        });
    });

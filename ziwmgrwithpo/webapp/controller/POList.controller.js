// @ts-nocheck
sap.ui.define([
    "./BaseController",
    "../mockdata/POList",
    "../mockdata/ExistPalletList"
],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, POData , ExistPalletList ) {
        "use strict";

        return Controller.extend("com.imove.iwm.ziwmgrwithpo.controller.POList", {

            C_MODEL_NAME_SELWH: "SelWHModel",

            selWHModel: null,
            shareModel: null,
            pageCtx: null,
            oInputScanner: null,
            isPopupOpening: false,

            onInit: function () {

                //init share model & context 
                this.shareModel = this.getOwnerComponent().getShareModel();
                this.pageCtx = this.shareModel.getContext("/PagePOList");

                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getTarget("POList").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));

                this.getView().bindObject("ShareModel>/PagePOList");
                this.initPageModel();

            },



            initPageModel: function () {
                this.selWHModel = new sap.ui.model.json.JSONModel();
                this.getView().setModel(this.selWHModel, this.C_MODEL_NAME_SELWH);
            },

            handleRouteMatched: function (oEvent) {
                if (!this.pageCtx.getObject().SelWarehouse) {
                    setTimeout(function () {
                        this.openDialogSelectWarehouse();
                    }.bind(this), 1000);
                }
            },

            onAfterRendering: function () {

            },

            onPressEditSelWarehouse: function (oEvent) {
                this.openDialogSelectWarehouse();
            },

            openDialogSelectWarehouse: function () {

                if (!this.oSelectWarehouseDialog) {
                    this.oSelectWarehouseDialog = sap.ui.xmlfragment("com.imove.iwm.ziwmgrwithpo.view.fragment.DialogSelectWarehouse", this);
                    this.getView().addDependent(this.oSelectWarehouseDialog);
                    // this.oSelectWarehouseDialog.attachAfterClose(function (oEvent) {
                    //     this.isPopupOpening = false;
                    //     this.focusScanner();
                    // }.bind(this));
                    // this.oSelectWarehouseDialog.attachBeforeOpen(function (oEvent) {
                    //     this.isPopupOpening = true;
                    // }.bind(this));
                }

                //default value
                var ctxObj = this.pageCtx.getObject();
                this.selWHModel.setData({
                    SelWarehouse: ctxObj.SelWarehouse ? ctxObj.SelWarehouse : ""
                });
                this.oSelectWarehouseDialog.open();

            },

            onPressCancelSelectWarehouse: function (oEvent) {
                this.oSelectWarehouseDialog.close();
            },

            onPressOKSelectWarehouse: function () {
                var oData = this.selWHModel.getData();

                var ctxObj = this.pageCtx.getObject();
                ctxObj.SelWarehouse = oData.SelWarehouse;
                this.shareModel.refresh();
                this.oSelectWarehouseDialog.close();
                this.loadPOData();
                this.loadStubExistPallet();

            },

            loadPOData: function () {
                var oPageOdata = this.pageCtx.getObject();
                var warehouse = oPageOdata.SelWarehouse
                var aData = JSON.parse(JSON.stringify(POData)).filter(function (obj) {
                    if (obj.Warehouse === warehouse) {
                        return true;
                    }
                    return false;
                });
                oPageOdata.POList = aData;
                this.shareModel.refresh();
            },

            loadStubExistPallet : function(){
                var oPageOdata = this.pageCtx.getObject();
                oPageOdata.PalletList = JSON.parse(JSON.stringify(ExistPalletList));
                this.shareModel.refresh();
            },

            onChangeInputScanner: function (oEvent) {
                this.doFilterPO();
            },

            doFilterPO: function () {
                var oList = this.getView().byId("list_po");
                var filterPO = this.pageCtx.getObject().FilterPO;
                var oBinding = oList.getBinding("items");
                if (filterPO) {
                    oBinding.filter([new sap.ui.model.Filter("PONo", "EQ", filterPO)]);
                } else {
                    oBinding.filter([]);
                }

            },

            onPOItemPress: function (oEvent) {
                var oBindCtx = oEvent.getSource().getBindingContext("ShareModel");
                var selObj = oBindCtx.getObject();
 
                var oNextPageData = this.shareModel.getProperty("/PageConfirmGR");
                oNextPageData.SelPOItem = $.extend({}, selObj, true);
                this.shareModel.setProperty("/PageConfirmGR", oNextPageData);
                this.oRouter.navTo("ConfirmGR");
            },

            focusScanner: function () {
                this.oInputScanner.focus();
            },

            

            onPressHome: function () {
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation"); // get a handle on the global XAppNav service
                oCrossAppNavigator.toExternal({
                    target: {
                        shellHash: "#"
                    }
                }); // navigate to Supplier application
            }


        });
    });

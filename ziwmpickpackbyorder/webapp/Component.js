// @ts-nocheck
sap.ui.define([
    "sap/ui/core/UIComponent",
    "sap/ui/Device",
    "com/imove/iwm/ziwmpickpackbyorder/model/models"
], function (UIComponent, Device, models) {
    "use strict";

    return UIComponent.extend("com.imove.iwm.ziwmpickpackbyorder.Component", {

        metadata: {
            manifest: "json"
        },

        aMockTaskList: null,

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
        init: function () {
            // call the base component's init function
            UIComponent.prototype.init.apply(this, arguments);

            // enable routing
            this.getRouter().initialize();

            // set the device model
            this.setModel(models.createDeviceModel(), "device");

            try {
                var oRenderer = sap.ushell.Container.getRenderer("fiori2");
                oRenderer.setHeaderVisibility(false, true);
            } catch (e) { }
        },

        getMockAllTask: function () {
            if (this.aMockTaskList == null) {
                var mMockData = new sap.ui.model.json.JSONModel({});
                var sModulePath = jQuery.sap.getModulePath("com.imove.iwm.ziwmpickpackbyorder.mockup", "/PalletData.json");
                mMockData.loadData(sModulePath, null, false);
                this.aMockTaskList = mMockData.getProperty("/PalletList");
            }
            return this.aMockTaskList;
        },

        getWarehouseTaskbyLocation: function (oLocation) {
            var sWarehouse = oLocation.WarehouseNo;
            var sArea = oLocation.Area;
            var aAllTaskList = this.getMockAllTask();

            if (!!sWarehouse && !!sArea) {
                return aAllTaskList.filter(function (oItem) {
                    return oItem.WarehouseNo === sWarehouse && oItem.Area === sArea;
                });
            } else {
                return [];
            }
        },

        confirmWarehouseTask: function (sTaskNo) {
            for (var nIndex in this.aMockTaskList) {
                if (this.aMockTaskList[nIndex].TaskNo === sTaskNo) {
                    this.aMockTaskList.splice(nIndex, 1);
                    break;
                }
            }
        },

        createContent: function () {
            var app = new sap.m.App("app");
            var appType = "app";
            var appBackgroundColor = "#FFFFFF";
            if (appType === "app" && appBackgroundColor) {
                app.setBackgroundColor(appBackgroundColor);
            }
            app.addStyleClass("sapUiSizeCompact");

            return app;
        }
    });
});

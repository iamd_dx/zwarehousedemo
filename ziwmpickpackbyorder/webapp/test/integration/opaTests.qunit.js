// @ts-nocheck
/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"com/imove/iwm/ziwmpickpackbyorder/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});

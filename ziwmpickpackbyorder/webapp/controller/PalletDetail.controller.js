// @ts-nocheck
sap.ui.define([
    "sap/ui/core/mvc/Controller",
    'sap/m/MessageStrip'
],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, MessageStrip) {
        "use strict";

        return Controller.extend("com.imove.iwm.ziwmpickpackbyorder.controller.PalletDetail", {
            mScrModel: null,
            mNewPalletModel: new sap.ui.model.json.JSONModel({ PalletNo: "" }, true),

            onInit: function () {
                this.oPage = this.getView().byId("idPage");
                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getTarget("PalletDetail").attachDisplay(this.onRouteMatched.bind(this));
            },

            onRouteMatched: function (oEvent) {
                this.initialDataModel();
                this.isBringNewPallet();
                this.isMoveOutOrMoveIn();
                this.summaryPickupMethod();

                if (this.mScrModel.getProperty("/SelectedPallet/IsBringNewPallet")) {
                    setTimeout(function () {
                        this.openDialogScanNewPallet();
                    }.bind(this), 500);
                } else {
                    this.turnOnListenScan("idBarcode");
                }
            },

            onNavBack: function (oEvent) {
                var oHistory = sap.ui.core.routing.History.getInstance();
                var sPreviousHash = oHistory.getPreviousHash();
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                if (sPreviousHash !== undefined || !oCrossAppNavigator.isInitialNavigation()) {
                    history.go(-1);
                } else {
                    oCrossAppNavigator.toExternal({
                        target: { shellHash: "#Shell-home" }
                    });
                }
            },

            onChangedBarcode: function (oEvent) {
                var oSelectedPallet = this.mScrModel.getProperty("/SelectedPallet");
                var sBarcode = oEvent.getSource().getValue();
                oEvent.getSource().setValue(null);
                this.clearMessageStrip();

                if ((oSelectedPallet.PickupMethod !== "PICK" && oSelectedPallet.PalletNo === sBarcode) ||
                    (oSelectedPallet.PickupMethod === "PICK" && oSelectedPallet.NewPallet === sBarcode)) {
                    this.openConfirmPickup();
                } else {
                    this.showMessageStrip("Error", "กรุณาสแกน Pallet ให้ถูกต้อง!!");
                }
            },

            onPressCancelScanPallet: function () {
                this.oScanNewPallerDialog.close();
                this.onNavBack();
            },

            onChangedNewPalletBarcode: function (oEvent) {
                var sBarcode = oEvent.getSource().getValue();
                this.oScanNewPallerDialog.close();
                if (!!sBarcode) {
                    this.mScrModel.setProperty("/SelectedPallet/NewPallet", sBarcode);
                } else {
                    this.onNavBack();
                }
            },

            onPressCancel: function () {
                this.oConfirmPickupDialog.close();
            },

            onPressConfirm: function () {
                var sTaskNo = this.mScrModel.getProperty("/SelectedPallet/TaskNo");
                this.getOwnerComponent().confirmWarehouseTask(sTaskNo);
                this.oConfirmPickupDialog.close();
                this.onNavBack();
            },



            initialDataModel: function () {
                this.mScrModel = this.getOwnerComponent().getModel("mScrModel");
                this.getView().setModel(this.mNewPalletModel, "mNewPallet");
            },

            turnOnListenScan: function (id) {
                var oInput = this.getView().byId(id);
                if (!oInput) oInput = sap.ui.getCore().byId(id);
                var $input = $("input", oInput.getDomRef());
                $input.on("blur", function (oEvent) {
                    this.focusScanner(id);
                }.bind(this));

                this.focusScanner(id);
            },

            turnOffListenScan: function (id) {
                var oInput = this.getView().byId(id);
                if (!oInput) oInput = sap.ui.getCore().byId(id);
                var $input = $("input", oInput.getDomRef());
                $input.off("blur");
            },

            isBringNewPallet: function () {
                var oSelectedPallet = this.mScrModel.getProperty("/SelectedPallet");
                if (oSelectedPallet && oSelectedPallet.PalletQuantity !== oSelectedPallet.PickupQuantity) {
                    oSelectedPallet.IsBringNewPallet = true;
                }
            },

            isMoveOutOrMoveIn: function () {
                // Verify quantity for decision move-out and bring old pallet or move-in and bring new pallet
                var oSelectedPallet = this.mScrModel.getProperty("/SelectedPallet");
                if (oSelectedPallet && (oSelectedPallet.PalletQuantity / 2) < oSelectedPallet.PickupQuantity) {
                    oSelectedPallet.IsChangeStorePallet = true;
                }
            },

            summaryPickupMethod: function () {
                var oSelectedPallet = this.mScrModel.getProperty("/SelectedPallet");
                if (oSelectedPallet.IsBringNewPallet && oSelectedPallet.IsChangeStorePallet) {
                    oSelectedPallet.PickupMethod = "REPLACE";
                    oSelectedPallet.PickupMethodDescp = "หยิบเก็บ " + (oSelectedPallet.PalletQuantity - oSelectedPallet.PickupQuantity);
                } else if (oSelectedPallet.IsBringNewPallet && !oSelectedPallet.IsChangeStorePallet) {
                    oSelectedPallet.PickupMethod = "PICK";
                    oSelectedPallet.PickupMethodDescp = "หยิบออก " + oSelectedPallet.PickupQuantity;
                } else if (!oSelectedPallet.IsBringNewPallet) {
                    oSelectedPallet.PickupMethod = "ALL";
                    oSelectedPallet.PickupMethodDescp = "ทั้ง Pallet";
                }
                this.mScrModel.refresh();
            },

            showMessageStrip: function (sType, sMessage) {
                this.clearMessageStrip();

                var oMsg = new sap.m.MessageStrip("idMessage", {
                    text: sMessage,
                    showIcon: true,
                    type: sType
                });

                this.getView().addDependent(oMsg);
                this.oPage.insertContent(oMsg, 0);
            },

            clearMessageStrip: function (sType, sMessage) {
                var oMsg = sap.ui.getCore().byId("idMessage");
                if (oMsg) {
                    oMsg.destroy();
                }
            },

            focusScanner: function (id) {
                var oInput = this.getView().byId(id);
                if (!oInput) oInput = sap.ui.getCore().byId(id);
                oInput.focus();
            },

            openDialogScanNewPallet: function () {
                if (!this.oScanNewPallerDialog) {
                    this.oScanNewPallerDialog = sap.ui.xmlfragment("com.imove.iwm.ziwmpickpackbyorder.view.fragment.DialogScanNewPallet", this);
                    this.getView().addDependent(this.oScanNewPallerDialog);
                    this.oScanNewPallerDialog.attachAfterClose(function (oEvent) {
                        this.isPopupOpening = false;
                        this.turnOnListenScan("idBarcode");
                        this.turnOffListenScan("idNewPalletBarcode");
                    }.bind(this));
                    this.oScanNewPallerDialog.attachBeforeOpen(function (oEvent) {
                        var oNewPallet = this.mNewPalletModel.getData();
                        var oScrData = this.mScrModel.getData();
                        oNewPallet.PalletNo = "";
                        this.isPopupOpening = true;
                        this.mNewPalletModel.refresh();

                        setTimeout(function () {
                            this.turnOnListenScan("idNewPalletBarcode");
                        }.bind(this), 500);
                    }.bind(this));
                }

                //default value
                this.oScanNewPallerDialog.open();
                this.turnOffListenScan("idBarcode");
            },

            openConfirmPickup: function (newDestBin) {
                if (!this.oConfirmPickupDialog) {
                    this.oConfirmPickupDialog = sap.ui.xmlfragment("com.imove.iwm.ziwmpickpackbyorder.view.fragment.DialogConfirmPickup", this);
                    this.getView().addDependent(this.oConfirmPickupDialog);
                    this.oConfirmPickupDialog.attachAfterClose(function (oEvent) {
                        this.isPopupOpening = false;
                    }.bind(this));
                    this.oConfirmPickupDialog.attachBeforeOpen(function (oEvent) {
                        this.isPopupOpening = true;
                    }.bind(this));
                }

                this.oConfirmPickupDialog.open();
            },
        });
    });

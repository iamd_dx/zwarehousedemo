// @ts-nocheck
sap.ui.define([
    "sap/ui/core/mvc/Controller"
],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller) {
        "use strict";

        return Controller.extend("com.imove.iwm.ziwmvismanagement.controller.App", {
            onInit: function () {

                var oViewModel,
                    fnSetAppNotBusy,
                    iOriginalBusyDelay = this.getView().getBusyIndicatorDelay();

                oViewModel = new sap.ui.model.json.JSONModel({
                    layout: "TwoColumnsMidExpanded",
                    previousLayout: "",
                    actionButtonsInfo: {
                        midColumn: {
                            fullScreen: false
                        }
                    }
                });
                this.getView().setModel(oViewModel, "appView");

                this.getView().addStyleClass("sapUiSizeCompact");

            }
        });
    });

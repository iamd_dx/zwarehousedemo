// @ts-nocheck

sap.ui.define([
    "./BaseController",
    "../mockdata/WarehouseLayout"
],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, WarehouseLayoutData) {
        "use strict";

        return Controller.extend("com.imove.iwm.ziwmvismanagement.controller.3DView", {

            shareModel: null,
            pageCtx: null,

            onInit: function () {

                //init share model & context
                this.shareModel = this.getOwnerComponent().getShareModel();
                this.pageCtx = this.shareModel.getContext("/Page3D");
                this.getView().bindObject("ShareModel>/Page3D");

                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getTarget("3DView").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
                
                this.getOwnerComponent().set3DControl(this.getView().byId("TRDVis"));
            },

            handleRouteMatched: function (oEvent) {

            },

            onBinClick: function (oEvent) {

                var oClickBin = oEvent.getSource().getBindingContext("ShareModel").getObject();
                var oDataPageControl = this.shareModel.getContext("/PageControl").getObject();

                switch (oDataPageControl.Action) {

                    case "NONE":
                    case "BINSELECTED":
                        this.clearCurrentSelectBinValue();
                        //set selected bin
                        oClickBin.IsHighLight = true;
                        oDataPageControl.SelBin = oClickBin;
                        oDataPageControl.Action = "BINSELECTED";
                        //set default transfer mode
                        oDataPageControl.TransferMode = "";
                        this.shareModel.refresh();
                        // this.getView().byId("TRDVis").rerenderVisualizeContent();
                        break;

                    case "PREPARETRANSFER":

                        if (oDataPageControl.TransferMode === "INT_TRANSFER") {

                            if (!this.checkSelectBinHasSelectedItem()) {
                                sap.m.MessageToast.show(this.getText("msgPleaseSelectPalletToMove"));
                                break;
                            }

                            this.createDialogConfirm({
                                title: this.getText("lbConfirmMove"),
                                message: this.getText("msgMoveToBin") + ": " + oClickBin.Bin,
                                fnOK: function () {
                                    this.transferPallet(oDataPageControl.SelBin, oClickBin);
                                    this.dePressToggleButton();
                                    oDataPageControl.SelBin = null;
                                    oDataPageControl.Action = "NONE";
                                    //set default transfer mode
                                    oDataPageControl.TransferMode = "";

                                    this.shareModel.refresh();
                                }.bind(this) 
                            }).open();

                        }else if( oPageControlData.TransferMode === "EXT_TRANSFER" ){

                        }

                        break;

                    default:
                        break;

                }

            },

            clearCurrentSelectBinValue: function () {
                var oDataPageControl = this.shareModel.getContext("/PageControl").getObject();
                // clear value previous select bin
                if (oDataPageControl.SelBin) {
                    //dehightlight
                    oDataPageControl.SelBin.IsHighLight = false;
                    //deselect pallet
                    if (oDataPageControl.SelBin.Pallet) {
                        for (var i in oDataPageControl.SelBin.Pallet) {
                            oDataPageControl.SelBin.Pallet[i].IsSelect = false;
                        }
                    }
                }
                this.shareModel.refresh();
            },

            checkSelectBinHasSelectedItem: function () {
                var oPageControlData = this.shareModel.getContext("/PageControl").getObject();
                if (oPageControlData.SelBin) {
                    var aPallet = oPageControlData.SelBin.Pallet;
                    for (var i in aPallet) {
                        if (aPallet[i].IsSelect) {
                            return true;
                        }
                    }
                }
                return false;
            },

            transferPallet: function (oSourceBin, oDestBin) {

                for (var i = 0; i < oSourceBin.Pallet.length;) {
                    var oPallet = oSourceBin.Pallet[i];
                    if (oPallet.IsSelect) {
                        var oMovePallet = oSourceBin.Pallet.splice(i, 1)[0];
                        oMovePallet.IsSelect = false; //deselect
                        oDestBin.Pallet.push(oMovePallet);
                    } else {
                        i++;
                    }
                }

                //set highlight movement bin
                oSourceBin.IsHighLight = true;
                oDestBin.IsHighLight = true;
                this.shareModel.refresh();
                sap.m.MessageToast.show(this.getText("msgCreateMoveTaskSuccess"));
                //delay to dehighlight
                setTimeout(function () {
                    oSourceBin.IsHighLight = false;
                    oDestBin.IsHighLight = false;
                    this.shareModel.refresh();
                }.bind(this), 3000);

            },

            dePressToggleButton: function () {
                var aToggleButton = this.shareModel.getProperty("/PageControl/TransferToggleButton");
                for (var i in aToggleButton) {
                    var button = aToggleButton[i];
                    button.IsPress = false;
                }
                this.shareModel.refresh();
            },

        });
    });

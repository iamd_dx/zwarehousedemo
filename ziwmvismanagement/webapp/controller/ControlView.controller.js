// @ts-nocheck

sap.ui.define([
    "./BaseController",
    "../mockdata/PalletInBin",
    "../mockdata/WarehouseLayout"
],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, PalletInBin, WarehouseLayoutData) {
        "use strict";

        return Controller.extend("com.imove.iwm.ziwmvismanagement.controller.ControlView", {

            C_ACTION_NONE: "NONE",
            C_ACTION_BINSELECTED: "BINSELECTED",
            C_ACTION_PREPARETRANSFER: "PREPARETRANSFER",

            C_TRANS_MODE_INT: "INT_TRANSFER",
            C_TRANS_MODE_EXT: "EXT_TRANSFER",

            C_MODEL_NAME_SELWH: "SelWHModel",
            C_MODEL_NAME_FILTER: "FilterModel",
            C_MODEL_NAME_FILTER_GRID: "FilterGridModel",
            selWHModel: null,
            shareModel: null,
            pageCtx: null,

            onInit: function () {

                //init share model & context
                this.shareModel = this.getOwnerComponent().getShareModel();
                this.pageCtx = this.shareModel.getContext("/PageControl");
                this.getView().bindObject("ShareModel>/PageControl");

                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getTarget("ControlView").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));

                this.initPageModel();
                this.initTransferToggleButton();


            },

            handleRouteMatched: function (oEvent) {
                if (!this.pageCtx.getObject().SelWarehouse) {
                    setTimeout(function () {
                        this.openDialogSelectWarehouse();
                    }.bind(this), 1000);
                }
            },

            initPageModel: function () {
                this.selWHModel = new sap.ui.model.json.JSONModel();
                this.getView().setModel(this.selWHModel, this.C_MODEL_NAME_SELWH);

                this.filterModel = new sap.ui.model.json.JSONModel({
                    Zone : "",
                    Rack : "",
                    Mat : ""
                });
                this.getView().setModel(this.filterModel, this.C_MODEL_NAME_FILTER);

                this.filterGridModel = new sap.ui.model.json.JSONModel({
                    LabelSpan: "L2 M3 S4",
                    FieldSpan: "L2 M3 S8"
                });
                this.getView().setModel(this.filterGridModel, this.C_MODEL_NAME_FILTER_GRID);
            },

            initTransferToggleButton: function () {
                var oPageObj = this.pageCtx.getObject();
                oPageObj.TransferToggleButton = [
                    {
                        Key: this.C_TRANS_MODE_INT,
                        Text: "lbInternalTransfer",
                        IsPress: false
                    },
                    {
                        Key: this.C_TRANS_MODE_EXT,
                        Text: "lbExtTransfer",
                        IsPress: false
                    }
                ];
                this.shareModel.refresh();
            },

            onPressEditSelWarehouse: function (oEvent) {
                this.openDialogSelectWarehouse();
            },

            openDialogSelectWarehouse: function () {

                if (!this.oSelectWarehouseDialog) {
                    this.oSelectWarehouseDialog = sap.ui.xmlfragment("com.imove.iwm.ziwmvismanagement.view.fragment.DialogSelectWarehouse", this);
                    this.getView().addDependent(this.oSelectWarehouseDialog);
                }

                //default value
                var ctxObj = this.pageCtx.getObject();
                this.selWHModel.setData({
                    SelWarehouse: ctxObj.SelWarehouse ? ctxObj.SelWarehouse : ""
                });
                this.oSelectWarehouseDialog.open();

            },

            onPressCancelSelectWarehouse: function (oEvent) {
                this.oSelectWarehouseDialog.close();
            },

            onPressOKSelectWarehouse: function () {
                var oData = this.selWHModel.getData();
                var ctxObj = this.pageCtx.getObject();
                ctxObj.SelWarehouse = oData.SelWarehouse;
                ctxObj.SelArea = oData.SelArea;
                this.shareModel.refresh();
                this.oSelectWarehouseDialog.close();
                this.loadWarehouseLayout();
            },

            loadWarehouseLayout: function () {

                var oPageObj = this.pageCtx.getObject();
                var selWarehouse = oPageObj.SelWarehouse;
                var oData = JSON.parse(JSON.stringify(WarehouseLayoutData));

                var fnFilterArrayObjWarehouse = function (obj) {
                    if (obj.WarehouseNo === selWarehouse) {
                        return true;
                    }
                    return false;
                };

                var aLightPoint = oData.LightPoints.filter(fnFilterArrayObjWarehouse);
                var aWall = oData.WarehouseWalls.filter(fnFilterArrayObjWarehouse);
                var aBin = oData.StorageBins.filter(fnFilterArrayObjWarehouse);
                // aBin.forEach(function (item) {
                //     item.Color = this.generateBinColor(
                //         item.EmptyRatio,
                //         0, 100,
                //         0xFF6151, 0x51FF54);
                // }.bind(this));
                //update 3d model
                var oData3D = this.shareModel.getContext("/Page3D").getObject();
                oData3D.LightPointList = aLightPoint;
                oData3D.WallList = aWall;
                oData3D.BinList = aBin;
                this.shareModel.setSizeLimit(aLightPoint.length + aWall.length + aBin.length);
                this.shareModel.refresh();
                console.log(this.shareModel.getData());

                this.getOwnerComponent().get3DControl().attachEventOnce("onAfterBinUpdate", function () {
                    setTimeout(function () {
                         this.getOwnerComponent().get3DControl().gotoCenterViewport();
                    }.bind(this), 100);
                }.bind(this));
            },

            onCancelSelectBin: function () {
                var oPageObj = this.pageCtx.getObject();
                oPageObj.SelBin.IsHighLight = false;
                oPageObj.SelBin = null;
                oPageObj.Action = this.C_ACTION_NONE;
                oPageObj.TransferMode = "";
                this.shareModel.refresh();
            },

            onToggleTransferPress: function (oEvent) {
                var pressed = oEvent.getParameters().pressed;
                var oPressObj = oEvent.getSource().getBindingContext("ShareModel").getObject();
                var oPageObject = this.pageCtx.getObject();
                var aToggleButton = oPageObject.TransferToggleButton;

                if (pressed) {
                    // case press 
                    // depress other
                    this.dePressToggleButtonWithExcept([oPressObj.Key]);
                    oPageObject.TransferMode = oPressObj.Key;
                    oPageObject.Action = this.C_ACTION_PREPARETRANSFER;
                } else {
                    //case depress
                    // depress all
                    this.dePressToggleButtonWithExcept([]);
                    oPageObject.TransferMode = "";
                    oPageObject.Action = this.C_ACTION_BINSELECTED; // switch mode back to binselected
                }

                this.shareModel.refresh();
            },

            dePressToggleButtonWithExcept: function (aExcept) {
                var aToggleButton = this.pageCtx.getObject().TransferToggleButton;
                for (var i in aToggleButton) {
                    var button = aToggleButton[i];
                    if (!aExcept.includes(button.Key)) {
                        button.IsPress = false;
                    }
                }
                this.shareModel.refresh();
            },

            deselectAllPalletItem: function () {

            },

            onSearch: function (oEvent) {
                var aBin = this.shareModel.getProperty("/Page3D/BinList");

                var oFilterData = this.filterModel.getData();
                var defSelect, defOpac;
                if (oFilterData.Rack || oFilterData.Zone || oFilterData.Mat) {
                    defSelect = false;
                    defOpac = 0;
                } else {
                    defSelect = true;
                    defOpac = 0.9;
                }

                for (var i in aBin) {
                    var oBin = aBin[i];
                    // set default all bin 
                    oBin.Selectable = defSelect;
                    oBin.Opacity = defOpac;
                }
                var aFilterBin = this.filterBin(aBin, oFilterData.Zone, oFilterData.Rack , oFilterData.Mat);
                for (var i in aFilterBin) {
                    this.setBinAttributeFromFilter(aFilterBin[i]);
                }
                this.shareModel.refresh();

            },

            filterBin: function (aBin, sZone, sRack, sMat) {

                var aResult = aBin.filter(function (item) {
                    var aWhereBinSegment = sRack.split("-");
                    var sWhereRackStr = aWhereBinSegment[0] || "";
                    var sWhereRowStr = aWhereBinSegment[1] || "";
                    var sWhereColStr = aWhereBinSegment[2] || "";
                    var sWhereMatStr = sMat || "";
                    var aBinSegment = item.Bin.split("-");
                    var sRackStr = aBinSegment[0] || "";
                    var sRowStr = aBinSegment[1] || "";
                    var sColStr = aBinSegment[2] || "";

                    var fl1 = true;
                    var fl2 = true;
                    if (!!sWhereRackStr) var sWhereRackRegEx = new RegExp("^" + sWhereRackStr.replace(/\+/g, ".").replace(/\*/g, ".*"));
                    if (!!sWhereRowStr) var sWhereRowRegEx = new RegExp("^" + sWhereRowStr.replace(/\+/g, ".").replace(/\*/g, ".*"));
                    if (!!sWhereColStr) var sWhereColRegEx = new RegExp("^" + sWhereColStr.replace(/\+/g, ".").replace(/\*/g, ".*"));


                    if ((!sZone || item.ZoneId === sZone) &&
                        (!sWhereRackStr || sWhereRackRegEx.test(sRackStr)) &&
                        (!sWhereRowStr || sWhereRowRegEx.test(sRowStr)) &&
                        (!sWhereColStr || sWhereColRegEx.test(sColStr))
                    ) {
                        // return true;
                    } else {
                        // return false;
                        fl1 = false;
                    }

                    if (sWhereMatStr && item.Pallet ) {
                        var resultPallet = this.filterPallet(item.Pallet , sWhereMatStr );
                        if( resultPallet.length === 0 ){
                            fl2 = false;
                        }
                    }

                    return fl1 && fl2 ;

                }.bind(this));
                return aResult;
            },

            filterPallet: function (aPallet, sMat) {
                if (!!sMat) var sWhereMatRegEx = new RegExp("^" + sMat.replace(/\+/g, ".").replace(/\*/g, ".*"));
                var aResult = aPallet.filter(function( item ){
                    if( !sMat || sWhereMatRegEx.test( item.MatDesc) ){
                        return true;
                    }else{
                        return false;
                    }
                }.bind(this));
                return aResult;
            },

            setBinAttributeFromFilter: function (oBin) {
                oBin.Selectable = true;
                oBin.Opacity = 0.9;
            },

            onPressHome: function () {
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation"); // get a handle on the global XAppNav service
                oCrossAppNavigator.toExternal({
                    target: {
                        shellHash: "#"
                    }
                });
            },

            generateBinColor: function (value, minValue, maxValue, minColor, maxColor) {
                value = parseFloat(value);
                var vMinColorHex = ("000000" + minColor.toString(16)).slice(-6).toUpperCase();
                var vMaxColorHex = ("000000" + maxColor.toString(16)).slice(-6).toUpperCase();
                var vRatio = (value - minValue) / (maxValue - minValue);

                var vMinColorVectorR = parseInt(vMinColorHex.substr(0, 2), 16);
                var vMinColorVectorG = parseInt(vMinColorHex.substr(2, 2), 16);
                var vMinColorVectorB = parseInt(vMinColorHex.substr(4, 2), 16);

                var vMaxColorVectorR = parseInt(vMaxColorHex.substr(0, 2), 16);
                var vMaxColorVectorG = parseInt(vMaxColorHex.substr(2, 2), 16);
                var vMaxColorVectorB = parseInt(vMaxColorHex.substr(4, 2), 16);

                var vColorR = Math.floor(vMinColorVectorR + ((vMaxColorVectorR - vMinColorVectorR) * vRatio));
                var vColorG = Math.floor(vMinColorVectorG + ((vMaxColorVectorG - vMinColorVectorG) * vRatio));
                var vColorB = Math.floor(vMinColorVectorB + ((vMaxColorVectorB - vMinColorVectorB) * vRatio));

                var vColorHex = ("00" + vColorR.toString(16)).slice(-2) +
                    ("00" + vColorG.toString(16)).slice(-2) +
                    ("00" + vColorB.toString(16)).slice(-2);
                var vColor = parseInt(vColorHex, 16);

                return vColor;
            }

        });
    });

// @ts-nocheck
sap.ui.define([
    "sap/ui/core/UIComponent",
    "sap/ui/Device",
    "com/imove/iwm/ziwmvismanagement/model/models",
    "iam/control/binvisualize/src/js/three"
    // "iam.control.binvisualize.src.controls.TRDBinVisualize"
], function (UIComponent, Device, models) {
    "use strict";

    return UIComponent.extend("com.imove.iwm.ziwmvismanagement.Component", {

        metadata: {
            manifest: "json"
        },

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
        init: function () {
            // call the base component's init function
            UIComponent.prototype.init.apply(this, arguments);

            // enable routing
            this.getRouter().initialize();

            // set the device model
            this.setModel(models.createDeviceModel(), "device");
            this.initShareModel();
            this.hideLaunchpadShell();
        },

        initShareModel: function () {
            this.shareModel = new sap.ui.model.json.JSONModel({
                PageControl: {
                    Action: "NONE",
                    SelWarehouse: "",
                    SelBin: null,
                    TransferMode : "",
                    TransferToggleButton : []

                },
                Page3D: {
                    LightPointList: [],
                    WallList: [],
                    BinList: []
                }
            });
            this.setModel(this.shareModel, "ShareModel");
        },

        hideLaunchpadShell: function () {
            if (sap.ushell) {
                var oRenderer = sap.ushell.Container.getRenderer("fiori2");
                oRenderer.setHeaderVisibility(false, true);
            }
        },

        getShareModel: function () {
            return this.shareModel;
        },

        set3DControl: function (oCtrl) {
            this.__3dCtrlObject = oCtrl;
        },

        get3DControl: function () {
            return this.__3dCtrlObject || null;
        }
    });
});

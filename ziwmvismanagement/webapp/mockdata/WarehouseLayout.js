sap.ui.define([],

    function () {

        var data = {
            "LightPoints": [
                {
                    "WarehouseNo": "DC1",
                    "LightId": "01-001",
                    "Xpos": "4.0000",
                    "Ypos": "3.0000",
                    "Zpos": "3.0000",
                    "Color": "",
                    "Intensity": "1.0000",
                    "Distance": "1000",
                    "Decay": "2.0000"
                },
                {
                    "WarehouseNo": "DC1",
                    "LightId": "01-002",
                    "Xpos": "27.0000",
                    "Ypos": "3.0000",
                    "Zpos": "3.0000",
                    "Color": "",
                    "Intensity": "1.0000",
                    "Distance": "1000",
                    "Decay": "2.0000"
                },
                {
                    "WarehouseNo": "DC1",
                    "LightId": "02-003",
                    "Xpos": "-3.0000",
                    "Ypos": "17.0000",
                    "Zpos": "3.0000",
                    "Color": "",
                    "Intensity": "1.0000",
                    "Distance": "1000",
                    "Decay": "2.0000"
                },
                {
                    "WarehouseNo": "DC1",
                    "LightId": "02-001",
                    "Xpos": "-3.0000",
                    "Ypos": "3.0000",
                    "Zpos": "3.0000",
                    "Color": "",
                    "Intensity": "1.0000",
                    "Distance": "1000",
                    "Decay": "2.0000"
                },
                {
                    "WarehouseNo": "DC1",
                    "LightId": "01-003",
                    "Xpos": "28.0000",
                    "Ypos": "26.0000",
                    "Zpos": "3.0000",
                    "Color": "",
                    "Intensity": "1.0000",
                    "Distance": "1000",
                    "Decay": "2.0000"
                },
                {
                    "WarehouseNo": "DC1",
                    "LightId": "02-002",
                    "Xpos": "-28.0000",
                    "Ypos": "3.0000",
                    "Zpos": "3.0000",
                    "Color": "",
                    "Intensity": "1.0000",
                    "Distance": "1000",
                    "Decay": "2.0000"
                },
                {
                    "WarehouseNo": "DC1",
                    "LightId": "02-004",
                    "Xpos": "-28.0000",
                    "Ypos": "15.0000",
                    "Zpos": "3.0000",
                    "Color": "",
                    "Intensity": "1.0000",
                    "Distance": "1000",
                    "Decay": "2.0000"
                },
                {
                    "WarehouseNo": "DC1",
                    "LightId": "01-004",
                    "Xpos": "14.5000",
                    "Ypos": "26.0000",
                    "Zpos": "3.0000",
                    "Color": "",
                    "Intensity": "1.0000",
                    "Distance": "1000",
                    "Decay": "2.0000"
                }
            ],
            "StorageBins": [
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2JL-01-001",
                    "YDirection": "0.0000",
                    "XPos": "-23.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "2.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "222334/0819",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2JL-02-001",
                    "YDirection": "0.0000",
                    "XPos": "-23.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "2.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2JL-01-002",
                    "YDirection": "0.0000",
                    "XPos": "-23.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "832632/0834",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2JL-02-002",
                    "YDirection": "0.0000",
                    "XPos": "-23.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "326986/0137",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "462089/0439",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "736472/0832",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2JL-01-003",
                    "YDirection": "0.0000",
                    "XPos": "-23.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "624909/0062",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "342902/0661",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2JL-02-003",
                    "YDirection": "0.0000",
                    "XPos": "-23.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "640239/0112",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "097002/0658",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2JL-01-004",
                    "YDirection": "0.0000",
                    "XPos": "-23.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "5.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "163713/0713",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "651949/0625",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "604168/0749",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2JL-02-004",
                    "YDirection": "0.0000",
                    "XPos": "-23.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "5.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "287440/0603",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "927393/0589",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2JL-01-005",
                    "YDirection": "0.0000",
                    "XPos": "-23.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "6.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "334278/0582",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "578326/0426",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2JL-02-005",
                    "YDirection": "0.0000",
                    "XPos": "-23.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "6.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "794531/0631",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "874036/0027",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2JL-01-006",
                    "YDirection": "0.0000",
                    "XPos": "-23.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "288437/0107",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "526798/0801",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2JL-02-006",
                    "YDirection": "0.0000",
                    "XPos": "-23.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "558861/0451",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "927091/0275",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2JL-01-007",
                    "YDirection": "0.0000",
                    "XPos": "-23.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "710798/0631",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "029251/0318",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "551562/0768",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "213409/0305",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2JL-02-007",
                    "YDirection": "0.0000",
                    "XPos": "-23.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "991304/0686",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2JL-01-008",
                    "YDirection": "0.0000",
                    "XPos": "-23.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "9.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "224344/0945",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "813812/0794",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "106112/0614",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2JL-02-008",
                    "YDirection": "0.0000",
                    "XPos": "-23.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "9.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "871246/0180",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "213817/0910",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "205987/0284",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "491051/0586",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2JL-01-009",
                    "YDirection": "0.0000",
                    "XPos": "-23.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "10.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "363964/0522",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2JL-02-009",
                    "YDirection": "0.0000",
                    "XPos": "-23.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "10.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "467663/0695",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2JL-01-010",
                    "YDirection": "0.0000",
                    "XPos": "-23.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "064832/0402",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "148667/0350",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "525741/0257",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2JL-02-010",
                    "YDirection": "0.0000",
                    "XPos": "-23.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "036462/0112",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "570709/0505",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2JR-01-001",
                    "YDirection": "0.0000",
                    "XPos": "-22.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "2.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "102842/0489",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2JR-02-001",
                    "YDirection": "0.0000",
                    "XPos": "-22.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "2.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "888062/0053",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2JR-01-002",
                    "YDirection": "0.0000",
                    "XPos": "-22.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "720232/0635",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "687806/0113",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "379267/0728",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2JR-02-002",
                    "YDirection": "0.0000",
                    "XPos": "-22.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "105295/0294",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "816009/0487",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2JR-01-003",
                    "YDirection": "0.0000",
                    "XPos": "-22.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "991587/0603",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "801559/0666",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "276903/0872",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2JR-02-003",
                    "YDirection": "0.0000",
                    "XPos": "-22.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "836518/0071",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "433746/0722",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "281722/0064",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2JR-01-004",
                    "YDirection": "0.0000",
                    "XPos": "-22.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "5.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "178779/0842",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2JR-02-004",
                    "YDirection": "0.0000",
                    "XPos": "-22.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "5.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "915753/0021",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "152877/0479",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "055671/0656",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2JR-01-005",
                    "YDirection": "0.0000",
                    "XPos": "-22.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "6.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "845928/0474",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2JR-02-005",
                    "YDirection": "0.0000",
                    "XPos": "-22.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "6.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "393769/0820",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "792392/0399",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2JR-01-006",
                    "YDirection": "0.0000",
                    "XPos": "-22.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "688938/0445",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "618829/0800",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "513783/0516",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2JR-02-006",
                    "YDirection": "0.0000",
                    "XPos": "-22.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "107122/0770",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "761087/0798",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2JR-01-007",
                    "YDirection": "0.0000",
                    "XPos": "-22.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "380824/0506",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "249825/0225",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2JR-02-007",
                    "YDirection": "0.0000",
                    "XPos": "-22.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "730422/0943",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "672148/0346",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2JR-01-008",
                    "YDirection": "0.0000",
                    "XPos": "-22.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "9.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "381306/0603",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "041959/0416",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "663529/0407",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2JR-02-008",
                    "YDirection": "0.0000",
                    "XPos": "-22.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "9.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "140613/0704",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "011308/0233",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2JR-01-009",
                    "YDirection": "0.0000",
                    "XPos": "-22.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "10.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2JR-02-009",
                    "YDirection": "0.0000",
                    "XPos": "-22.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "10.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "635893/0896",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "335687/0016",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "569694/0944",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2JR-01-010",
                    "YDirection": "0.0000",
                    "XPos": "-22.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "769353/0964",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2JR-02-010",
                    "YDirection": "0.0000",
                    "XPos": "-22.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "191697/0874",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2IL-01-001",
                    "YDirection": "0.0000",
                    "XPos": "-19.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "2.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "113415/0921",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "771203/0167",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "103956/0420",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "700550/0565",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2IL-02-001",
                    "YDirection": "0.0000",
                    "XPos": "-19.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "2.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "896971/0668",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2IL-01-002",
                    "YDirection": "0.0000",
                    "XPos": "-19.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "972796/0350",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "397618/0560",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "386102/0676",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "049463/0262",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2IL-02-002",
                    "YDirection": "0.0000",
                    "XPos": "-19.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "019835/0970",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "584178/0167",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2IL-01-003",
                    "YDirection": "0.0000",
                    "XPos": "-19.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "358514/0142",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "467820/0890",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2IL-02-003",
                    "YDirection": "0.0000",
                    "XPos": "-19.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "151276/0590",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "202451/0777",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "129111/0951",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2IL-01-004",
                    "YDirection": "0.0000",
                    "XPos": "-19.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "5.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2IL-02-004",
                    "YDirection": "0.0000",
                    "XPos": "-19.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "5.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "897979/0279",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2IL-01-005",
                    "YDirection": "0.0000",
                    "XPos": "-19.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "6.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "439886/0203",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "804928/0506",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "263080/0743",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2IL-02-005",
                    "YDirection": "0.0000",
                    "XPos": "-19.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "6.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "557488/0414",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2IL-01-006",
                    "YDirection": "0.0000",
                    "XPos": "-19.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "347332/0017",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "518763/0194",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2IL-02-006",
                    "YDirection": "0.0000",
                    "XPos": "-19.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "322848/0529",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "342759/0918",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2IL-01-007",
                    "YDirection": "0.0000",
                    "XPos": "-19.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "829242/0823",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "522396/0910",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2IL-02-007",
                    "YDirection": "0.0000",
                    "XPos": "-19.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "837213/0338",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "563121/0432",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2IL-01-008",
                    "YDirection": "0.0000",
                    "XPos": "-19.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "9.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2IL-02-008",
                    "YDirection": "0.0000",
                    "XPos": "-19.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "9.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "994316/0364",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "192450/0907",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2IL-01-009",
                    "YDirection": "0.0000",
                    "XPos": "-19.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "10.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "479366/0076",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "923947/0381",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2IL-02-009",
                    "YDirection": "0.0000",
                    "XPos": "-19.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "10.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "547092/0286",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2IL-01-010",
                    "YDirection": "0.0000",
                    "XPos": "-19.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "672616/0349",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "349612/0131",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "681036/0760",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "680584/0583",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2IL-02-010",
                    "YDirection": "0.0000",
                    "XPos": "-19.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "126041/0178",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "264824/0847",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2IR-01-001",
                    "YDirection": "0.0000",
                    "XPos": "-18.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "2.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "074306/0002",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2IR-02-001",
                    "YDirection": "0.0000",
                    "XPos": "-18.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "2.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "368907/0171",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "951075/0772",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2IR-01-002",
                    "YDirection": "0.0000",
                    "XPos": "-18.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "467349/0988",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "181582/0661",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "392976/0502",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2IR-02-002",
                    "YDirection": "0.0000",
                    "XPos": "-18.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "720082/0574",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "748395/0771",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "192348/0027",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2IR-01-003",
                    "YDirection": "0.0000",
                    "XPos": "-18.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "002619/0968",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "998237/0162",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2IR-02-003",
                    "YDirection": "0.0000",
                    "XPos": "-18.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2IR-01-004",
                    "YDirection": "0.0000",
                    "XPos": "-18.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "5.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "770387/0268",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "300475/0982",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2IR-02-004",
                    "YDirection": "0.0000",
                    "XPos": "-18.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "5.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "051401/0150",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "083279/0009",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "077546/0214",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2IR-01-005",
                    "YDirection": "0.0000",
                    "XPos": "-18.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "6.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "868517/0813",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2IR-02-005",
                    "YDirection": "0.0000",
                    "XPos": "-18.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "6.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "868491/0070",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2IR-01-006",
                    "YDirection": "0.0000",
                    "XPos": "-18.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "013414/0997",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2IR-02-006",
                    "YDirection": "0.0000",
                    "XPos": "-18.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "968821/0796",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2IR-01-007",
                    "YDirection": "0.0000",
                    "XPos": "-18.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "638897/0345",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "776694/0171",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "053831/0298",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2IR-02-007",
                    "YDirection": "0.0000",
                    "XPos": "-18.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "698917/0371",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "194467/0086",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "709202/0183",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2IR-01-008",
                    "YDirection": "0.0000",
                    "XPos": "-18.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "9.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "714760/0636",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "949918/0187",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2IR-02-008",
                    "YDirection": "0.0000",
                    "XPos": "-18.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "9.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "941708/0135",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2IR-01-009",
                    "YDirection": "0.0000",
                    "XPos": "-18.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "10.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "655886/0996",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "112902/0324",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2IR-02-009",
                    "YDirection": "0.0000",
                    "XPos": "-18.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "10.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2IR-01-010",
                    "YDirection": "0.0000",
                    "XPos": "-18.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "513821/0197",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "137719/0682",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2IR-02-010",
                    "YDirection": "0.0000",
                    "XPos": "-18.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "575454/0473",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "282314/0485",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "741924/0045",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2HL-01-001",
                    "YDirection": "0.0000",
                    "XPos": "-15.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "2.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2HL-02-001",
                    "YDirection": "0.0000",
                    "XPos": "-15.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "2.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "310653/0406",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "692955/0838",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "690314/0180",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2HL-01-002",
                    "YDirection": "0.0000",
                    "XPos": "-15.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "291631/0296",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "867499/0485",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "698456/0106",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2HL-02-002",
                    "YDirection": "0.0000",
                    "XPos": "-15.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "886118/0359",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "165277/0729",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "686359/0130",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2HL-01-003",
                    "YDirection": "0.0000",
                    "XPos": "-15.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "937707/0384",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "040043/0091",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "481848/0808",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "864127/0216",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2HL-02-003",
                    "YDirection": "0.0000",
                    "XPos": "-15.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "372739/0848",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "190388/0020",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2HL-01-004",
                    "YDirection": "0.0000",
                    "XPos": "-15.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "5.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "610758/0168",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "855330/0242",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "933614/0774",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2HL-02-004",
                    "YDirection": "0.0000",
                    "XPos": "-15.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "5.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "866060/0622",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2HL-01-005",
                    "YDirection": "0.0000",
                    "XPos": "-15.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "6.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "657729/0691",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "843232/0907",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "561029/0207",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "825818/0788",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2HL-02-005",
                    "YDirection": "0.0000",
                    "XPos": "-15.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "6.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "595013/0720",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2HL-01-006",
                    "YDirection": "0.0000",
                    "XPos": "-15.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "199705/0950",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2HL-02-006",
                    "YDirection": "0.0000",
                    "XPos": "-15.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "167715/0708",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "613932/0904",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2HL-01-007",
                    "YDirection": "0.0000",
                    "XPos": "-15.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "635286/0038",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "491424/0797",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2HL-02-007",
                    "YDirection": "0.0000",
                    "XPos": "-15.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "933724/0112",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "351994/0458",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "781407/0109",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2HL-01-008",
                    "YDirection": "0.0000",
                    "XPos": "-15.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "9.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "304241/0638",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "484831/0579",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2HL-02-008",
                    "YDirection": "0.0000",
                    "XPos": "-15.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "9.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "451679/0697",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "428531/0734",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2HL-01-009",
                    "YDirection": "0.0000",
                    "XPos": "-15.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "10.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "594542/0371",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2HL-02-009",
                    "YDirection": "0.0000",
                    "XPos": "-15.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "10.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "184793/0823",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "805425/0031",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2HL-01-010",
                    "YDirection": "0.0000",
                    "XPos": "-15.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "450538/0327",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "522601/0441",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "067659/0143",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "743317/0370",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2HL-02-010",
                    "YDirection": "0.0000",
                    "XPos": "-15.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "720102/0925",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "004500/0107",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "050195/0381",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "707508/0754",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2HR-01-001",
                    "YDirection": "0.0000",
                    "XPos": "-14.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "2.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2HR-02-001",
                    "YDirection": "0.0000",
                    "XPos": "-14.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "2.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "231581/0206",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "278811/0513",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "002161/0871",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2HR-01-002",
                    "YDirection": "0.0000",
                    "XPos": "-14.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "347662/0110",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "701159/0059",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2HR-02-002",
                    "YDirection": "0.0000",
                    "XPos": "-14.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "459092/0811",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "487855/0215",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "836858/0573",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2HR-01-003",
                    "YDirection": "0.0000",
                    "XPos": "-14.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "709863/0029",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "788165/0647",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "921855/0213",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "028898/0814",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2HR-02-003",
                    "YDirection": "0.0000",
                    "XPos": "-14.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "609732/0688",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "825233/0475",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2HR-01-004",
                    "YDirection": "0.0000",
                    "XPos": "-14.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "5.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "430217/0645",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "994781/0105",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "097622/0490",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2HR-02-004",
                    "YDirection": "0.0000",
                    "XPos": "-14.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "5.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "730427/0248",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "936284/0560",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "875729/0566",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2HR-01-005",
                    "YDirection": "0.0000",
                    "XPos": "-14.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "6.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "568262/0148",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "967136/0551",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "290977/0444",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2HR-02-005",
                    "YDirection": "0.0000",
                    "XPos": "-14.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "6.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "576828/0966",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2HR-01-006",
                    "YDirection": "0.0000",
                    "XPos": "-14.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "325874/0334",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "743392/0704",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2HR-02-006",
                    "YDirection": "0.0000",
                    "XPos": "-14.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "522272/0918",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "032669/0437",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2HR-01-007",
                    "YDirection": "0.0000",
                    "XPos": "-14.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "436800/0118",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "416665/0723",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2HR-02-007",
                    "YDirection": "0.0000",
                    "XPos": "-14.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2HR-01-008",
                    "YDirection": "0.0000",
                    "XPos": "-14.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "9.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "361988/0830",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "736793/0465",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2HR-02-008",
                    "YDirection": "0.0000",
                    "XPos": "-14.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "9.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "427700/0954",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "830877/0049",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2HR-01-009",
                    "YDirection": "0.0000",
                    "XPos": "-14.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "10.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "368666/0883",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2HR-02-009",
                    "YDirection": "0.0000",
                    "XPos": "-14.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "10.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "330602/0844",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "520096/0430",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2HR-01-010",
                    "YDirection": "0.0000",
                    "XPos": "-14.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "391653/0538",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "255746/0974",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "743839/0753",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "901888/0514",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2HR-02-010",
                    "YDirection": "0.0000",
                    "XPos": "-14.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "471645/0303",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "395643/0768",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2GL-01-001",
                    "YDirection": "0.0000",
                    "XPos": "-11.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "2.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "213234/0777",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "755840/0473",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "523256/0383",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "872719/0068",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2GL-02-001",
                    "YDirection": "0.0000",
                    "XPos": "-11.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "2.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "304858/0731",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "215228/0180",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2GL-01-002",
                    "YDirection": "0.0000",
                    "XPos": "-11.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "287848/0487",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "506155/0982",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2GL-02-002",
                    "YDirection": "0.0000",
                    "XPos": "-11.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "170255/0238",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2GL-01-003",
                    "YDirection": "0.0000",
                    "XPos": "-11.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "695149/0582",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "457226/0959",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2GL-02-003",
                    "YDirection": "0.0000",
                    "XPos": "-11.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "105782/0637",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "142189/0640",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2GL-01-004",
                    "YDirection": "0.0000",
                    "XPos": "-11.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "5.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "275547/0535",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "451646/0241",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "594021/0628",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2GL-02-004",
                    "YDirection": "0.0000",
                    "XPos": "-11.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "5.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "679747/0980",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "806977/0177",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2GL-01-005",
                    "YDirection": "0.0000",
                    "XPos": "-11.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "6.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "124612/0050",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2GL-02-005",
                    "YDirection": "0.0000",
                    "XPos": "-11.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "6.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "776240/0146",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "623220/0479",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2GL-01-006",
                    "YDirection": "0.0000",
                    "XPos": "-11.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "252212/0757",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "802246/0293",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "098930/0473",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2GL-02-006",
                    "YDirection": "0.0000",
                    "XPos": "-11.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "718810/0990",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2GL-01-007",
                    "YDirection": "0.0000",
                    "XPos": "-11.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "589002/0106",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "903500/0000",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2GL-02-007",
                    "YDirection": "0.0000",
                    "XPos": "-11.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "451660/0977",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "382169/0793",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "626773/0014",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2GL-01-008",
                    "YDirection": "0.0000",
                    "XPos": "-11.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "9.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "994968/0884",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2GL-02-008",
                    "YDirection": "0.0000",
                    "XPos": "-11.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "9.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "678675/0844",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2GL-01-009",
                    "YDirection": "0.0000",
                    "XPos": "-11.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "10.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "526433/0491",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2GL-02-009",
                    "YDirection": "0.0000",
                    "XPos": "-11.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "10.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "687161/0033",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "444329/0980",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2GL-01-010",
                    "YDirection": "0.0000",
                    "XPos": "-11.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2GL-02-010",
                    "YDirection": "0.0000",
                    "XPos": "-11.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "251331/0169",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "098468/0331",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "510674/0121",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2GR-01-001",
                    "YDirection": "0.0000",
                    "XPos": "-10.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "2.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "821703/0168",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "417904/0528",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "133985/0321",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2GR-02-001",
                    "YDirection": "0.0000",
                    "XPos": "-10.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "2.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "419838/0845",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "621807/0312",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2GR-01-002",
                    "YDirection": "0.0000",
                    "XPos": "-10.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "461467/0122",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "454387/0326",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2GR-02-002",
                    "YDirection": "0.0000",
                    "XPos": "-10.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "362275/0601",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "140875/0278",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "881661/0935",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2GR-01-003",
                    "YDirection": "0.0000",
                    "XPos": "-10.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "589088/0032",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "886074/0077",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2GR-02-003",
                    "YDirection": "0.0000",
                    "XPos": "-10.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "270815/0553",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "214924/0261",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2GR-01-004",
                    "YDirection": "0.0000",
                    "XPos": "-10.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "5.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "799267/0567",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "464433/0815",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "720239/0001",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2GR-02-004",
                    "YDirection": "0.0000",
                    "XPos": "-10.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "5.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "809854/0423",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "056580/0923",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "541264/0775",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "439530/0408",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2GR-01-005",
                    "YDirection": "0.0000",
                    "XPos": "-10.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "6.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2GR-02-005",
                    "YDirection": "0.0000",
                    "XPos": "-10.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "6.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "710639/0836",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2GR-01-006",
                    "YDirection": "0.0000",
                    "XPos": "-10.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "112845/0743",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "785186/0942",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2GR-02-006",
                    "YDirection": "0.0000",
                    "XPos": "-10.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "622458/0688",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "899148/0891",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "125228/0664",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2GR-01-007",
                    "YDirection": "0.0000",
                    "XPos": "-10.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "071562/0585",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2GR-02-007",
                    "YDirection": "0.0000",
                    "XPos": "-10.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "193700/0273",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "003666/0764",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2GR-01-008",
                    "YDirection": "0.0000",
                    "XPos": "-10.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "9.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "328118/0410",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2GR-02-008",
                    "YDirection": "0.0000",
                    "XPos": "-10.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "9.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "086851/0853",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "714284/0107",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "074207/0968",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "793389/0610",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2GR-01-009",
                    "YDirection": "0.0000",
                    "XPos": "-10.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "10.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "513619/0658",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "785411/0201",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "911206/0993",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "535720/0432",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2GR-02-009",
                    "YDirection": "0.0000",
                    "XPos": "-10.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "10.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "242402/0228",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "810410/0050",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "541522/0294",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2GR-01-010",
                    "YDirection": "0.0000",
                    "XPos": "-10.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "434164/0081",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2GR-02-010",
                    "YDirection": "0.0000",
                    "XPos": "-10.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "805954/0684",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "613696/0928",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "TEMP-01",
                    "YDirection": "0.0000",
                    "XPos": "-8.0000",
                    "ZDirection": "1.0000",
                    "XWidth": "12.0000",
                    "YPos": "16.5000",
                    "YWidth": "5.0000",
                    "ZPos": "0.0050",
                    "ZWidth": "0.1000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "640393/0857",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "459806/0419",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2FL-01-001",
                    "YDirection": "0.0000",
                    "XPos": "-7.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "2.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "722548/0269",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "862390/0040",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "441989/0827",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2FL-02-001",
                    "YDirection": "0.0000",
                    "XPos": "-7.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "2.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "605472/0395",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2FL-01-002",
                    "YDirection": "0.0000",
                    "XPos": "-7.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "773178/0893",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2FL-02-002",
                    "YDirection": "0.0000",
                    "XPos": "-7.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2FL-01-003",
                    "YDirection": "0.0000",
                    "XPos": "-7.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2FL-02-003",
                    "YDirection": "0.0000",
                    "XPos": "-7.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "874022/0902",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "791726/0596",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2FL-01-004",
                    "YDirection": "0.0000",
                    "XPos": "-7.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "5.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "515053/0866",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "166784/0390",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "166516/0346",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2FL-02-004",
                    "YDirection": "0.0000",
                    "XPos": "-7.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "5.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "187325/0987",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "236392/0977",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "489639/0986",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2FL-01-005",
                    "YDirection": "0.0000",
                    "XPos": "-7.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "6.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "039718/0802",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "536578/0952",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2FL-02-005",
                    "YDirection": "0.0000",
                    "XPos": "-7.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "6.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "084953/0794",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "688126/0970",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2FL-01-006",
                    "YDirection": "0.0000",
                    "XPos": "-7.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "852633/0348",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2FL-02-006",
                    "YDirection": "0.0000",
                    "XPos": "-7.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "129132/0925",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "516900/0662",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2FL-01-007",
                    "YDirection": "0.0000",
                    "XPos": "-7.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "989196/0973",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2FL-02-007",
                    "YDirection": "0.0000",
                    "XPos": "-7.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "501209/0897",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "159502/0982",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2FL-01-008",
                    "YDirection": "0.0000",
                    "XPos": "-7.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "9.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "731024/0318",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "237070/0413",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2FL-02-008",
                    "YDirection": "0.0000",
                    "XPos": "-7.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "9.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "555578/0335",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "990930/0196",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2FL-01-009",
                    "YDirection": "0.0000",
                    "XPos": "-7.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "10.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "124861/0056",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2FL-02-009",
                    "YDirection": "0.0000",
                    "XPos": "-7.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "10.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "261791/0397",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "170875/0505",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "855022/0633",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2FL-01-010",
                    "YDirection": "0.0000",
                    "XPos": "-7.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "-1.0000",
                    "Bin": "2FL-02-010",
                    "YDirection": "0.0000",
                    "XPos": "-7.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "787157/0714",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "685940/0775",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2FR-01-001",
                    "YDirection": "0.0000",
                    "XPos": "-6.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "2.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2FR-02-001",
                    "YDirection": "0.0000",
                    "XPos": "-6.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "2.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2FR-01-002",
                    "YDirection": "0.0000",
                    "XPos": "-6.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "144153/0594",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2FR-02-002",
                    "YDirection": "0.0000",
                    "XPos": "-6.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "449782/0256",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "706085/0793",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2FR-01-003",
                    "YDirection": "0.0000",
                    "XPos": "-6.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "147985/0942",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "370232/0807",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2FR-02-003",
                    "YDirection": "0.0000",
                    "XPos": "-6.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "679475/0189",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "225915/0084",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "718655/0923",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2FR-01-004",
                    "YDirection": "0.0000",
                    "XPos": "-6.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "5.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "142746/0015",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "647679/0634",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2FR-02-004",
                    "YDirection": "0.0000",
                    "XPos": "-6.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "5.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "486766/0387",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "548267/0971",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2FR-01-005",
                    "YDirection": "0.0000",
                    "XPos": "-6.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "6.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "795407/0596",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "387223/0582",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2FR-02-005",
                    "YDirection": "0.0000",
                    "XPos": "-6.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "6.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "303994/0663",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2FR-01-006",
                    "YDirection": "0.0000",
                    "XPos": "-6.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "902269/0779",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "257498/0583",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "093906/0571",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2FR-02-006",
                    "YDirection": "0.0000",
                    "XPos": "-6.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "580769/0026",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2FR-01-007",
                    "YDirection": "0.0000",
                    "XPos": "-6.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "679869/0042",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "131105/0741",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2FR-02-007",
                    "YDirection": "0.0000",
                    "XPos": "-6.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2FR-01-008",
                    "YDirection": "0.0000",
                    "XPos": "-6.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "9.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "068864/0287",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "566394/0666",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2FR-02-008",
                    "YDirection": "0.0000",
                    "XPos": "-6.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "9.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "949819/0002",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "738827/0879",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2FR-01-009",
                    "YDirection": "0.0000",
                    "XPos": "-6.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "10.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "481015/0121",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "112372/0705",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2FR-02-009",
                    "YDirection": "0.0000",
                    "XPos": "-6.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "10.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "360519/0891",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "375832/0362",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2FR-01-010",
                    "YDirection": "0.0000",
                    "XPos": "-6.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "322118/0214",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "191450/0184",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "020054/0184",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "1.0000",
                    "Bin": "2FR-02-010",
                    "YDirection": "0.0000",
                    "XPos": "-6.0000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "044099/0041",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "012398/0852",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "985966/0933",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "B"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "TEMP-02",
                    "YDirection": "0.0000",
                    "XPos": "15.0000",
                    "ZDirection": "1.0000",
                    "XWidth": "6.0000",
                    "YPos": "24.5000",
                    "YWidth": "6.0000",
                    "ZPos": "0.0050",
                    "ZWidth": "0.1000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "956218/0694",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "402806/0249",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2EL-01-001",
                    "YDirection": "-1.0000",
                    "XPos": "19.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "232661/0736",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "135576/0508",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2EL-02-001",
                    "YDirection": "-1.0000",
                    "XPos": "19.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "795984/0955",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2ER-01-001",
                    "YDirection": "1.0000",
                    "XPos": "19.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "905425/0310",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "159654/0024",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2ER-02-001",
                    "YDirection": "1.0000",
                    "XPos": "19.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "568817/0116",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DL-01-001",
                    "YDirection": "-1.0000",
                    "XPos": "19.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "361167/0392",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "132225/0960",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DL-02-001",
                    "YDirection": "-1.0000",
                    "XPos": "19.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "055627/0432",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "021203/0897",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "430992/0766",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DR-01-001",
                    "YDirection": "1.0000",
                    "XPos": "19.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "306675/0213",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "527649/0932",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "355457/0295",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DR-02-001",
                    "YDirection": "1.0000",
                    "XPos": "19.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "469780/0220",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "421398/0752",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "663718/0786",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CL-01-001",
                    "YDirection": "-1.0000",
                    "XPos": "19.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "904820/0070",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "718981/0316",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "628207/0247",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "947623/0643",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CL-02-001",
                    "YDirection": "-1.0000",
                    "XPos": "19.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "708554/0776",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "950368/0321",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "103049/0938",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "844490/0542",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CR-01-001",
                    "YDirection": "1.0000",
                    "XPos": "19.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "12.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "464198/0717",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CR-02-001",
                    "YDirection": "1.0000",
                    "XPos": "19.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "12.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BL-01-001",
                    "YDirection": "-1.0000",
                    "XPos": "19.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "15.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "523364/0401",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "513470/0255",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "592307/0868",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BL-02-001",
                    "YDirection": "-1.0000",
                    "XPos": "19.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "15.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "289327/0260",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "351086/0460",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BR-01-001",
                    "YDirection": "1.0000",
                    "XPos": "19.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "16.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "195582/0664",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "186339/0413",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "742275/0950",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BR-02-001",
                    "YDirection": "1.0000",
                    "XPos": "19.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "16.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AL-01-001",
                    "YDirection": "-1.0000",
                    "XPos": "19.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "19.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.7500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "233264/0306",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AL-02-001",
                    "YDirection": "-1.0000",
                    "XPos": "19.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "19.5000",
                    "YWidth": "1.0000",
                    "ZPos": "2.2500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "832950/0328",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "112336/0829",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AR-01-001",
                    "YDirection": "1.0000",
                    "XPos": "19.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "20.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.7500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "249427/0818",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "216347/0752",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AR-02-001",
                    "YDirection": "1.0000",
                    "XPos": "19.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "20.5000",
                    "YWidth": "1.0000",
                    "ZPos": "2.2500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "900234/0593",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2EL-01-002",
                    "YDirection": "-1.0000",
                    "XPos": "20.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "636202/0385",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2EL-02-002",
                    "YDirection": "-1.0000",
                    "XPos": "20.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "361077/0701",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "050587/0874",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "352504/0046",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2ER-01-002",
                    "YDirection": "1.0000",
                    "XPos": "20.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "906504/0885",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "236430/0974",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "319238/0447",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "096316/0020",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2ER-02-002",
                    "YDirection": "1.0000",
                    "XPos": "20.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "735451/0619",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DL-01-002",
                    "YDirection": "-1.0000",
                    "XPos": "20.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "059070/0835",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "318333/0345",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DL-02-002",
                    "YDirection": "-1.0000",
                    "XPos": "20.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DR-01-002",
                    "YDirection": "1.0000",
                    "XPos": "20.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "400615/0057",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "392527/0229",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "750409/0667",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DR-02-002",
                    "YDirection": "1.0000",
                    "XPos": "20.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "982065/0195",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "986227/0419",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "917450/0755",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CL-01-002",
                    "YDirection": "-1.0000",
                    "XPos": "20.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "829907/0559",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "725721/0368",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "950514/0530",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CL-02-002",
                    "YDirection": "-1.0000",
                    "XPos": "20.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "958611/0983",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "737485/0418",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CR-01-002",
                    "YDirection": "1.0000",
                    "XPos": "20.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "12.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "716847/0725",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "042333/0674",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "078086/0705",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "779098/0453",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CR-02-002",
                    "YDirection": "1.0000",
                    "XPos": "20.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "12.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "649846/0428",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BL-01-002",
                    "YDirection": "-1.0000",
                    "XPos": "20.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "15.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "709797/0782",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BL-02-002",
                    "YDirection": "-1.0000",
                    "XPos": "20.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "15.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "758164/0969",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "764619/0498",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BR-01-002",
                    "YDirection": "1.0000",
                    "XPos": "20.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "16.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "678633/0996",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "815783/0891",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "717746/0820",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BR-02-002",
                    "YDirection": "1.0000",
                    "XPos": "20.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "16.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "816008/0068",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "463885/0023",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "032637/0001",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AL-01-002",
                    "YDirection": "-1.0000",
                    "XPos": "20.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "19.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.7500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "930126/0800",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "963195/0548",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AL-02-002",
                    "YDirection": "-1.0000",
                    "XPos": "20.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "19.5000",
                    "YWidth": "1.0000",
                    "ZPos": "2.2500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "170336/0488",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "442895/0828",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AR-01-002",
                    "YDirection": "1.0000",
                    "XPos": "20.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "20.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.7500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AR-02-002",
                    "YDirection": "1.0000",
                    "XPos": "20.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "20.5000",
                    "YWidth": "1.0000",
                    "ZPos": "2.2500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "201521/0591",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2EL-01-003",
                    "YDirection": "-1.0000",
                    "XPos": "21.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "647297/0810",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2EL-02-003",
                    "YDirection": "-1.0000",
                    "XPos": "21.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "450889/0990",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "454550/0731",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2ER-01-003",
                    "YDirection": "1.0000",
                    "XPos": "21.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "329670/0840",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "463496/0179",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2ER-02-003",
                    "YDirection": "1.0000",
                    "XPos": "21.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "024683/0667",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "776621/0589",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "656061/0927",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "470900/0239",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DL-01-003",
                    "YDirection": "-1.0000",
                    "XPos": "21.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "819402/0115",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DL-02-003",
                    "YDirection": "-1.0000",
                    "XPos": "21.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "321373/0013",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "309704/0571",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "704512/0522",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DR-01-003",
                    "YDirection": "1.0000",
                    "XPos": "21.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DR-02-003",
                    "YDirection": "1.0000",
                    "XPos": "21.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "001236/0795",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "302312/0046",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "926946/0068",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CL-01-003",
                    "YDirection": "-1.0000",
                    "XPos": "21.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "987294/0919",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "592931/0931",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CL-02-003",
                    "YDirection": "-1.0000",
                    "XPos": "21.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CR-01-003",
                    "YDirection": "1.0000",
                    "XPos": "21.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "12.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "123287/0339",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CR-02-003",
                    "YDirection": "1.0000",
                    "XPos": "21.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "12.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "553718/0348",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "971332/0867",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BL-01-003",
                    "YDirection": "-1.0000",
                    "XPos": "21.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "15.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BL-02-003",
                    "YDirection": "-1.0000",
                    "XPos": "21.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "15.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "741229/0470",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "853262/0478",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "271270/0625",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BR-01-003",
                    "YDirection": "1.0000",
                    "XPos": "21.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "16.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "180796/0541",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "252812/0943",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "592249/0778",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BR-02-003",
                    "YDirection": "1.0000",
                    "XPos": "21.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "16.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "478379/0619",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "539687/0650",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "740634/0377",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AL-01-003",
                    "YDirection": "-1.0000",
                    "XPos": "21.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "19.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.7500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AL-02-003",
                    "YDirection": "-1.0000",
                    "XPos": "21.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "19.5000",
                    "YWidth": "1.0000",
                    "ZPos": "2.2500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "784452/0782",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "316378/0852",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "513233/0055",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "465075/0133",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AR-01-003",
                    "YDirection": "1.0000",
                    "XPos": "21.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "20.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.7500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "141143/0713",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "568983/0134",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "583796/0388",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AR-02-003",
                    "YDirection": "1.0000",
                    "XPos": "21.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "20.5000",
                    "YWidth": "1.0000",
                    "ZPos": "2.2500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2EL-01-004",
                    "YDirection": "-1.0000",
                    "XPos": "22.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "058206/0808",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "290038/0221",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2EL-02-004",
                    "YDirection": "-1.0000",
                    "XPos": "22.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "456807/0538",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2ER-01-004",
                    "YDirection": "1.0000",
                    "XPos": "22.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "470870/0760",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2ER-02-004",
                    "YDirection": "1.0000",
                    "XPos": "22.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "868835/0275",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "231639/0454",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "604174/0252",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DL-01-004",
                    "YDirection": "-1.0000",
                    "XPos": "22.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DL-02-004",
                    "YDirection": "-1.0000",
                    "XPos": "22.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "691723/0363",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "458943/0554",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DR-01-004",
                    "YDirection": "1.0000",
                    "XPos": "22.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "831139/0694",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "782457/0232",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DR-02-004",
                    "YDirection": "1.0000",
                    "XPos": "22.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "597481/0576",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "635208/0711",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CL-01-004",
                    "YDirection": "-1.0000",
                    "XPos": "22.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "408012/0034",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "649892/0959",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CL-02-004",
                    "YDirection": "-1.0000",
                    "XPos": "22.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "847288/0290",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CR-01-004",
                    "YDirection": "1.0000",
                    "XPos": "22.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "12.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CR-02-004",
                    "YDirection": "1.0000",
                    "XPos": "22.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "12.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "225714/0628",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BL-01-004",
                    "YDirection": "-1.0000",
                    "XPos": "22.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "15.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "053889/0407",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "139256/0289",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "407385/0086",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BL-02-004",
                    "YDirection": "-1.0000",
                    "XPos": "22.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "15.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BR-01-004",
                    "YDirection": "1.0000",
                    "XPos": "22.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "16.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "914009/0505",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "100595/0937",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BR-02-004",
                    "YDirection": "1.0000",
                    "XPos": "22.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "16.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "152529/0048",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AL-01-004",
                    "YDirection": "-1.0000",
                    "XPos": "22.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "19.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.7500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "311443/0874",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "326371/0095",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "989486/0403",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AL-02-004",
                    "YDirection": "-1.0000",
                    "XPos": "22.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "19.5000",
                    "YWidth": "1.0000",
                    "ZPos": "2.2500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AR-01-004",
                    "YDirection": "1.0000",
                    "XPos": "22.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "20.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.7500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "281209/0150",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AR-02-004",
                    "YDirection": "1.0000",
                    "XPos": "22.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "20.5000",
                    "YWidth": "1.0000",
                    "ZPos": "2.2500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "531453/0583",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "167940/0053",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2EL-01-005",
                    "YDirection": "-1.0000",
                    "XPos": "23.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "758305/0797",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2EL-02-005",
                    "YDirection": "-1.0000",
                    "XPos": "23.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "958876/0088",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "394271/0377",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "199816/0649",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2ER-01-005",
                    "YDirection": "1.0000",
                    "XPos": "23.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "922765/0963",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2ER-02-005",
                    "YDirection": "1.0000",
                    "XPos": "23.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "767597/0274",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "682570/0382",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "391552/0125",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DL-01-005",
                    "YDirection": "-1.0000",
                    "XPos": "23.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DL-02-005",
                    "YDirection": "-1.0000",
                    "XPos": "23.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "233151/0825",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "474859/0008",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DR-01-005",
                    "YDirection": "1.0000",
                    "XPos": "23.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "329357/0096",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "255986/0380",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "219545/0498",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DR-02-005",
                    "YDirection": "1.0000",
                    "XPos": "23.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "301923/0670",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CL-01-005",
                    "YDirection": "-1.0000",
                    "XPos": "23.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "774840/0875",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "378168/0142",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "417223/0063",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "862621/0439",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CL-02-005",
                    "YDirection": "-1.0000",
                    "XPos": "23.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "214402/0390",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CR-01-005",
                    "YDirection": "1.0000",
                    "XPos": "23.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "12.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "108711/0063",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "954928/0861",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "803050/0817",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "619324/0584",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CR-02-005",
                    "YDirection": "1.0000",
                    "XPos": "23.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "12.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "574239/0669",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "947095/0813",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "977731/0620",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BL-01-005",
                    "YDirection": "-1.0000",
                    "XPos": "23.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "15.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "497834/0198",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BL-02-005",
                    "YDirection": "-1.0000",
                    "XPos": "23.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "15.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "750408/0014",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BR-01-005",
                    "YDirection": "1.0000",
                    "XPos": "23.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "16.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "825865/0023",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "452271/0796",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "644012/0892",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BR-02-005",
                    "YDirection": "1.0000",
                    "XPos": "23.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "16.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "530899/0824",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AL-01-005",
                    "YDirection": "-1.0000",
                    "XPos": "23.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "19.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.7500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "725052/0792",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AL-02-005",
                    "YDirection": "-1.0000",
                    "XPos": "23.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "19.5000",
                    "YWidth": "1.0000",
                    "ZPos": "2.2500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "161696/0721",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "088153/0815",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AR-01-005",
                    "YDirection": "1.0000",
                    "XPos": "23.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "20.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.7500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "213875/0869",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "108946/0285",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "642931/0468",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AR-02-005",
                    "YDirection": "1.0000",
                    "XPos": "23.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "20.5000",
                    "YWidth": "1.0000",
                    "ZPos": "2.2500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "388113/0542",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2EL-01-006",
                    "YDirection": "-1.0000",
                    "XPos": "24.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "292470/0455",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "312400/0047",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2EL-02-006",
                    "YDirection": "-1.0000",
                    "XPos": "24.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "900448/0202",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "272142/0986",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "963348/0375",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "729271/0896",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2ER-01-006",
                    "YDirection": "1.0000",
                    "XPos": "24.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "899355/0856",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "555293/0460",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "652447/0106",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2ER-02-006",
                    "YDirection": "1.0000",
                    "XPos": "24.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "905485/0160",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "436653/0364",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DL-01-006",
                    "YDirection": "-1.0000",
                    "XPos": "24.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "861352/0142",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "780423/0000",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "339296/0135",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "078190/0113",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DL-02-006",
                    "YDirection": "-1.0000",
                    "XPos": "24.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "918069/0829",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "151862/0750",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "441322/0955",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "561313/0471",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DR-01-006",
                    "YDirection": "1.0000",
                    "XPos": "24.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "936047/0216",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "054248/0184",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DR-02-006",
                    "YDirection": "1.0000",
                    "XPos": "24.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "085295/0462",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "817877/0205",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CL-01-006",
                    "YDirection": "-1.0000",
                    "XPos": "24.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CL-02-006",
                    "YDirection": "-1.0000",
                    "XPos": "24.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CR-01-006",
                    "YDirection": "1.0000",
                    "XPos": "24.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "12.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "248354/0699",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "663968/0807",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "019724/0785",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "973668/0864",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CR-02-006",
                    "YDirection": "1.0000",
                    "XPos": "24.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "12.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "667492/0731",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "693843/0165",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BL-01-006",
                    "YDirection": "-1.0000",
                    "XPos": "24.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "15.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BL-02-006",
                    "YDirection": "-1.0000",
                    "XPos": "24.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "15.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "470963/0263",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "730857/0227",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "206570/0560",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BR-01-006",
                    "YDirection": "1.0000",
                    "XPos": "24.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "16.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "032771/0740",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BR-02-006",
                    "YDirection": "1.0000",
                    "XPos": "24.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "16.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "963946/0997",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "079998/0677",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "577641/0312",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AL-01-006",
                    "YDirection": "-1.0000",
                    "XPos": "24.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "19.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.7500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "190383/0770",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "202198/0140",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "634504/0109",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "399976/0629",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AL-02-006",
                    "YDirection": "-1.0000",
                    "XPos": "24.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "19.5000",
                    "YWidth": "1.0000",
                    "ZPos": "2.2500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "531504/0599",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AR-01-006",
                    "YDirection": "1.0000",
                    "XPos": "24.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "20.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.7500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "917359/0586",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "061134/0211",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "869165/0669",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "978179/0036",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AR-02-006",
                    "YDirection": "1.0000",
                    "XPos": "24.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "20.5000",
                    "YWidth": "1.0000",
                    "ZPos": "2.2500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "716900/0711",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2EL-01-007",
                    "YDirection": "-1.0000",
                    "XPos": "25.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "166877/0268",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "016602/0809",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "480245/0475",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2EL-02-007",
                    "YDirection": "-1.0000",
                    "XPos": "25.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "905809/0279",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2ER-01-007",
                    "YDirection": "1.0000",
                    "XPos": "25.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2ER-02-007",
                    "YDirection": "1.0000",
                    "XPos": "25.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "897618/0399",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "039808/0167",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "169520/0461",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DL-01-007",
                    "YDirection": "-1.0000",
                    "XPos": "25.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "901386/0788",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DL-02-007",
                    "YDirection": "-1.0000",
                    "XPos": "25.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "920369/0990",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "017772/0553",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DR-01-007",
                    "YDirection": "1.0000",
                    "XPos": "25.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "972820/0385",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "398150/0486",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "447159/0601",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DR-02-007",
                    "YDirection": "1.0000",
                    "XPos": "25.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "793007/0056",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "723940/0942",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CL-01-007",
                    "YDirection": "-1.0000",
                    "XPos": "25.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "119304/0142",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CL-02-007",
                    "YDirection": "-1.0000",
                    "XPos": "25.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CR-01-007",
                    "YDirection": "1.0000",
                    "XPos": "25.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "12.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "369481/0407",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CR-02-007",
                    "YDirection": "1.0000",
                    "XPos": "25.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "12.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BL-01-007",
                    "YDirection": "-1.0000",
                    "XPos": "25.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "15.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "716009/0856",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BL-02-007",
                    "YDirection": "-1.0000",
                    "XPos": "25.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "15.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "306961/0993",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "098410/0669",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BR-01-007",
                    "YDirection": "1.0000",
                    "XPos": "25.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "16.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "658116/0519",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "427845/0288",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "655742/0053",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BR-02-007",
                    "YDirection": "1.0000",
                    "XPos": "25.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "16.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "086003/0970",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "061672/0341",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AL-01-007",
                    "YDirection": "-1.0000",
                    "XPos": "25.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "19.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.7500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "262362/0180",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "733459/0828",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AL-02-007",
                    "YDirection": "-1.0000",
                    "XPos": "25.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "19.5000",
                    "YWidth": "1.0000",
                    "ZPos": "2.2500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "967453/0929",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AR-01-007",
                    "YDirection": "1.0000",
                    "XPos": "25.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "20.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.7500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "451728/0680",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AR-02-007",
                    "YDirection": "1.0000",
                    "XPos": "25.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "20.5000",
                    "YWidth": "1.0000",
                    "ZPos": "2.2500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "770249/0908",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "482037/0759",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2EL-01-008",
                    "YDirection": "-1.0000",
                    "XPos": "26.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "770815/0171",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2EL-02-008",
                    "YDirection": "-1.0000",
                    "XPos": "26.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "781212/0015",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "393616/0119",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "952510/0049",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "553511/0335",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2ER-01-008",
                    "YDirection": "1.0000",
                    "XPos": "26.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "153843/0674",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2ER-02-008",
                    "YDirection": "1.0000",
                    "XPos": "26.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "900946/0851",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "501064/0926",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "842464/0037",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "332033/0673",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DL-01-008",
                    "YDirection": "-1.0000",
                    "XPos": "26.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DL-02-008",
                    "YDirection": "-1.0000",
                    "XPos": "26.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "420412/0909",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "629694/0715",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "106927/0551",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DR-01-008",
                    "YDirection": "1.0000",
                    "XPos": "26.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "188560/0762",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DR-02-008",
                    "YDirection": "1.0000",
                    "XPos": "26.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "046471/0466",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "603812/0805",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "923796/0567",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CL-01-008",
                    "YDirection": "-1.0000",
                    "XPos": "26.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "387141/0305",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "409117/0668",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CL-02-008",
                    "YDirection": "-1.0000",
                    "XPos": "26.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "720305/0199",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "403673/0263",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "573033/0050",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CR-01-008",
                    "YDirection": "1.0000",
                    "XPos": "26.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "12.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "156334/0328",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CR-02-008",
                    "YDirection": "1.0000",
                    "XPos": "26.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "12.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "964767/0959",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BL-01-008",
                    "YDirection": "-1.0000",
                    "XPos": "26.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "15.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "928685/0179",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "816957/0147",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "580042/0560",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BL-02-008",
                    "YDirection": "-1.0000",
                    "XPos": "26.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "15.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "385586/0018",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "297030/0646",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BR-01-008",
                    "YDirection": "1.0000",
                    "XPos": "26.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "16.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "853090/0571",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "401561/0238",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "029380/0233",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BR-02-008",
                    "YDirection": "1.0000",
                    "XPos": "26.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "16.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "873219/0089",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "873918/0015",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "422833/0417",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AL-01-008",
                    "YDirection": "-1.0000",
                    "XPos": "26.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "19.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.7500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "616215/0311",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AL-02-008",
                    "YDirection": "-1.0000",
                    "XPos": "26.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "19.5000",
                    "YWidth": "1.0000",
                    "ZPos": "2.2500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "648347/0095",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "510991/0743",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AR-01-008",
                    "YDirection": "1.0000",
                    "XPos": "26.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "20.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.7500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "314259/0069",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "938007/0615",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "119405/0535",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AR-02-008",
                    "YDirection": "1.0000",
                    "XPos": "26.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "20.5000",
                    "YWidth": "1.0000",
                    "ZPos": "2.2500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "591211/0449",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2EL-01-009",
                    "YDirection": "-1.0000",
                    "XPos": "27.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "880834/0032",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "542189/0907",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "294840/0029",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2EL-02-009",
                    "YDirection": "-1.0000",
                    "XPos": "27.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "074089/0644",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "656586/0594",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "868038/0437",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2ER-01-009",
                    "YDirection": "1.0000",
                    "XPos": "27.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "950838/0212",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "703599/0658",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2ER-02-009",
                    "YDirection": "1.0000",
                    "XPos": "27.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "061621/0596",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "967456/0362",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DL-01-009",
                    "YDirection": "-1.0000",
                    "XPos": "27.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "371404/0924",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "936333/0694",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "490170/0088",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DL-02-009",
                    "YDirection": "-1.0000",
                    "XPos": "27.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "497418/0487",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "690767/0528",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "604297/0076",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DR-01-009",
                    "YDirection": "1.0000",
                    "XPos": "27.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "908733/0072",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "001288/0006",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DR-02-009",
                    "YDirection": "1.0000",
                    "XPos": "27.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "330671/0328",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "125375/0393",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "536090/0765",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CL-01-009",
                    "YDirection": "-1.0000",
                    "XPos": "27.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "445722/0036",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "080573/0567",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "994618/0307",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "027572/0901",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CL-02-009",
                    "YDirection": "-1.0000",
                    "XPos": "27.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CR-01-009",
                    "YDirection": "1.0000",
                    "XPos": "27.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "12.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "211140/0646",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CR-02-009",
                    "YDirection": "1.0000",
                    "XPos": "27.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "12.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "907845/0118",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "198099/0711",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BL-01-009",
                    "YDirection": "-1.0000",
                    "XPos": "27.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "15.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "007917/0449",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "906021/0886",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "222121/0768",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BL-02-009",
                    "YDirection": "-1.0000",
                    "XPos": "27.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "15.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "713546/0510",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "697039/0259",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "619701/0763",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "385806/0089",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BR-01-009",
                    "YDirection": "1.0000",
                    "XPos": "27.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "16.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "310294/0797",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BR-02-009",
                    "YDirection": "1.0000",
                    "XPos": "27.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "16.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "534458/0283",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AL-01-009",
                    "YDirection": "-1.0000",
                    "XPos": "27.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "19.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.7500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "856116/0669",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AL-02-009",
                    "YDirection": "-1.0000",
                    "XPos": "27.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "19.5000",
                    "YWidth": "1.0000",
                    "ZPos": "2.2500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "687224/0893",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "223233/0241",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AR-01-009",
                    "YDirection": "1.0000",
                    "XPos": "27.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "20.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.7500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "640955/0690",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "510237/0041",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "268706/0010",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "783407/0147",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AR-02-009",
                    "YDirection": "1.0000",
                    "XPos": "27.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "20.5000",
                    "YWidth": "1.0000",
                    "ZPos": "2.2500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "409557/0982",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "193743/0027",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2EL-01-010",
                    "YDirection": "-1.0000",
                    "XPos": "28.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "247364/0567",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "462660/0768",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "581118/0407",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2EL-02-010",
                    "YDirection": "-1.0000",
                    "XPos": "28.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "3.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "048338/0738",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "666638/0720",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "235426/0851",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2ER-01-010",
                    "YDirection": "1.0000",
                    "XPos": "28.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "963893/0661",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "362414/0824",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "096816/0544",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2ER-02-010",
                    "YDirection": "1.0000",
                    "XPos": "28.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "4.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "051445/0052",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "725653/0010",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DL-01-010",
                    "YDirection": "-1.0000",
                    "XPos": "28.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "173012/0980",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "961695/0535",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DL-02-010",
                    "YDirection": "-1.0000",
                    "XPos": "28.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "7.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "595094/0350",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DR-01-010",
                    "YDirection": "1.0000",
                    "XPos": "28.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "886842/0198",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "979458/0945",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "948350/0582",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "118713/0634",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2DR-02-010",
                    "YDirection": "1.0000",
                    "XPos": "28.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "8.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "065539/0989",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "805851/0228",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CL-01-010",
                    "YDirection": "-1.0000",
                    "XPos": "28.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "739962/0299",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "410896/0559",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "798349/0856",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CL-02-010",
                    "YDirection": "-1.0000",
                    "XPos": "28.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "11.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "945254/0049",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "195917/0410",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "066671/0348",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CR-01-010",
                    "YDirection": "1.0000",
                    "XPos": "28.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "12.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "139522/0968",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "300186/0795",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "095938/0007",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2CR-02-010",
                    "YDirection": "1.0000",
                    "XPos": "28.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "12.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "030283/0774",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "682919/0916",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BL-01-010",
                    "YDirection": "-1.0000",
                    "XPos": "28.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "15.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "861569/0329",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BL-02-010",
                    "YDirection": "-1.0000",
                    "XPos": "28.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "15.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "072765/0074",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "194428/0987",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "552046/0116",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "310658/0486",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BR-01-010",
                    "YDirection": "1.0000",
                    "XPos": "28.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "16.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "827901/0991",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2BR-02-010",
                    "YDirection": "1.0000",
                    "XPos": "28.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "16.5000",
                    "YWidth": "1.0000",
                    "ZPos": "1.5000",
                    "ZWidth": "1.0000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "564900/0414",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "217788/0395",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AL-01-010",
                    "YDirection": "-1.0000",
                    "XPos": "28.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "19.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.7500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "490531/0650",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "902688/0619",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "236490/0235",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AL-02-010",
                    "YDirection": "-1.0000",
                    "XPos": "28.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "19.5000",
                    "YWidth": "1.0000",
                    "ZPos": "2.2500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "221602/0256",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "404019/0893",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "132061/0939",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "642266/0562",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AR-01-010",
                    "YDirection": "1.0000",
                    "XPos": "28.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "20.5000",
                    "YWidth": "1.0000",
                    "ZPos": "0.7500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "672957/0139",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                },
                {
                    "MaterialCode": "",
                    "WarehouseNo": "DC1",
                    "XDirection": "0.0000",
                    "Bin": "2AR-02-010",
                    "YDirection": "1.0000",
                    "XPos": "28.5000",
                    "ZDirection": "0.0000",
                    "XWidth": "1.0000",
                    "YPos": "20.5000",
                    "YWidth": "1.0000",
                    "ZPos": "2.2500",
                    "ZWidth": "1.5000",
                    "EmptyRatio": "100.0000",
                    "Pallet": [
                        {
                            "MatId": "10201",
                            "MatDesc": "ซอสหอยนางรม 600 ml.",
                            "Batch": "554369/0090",
                            "Qty": "64",
                            "Unit": "BOX"
                        },
                        {
                            "MatId": "10101",
                            "MatDesc": "ซีอิ้วขาว 500 ml.",
                            "Batch": "826391/0564",
                            "Qty": "64",
                            "Unit": "BOX"
                        }
                    ],
                    "ZoneId": "A"
                }
            ],
            "WarehouseWalls": [
                {
                    "WarehouseNo": "DC1",
                    "ObjectId": "W2F00003",
                    "Description": "",
                    "Remark": "",
                    "Xpos": "-15.2500",
                    "Xwidth": "30.0000",
                    "Ypos": "10.0000",
                    "Ywidth": "19.0000",
                    "Zpos": "-0.0001",
                    "Zwidth": "0.0010",
                    "Xdir": "0.0000",
                    "Ydir": "0.0000",
                    "Zdir": "-1.0000",
                    "NoEdge": true
                },
                {
                    "WarehouseNo": "DC1",
                    "ObjectId": "W2F00002",
                    "Description": "",
                    "Remark": "",
                    "Xpos": "5.5000",
                    "Xwidth": "10.5000",
                    "Ypos": "4.6000",
                    "Ywidth": "8.3000",
                    "Zpos": "-0.0001",
                    "Zwidth": "0.0010",
                    "Xdir": "0.0000",
                    "Ydir": "0.0000",
                    "Zdir": "-1.0000",
                    "NoEdge": true
                },
                {
                    "WarehouseNo": "DC1",
                    "ObjectId": "W2F00001",
                    "Description": "",
                    "Remark": "",
                    "Xpos": "20.7500",
                    "Xwidth": "20.0000",
                    "Ypos": "14.3500",
                    "Ywidth": "27.8000",
                    "Zpos": "-0.0001",
                    "Zwidth": "0.0010",
                    "Xdir": "0.0000",
                    "Ydir": "0.0000", 
                    "Zdir": "-1.0000",
                    "NoEdge": true
                }
            ]
        };

        return data;

    });


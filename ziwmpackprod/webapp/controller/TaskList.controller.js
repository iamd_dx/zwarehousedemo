// @ts-nocheck
sap.ui.define([
    "sap/ui/core/mvc/Controller"
],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller) {
        "use strict";

        return Controller.extend("com.imove.iwm.ziwmpackprod.controller.TaskList", {
            mScrModel: new sap.ui.model.json.JSONModel({}, true),
            mSelWhModel: new sap.ui.model.json.JSONModel({}, true),

            onInit: function () {
                this.oPage = this.getView().byId("idPage");
                this.mScrModel.setProperty("/TaskList", []);

                this.getOwnerComponent().setModel(this.mScrModel, "mScrModel");
                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getTarget("TaskList").attachDisplay(this.onRouteMatched.bind(this));
            },

            onRouteMatched: function (oEvent) {
                this.initialDataModel();

                if (!this.mScrModel.getProperty("/WarehouseNo")) {
                    setTimeout(function () {
                        this.openDialogSelectWarehouse();
                    }.bind(this), 1000);
                } else {
                    this.turnOnListenScan();
                    this.getTaskByLocation();
                }
            },

            onNavBack: function (oEvent) {
                var oHistory = sap.ui.core.routing.History.getInstance();
                var sPreviousHash = oHistory.getPreviousHash();
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                if (sPreviousHash !== undefined || !oCrossAppNavigator.isInitialNavigation()) {
                    history.go(-1);
                } else {
                    oCrossAppNavigator.toExternal({
                        target: { shellHash: "#Shell-home" }
                    });
                }
            },

            onChangedBarcode: function (oEvent) {
                this.filterTaskList(oEvent.getSource().getValue());
            },

            onSelectedTask: function (oEvent) {
                var oSource = oEvent.getSource();
                var oSelected = oSource.getSelectedItem();
                var oData = oSelected.getBindingContext("mScrModel").getObject();
                oSource.removeSelections();
                this.mScrModel.setProperty("/SelectedTask", oData);
                this.displayOrderList();
            },

            onPressEditSelWarehouse: function (oEvent) {
                this.openDialogSelectWarehouse();
            },

            onPressCancelSelectWarehouse: function (oEvent) {
                this.oSelectWarehouseDialog.close();
            },

            onPressOKSelectWarehouse: function () {
                var oSelWh = this.mSelWhModel.getData();
                this.mScrModel.setProperty("/WarehouseNo", oSelWh.SelWarehouse);
                this.mScrModel.setProperty("/PackingArea", oSelWh.SelPackingArea);
                this.oSelectWarehouseDialog.close();
                var sBarcode = this.getView().byId("idBarcode").getValue();
                this.getTaskByLocation();
            },



            initialDataModel: function () {
                
            },

            insertSpecialDescription: function () {
                var aTaskList = this.mScrModel.getProperty("/TaskList");
                aTaskList.forEach(function (oItem) {
                    if (oItem.PickupQuantity !== oItem.PalletQuantity) oItem.SpecialDescription = "นำพาเลทใหม่ไปด้วย";
                });
                this.mScrModel.setProperty("/TaskList", aTaskList);
            },

            turnOnListenScan: function () {
                var $input = $("input", this.getView().byId("idBarcode").getDomRef());
                $input.on("blur", function (oEvent) {
                    this.focusScanner();
                }.bind(this));

                this.focusScanner();
            },

            turnOffListenScan: function () {
                var $input = $("input", this.getView().byId("idBarcode").getDomRef());
                $input.off("blur");
            },

            getTaskByLocation: function () {
                var sWarehouseNo = this.mScrModel.getProperty("/WarehouseNo");
                var sPackingArea = this.mScrModel.getProperty("/PackingArea");
                this.mScrModel.setProperty("/TaskList", this.getOwnerComponent().getWarehouseTaskbyLocation({
                    WarehouseNo: sWarehouseNo,
                    PackingArea: sPackingArea
                }));
                this.insertSpecialDescription();
            },

            filterTaskList: function (sOrder) {
                var oLstCtrl = this.getView().byId("idList");
                var oBindingItem = oLstCtrl.getBinding("items");
                var aFilters = [];
                if (sOrder) {
                    aFilters.push(new sap.ui.model.Filter("OrderNo", sap.ui.model.FilterOperator.Contains, sOrder));
                }
                oBindingItem.filter(aFilters, sap.ui.model.FilterType.Application);

                this.countTaskByLocation();
            },

            displayOrderList: function (sTaskNo) {
                this.clearMessageStrip();
                this.oRouter.navTo("TaskDetail");
            },

            showMessageStrip: function (sType, sMessage) {
                this.clearMessageStrip();

                var oMsg = new sap.m.MessageStrip("idMessage", {
                    text: sMessage,
                    showIcon: true,
                    type: sType
                });

                this.getView().addDependent(oMsg);
                this.oPage.insertContent(oMsg, 0);
            },

            clearMessageStrip: function (sType, sMessage) {
                var oMsg = sap.ui.getCore().byId("idMessage");
                if (oMsg) {
                    oMsg.destroy();
                }
            },

            focusScanner: function () {
                this.getView().byId("idBarcode").focus();
            },

            openDialogSelectWarehouse: function () {
                if (!this.oSelectWarehouseDialog) {
                    this.oSelectWarehouseDialog = sap.ui.xmlfragment("com.imove.iwm.ziwmpackprod.view.fragment.DialogSelectWarehouse", this);
                    this.getView().addDependent(this.oSelectWarehouseDialog);
                    this.oSelectWarehouseDialog.attachAfterClose(function (oEvent) {
                        this.isPopupOpening = false;
                        this.turnOnListenScan();
                    }.bind(this));
                    this.oSelectWarehouseDialog.attachBeforeOpen(function (oEvent) {
                        this.isPopupOpening = true;
                        setTimeout(function () {
                            sap.ui.getCore().byId("idSelWarehouseNo").focus();
                        }.bind(this), 500);
                    }.bind(this));
                    this.oSelectWarehouseDialog.setModel(this.mSelWhModel, "mSelWhModel");
                }

                //default value
                var oSelWh = this.mSelWhModel.getProperty("/");
                var oScrData = this.mScrModel.getProperty("/");
                oSelWh.SelWarehouse = oScrData.WarehouseNo || "";
                oSelWh.SelPackingArea = oScrData.PackingArea || "";
                this.mSelWhModel.setProperty("/", oSelWh);
                this.oSelectWarehouseDialog.open();
                this.turnOffListenScan();
            }
        });
    });

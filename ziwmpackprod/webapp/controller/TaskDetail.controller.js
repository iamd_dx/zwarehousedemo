// @ts-nocheck
sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageBox",
    'sap/m/MessageStrip'
],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, MessageBox, MessageStrip) {
        "use strict";

        return Controller.extend("com.imove.iwm.ziwmpackprod.controller.TaskDetail", {
            mScrModel: null,
            mNewRawMatModel: new sap.ui.model.json.JSONModel({}, true),
            mConfirmPackingQtyModel: new sap.ui.model.json.JSONModel({}, true),

            onInit: function () {
                this.oPage = this.getView().byId("idPage");
                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getTarget("TaskDetail").attachDisplay(this.onRouteMatched.bind(this));
            },

            onRouteMatched: function (oEvent) {
                this.initialDataModel();
            },

            onNavBack: function (oEvent) {
                var oHistory = sap.ui.core.routing.History.getInstance();
                var sPreviousHash = oHistory.getPreviousHash();
                if (sPreviousHash !== undefined || !oCrossAppNavigator.isInitialNavigation()) {
                    history.go(-1);
                } else {
                    var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                    oCrossAppNavigator.toExternal({
                        target: { shellHash: "#Shell-home" }
                    });
                }
            },

            onClickPurgeUrgent: function (oEvent) {
                this.openDialogGetNewRawMaterial();
            },

            onPressConfirmNewRawMaterial: function (oEvent) {
                var nQty = parseFloat(this.mNewRawMatModel.getProperty("/NewQuantity"));
                this.oGetNewRawMaterialDialog.close();

                if (nQty > 0) {
                    MessageBox.confirm("การแจ้งขอหยิบวัตถุดิบเพิ่มจะมีการบันทึกหักวัตถุดิบที่เหลือในระบบออก\r\nคุณต้องการดำเนินการต่อหรือไม่?", {
                        onClose: this.onConfirmPurgeUrget.bind(this),
                        title: "คำเดือน"
                    });
                }
            },

            onConfirmPurgeUrget: function (sAction) {
                if (sAction !== "OK") return;

                sap.m.MessageToast.show("ดำเนินการเสร็จสิ้น", {
                    duration: 3000,                  // default
                    closeOnBrowserNavigation: false  // default
                });

                var nQty = parseFloat(this.mNewRawMatModel.getProperty("/NewQuantity"));
                var oSelectedTask = this.mScrModel.getProperty("/SelectedTask");
                var aTaskList = this.mScrModel.getProperty("/TaskList");
                oSelectedTask.AdditionalStockQty += nQty;
                oSelectedTask.StockQtyInPackingUnit = (oSelectedTask.StockQty + oSelectedTask.AdditionalStockQty) * oSelectedTask.ConvertUnitRate;

                this.calculateRemainingStockQty();
            },

            onSelectedOrder: function (oEvent) {
                var oSource = oEvent.getSource();
                var oSelected = oSource.getSelectedItem();
                oSource.removeSelections();
                var oData = oSelected.getBindingContext("mScrModel").getObject();
                this.mScrModel.setProperty("/SelectedOrder", oData);
                this.openDialogConfirmPackingQuantity(oData);
            },

            onPressCancelNewRawMaterial: function () {
                this.oGetNewRawMaterialDialog.close();
            },

            onPressCancelPackingQuantity: function () {
                this.oConfirmPackingQtyDialog.close();
            },

            onPressConfirmPackingQuantity: function () {
                var oConfirmPackQty = this.mConfirmPackingQtyModel.getProperty("/");
                var oSelectedOrder = this.mScrModel.getProperty("/SelectedOrder");
                oSelectedOrder.ConfirmQty = oConfirmPackQty.ConfirmQty;

                this.mScrModel.refresh(true);
                this.oConfirmPackingQtyDialog.close();

                this.openWaitingBarcodePrinter();
                this.checkCompletedTask();
                this.calculateRemainingStockQty();
            },



            initialDataModel: function () {
                this.mScrModel = this.getOwnerComponent().getModel("mScrModel");
            },

            turnOnListenScan: function (id) {
                var oInput = this.getView().byId(id);
                if (!oInput) oInput = sap.ui.getCore().byId(id);
                var $input = $("input", oInput.getDomRef());
                $input.on("blur", function (oEvent) {
                    this.focusScanner(id);
                }.bind(this));

                this.focusScanner(id);
            },

            turnOffListenScan: function (id) {
                var oInput = this.getView().byId(id);
                if (!oInput) oInput = sap.ui.getCore().byId(id);
                var $input = $("input", oInput.getDomRef());
                $input.off("blur");
            },

            showMessageStrip: function (sType, sMessage) {
                this.clearMessageStrip();

                var oMsg = new sap.m.MessageStrip("idMessage", {
                    text: sMessage,
                    showIcon: true,
                    type: sType
                });

                this.getView().addDependent(oMsg);
                this.oPage.insertContent(oMsg, 0);
            },

            clearMessageStrip: function (sType, sMessage) {
                var oMsg = sap.ui.getCore().byId("idMessage");
                if (oMsg) {
                    oMsg.destroy();
                }
            },

            focusScanner: function (id) {
                var oInput = this.getView().byId(id);
                if (!oInput) oInput = sap.ui.getCore().byId(id);
                oInput.focus();
            },

            checkCompletedTask: function () {
                var oSelectedTask = this.mScrModel.getProperty("/SelectedTask");
                var bIsDone = true;

                oSelectedTask.PPOrderList.forEach(function (oItem) {
                    if (!oItem.ConfirmQty) bIsDone = false;
                });

                oSelectedTask.IsDone = bIsDone;
                this.mScrModel.refresh(true);
            },

            calculateRemainingStockQty: function () {
                var oSelectedTask = this.mScrModel.getProperty("/SelectedTask");
                var nRemaining = oSelectedTask.StockQtyInPackingUnit;

                oSelectedTask.PPOrderList.forEach(function (oItem) {
                    nRemaining -= oItem.ConfirmQty;
                });

                oSelectedTask.RemainingStockQtyInPackingUnit = nRemaining;
                this.mScrModel.refresh(true);
            },

            openWaitingBarcodePrinter: function () {
                MessageBox.alert("กรุณารอรับบาร์โค๊ด...", {
                    title: "กำลังพิมพ์บาร์โค๊ด"
                });
            },

            openDialogConfirmPackingQuantity: function (oOrderItem) {
                if (!this.oConfirmPackingQtyDialog) {
                    this.oConfirmPackingQtyDialog = sap.ui.xmlfragment("com.imove.iwm.ziwmpackprod.view.fragment.DialogConfirmPackingQuantity", this);
                    this.getView().addDependent(this.oConfirmPackingQtyDialog);
                    this.oConfirmPackingQtyDialog.attachAfterClose(function (oEvent) {
                        this.isPopupOpening = false;
                        //this.turnOffListenScan("idPackingQuantity");
                    }.bind(this));
                    this.oConfirmPackingQtyDialog.attachBeforeOpen(function (oEvent) {
                        this.isPopupOpening = true;
                        setTimeout(function () {
                            this.focusScanner("idPackingQuantity");
                            //this.turnOnListenScan("idPackingQuantity");
                        }.bind(this), 300);
                    }.bind(this));
                    this.oConfirmPackingQtyDialog.setModel(this.mConfirmPackingQtyModel, "mPackingQty");
                }

                //default value
                this.oConfirmPackingQtyDialog.open();

                var oConfirmData = jQuery.extend(true, {}, oOrderItem);
                if (oConfirmData.ConfirmQty == 0) oConfirmData.ConfirmQty = oConfirmData.ReqQty;
                this.mConfirmPackingQtyModel.setProperty("/", oConfirmData);
                this.mConfirmPackingQtyModel.refresh(true);
            },

            openDialogGetNewRawMaterial: function () {
                if (!this.oGetNewRawMaterialDialog) {
                    this.oGetNewRawMaterialDialog = sap.ui.xmlfragment("com.imove.iwm.ziwmpackprod.view.fragment.DialogGetMoreRawMaterial", this);
                    this.getView().addDependent(this.oGetNewRawMaterialDialog);
                    this.oGetNewRawMaterialDialog.attachAfterClose(function (oEvent) {
                        this.isPopupOpening = false;
                        //this.turnOffListenScan("idNewQuantity");
                    }.bind(this));
                    this.oGetNewRawMaterialDialog.attachBeforeOpen(function (oEvent) {
                        this.isPopupOpening = true;
                        setTimeout(function () {
                            this.focusScanner("idNewQuantity");
                            //this.turnOnListenScan("idNewQuantity");
                        }.bind(this), 300);
                    }.bind(this));
                    this.oGetNewRawMaterialDialog.setModel(this.mNewRawMatModel, "mNewRawMat");
                }

                //default value
                this.oGetNewRawMaterialDialog.open();

                var oNewRawMat = this.mNewRawMatModel.getProperty("/");
                var oScrData = this.mScrModel.getProperty("/SelectedTask");
                oNewRawMat.NewQuantity = 0;
                oNewRawMat.StockQtyUnit = oScrData.StockQtyUnit;
                this.mNewRawMatModel.refresh(true);
            },

            openConfirmPickup: function (newDestBin) {
                if (!this.oConfirmPickupDialog) {
                    this.oConfirmPickupDialog = sap.ui.xmlfragment("com.imove.iwm.ziwmpackprod.view.fragment.DialogConfirmPickup", this);
                    this.getView().addDependent(this.oConfirmPickupDialog);
                    this.oConfirmPickupDialog.attachAfterClose(function (oEvent) {
                        this.isPopupOpening = false;
                    }.bind(this));
                    this.oConfirmPickupDialog.attachBeforeOpen(function (oEvent) {
                        this.isPopupOpening = true;
                    }.bind(this));
                }

                this.oConfirmPickupDialog.open();
            },
        });
    });

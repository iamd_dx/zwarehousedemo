// @ts-nocheck
sap.ui.define([
        "sap/ui/core/mvc/Controller",
        "sap/m/MessageBox",
        "sap/m/MessageToast"
	],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
	function (Controller, MessageBox, MessageToast) {
		"use strict";
		return Controller.extend("com.iam.ziwmreceivestockto.controller.PalletList", {
            C_MODEL_SCREEN: "ScreenModel",

            INPUT_ID: "idPalletInput",
            
			onInit: function () {
                this.getView().addStyleClass("sapUiSizeCompact");
                this.I18N = this.getOwnerComponent().getModel("i18n").getResourceBundle();
                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getTarget("PalletList").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
                this.initModel();
            },
            initModel: function (oEvent) {
                //init share model & context
                this.shareModel = this.getOwnerComponent().getShareModel();

                this.getView().setModel(new sap.ui.model.json.JSONModel(), this.C_MODEL_SCREEN);
                this.resetScreenModel();
            },
            handleRouteMatched: function (oEvent) {
                //Stay Focus on Pallet ID input
                var $input = $("input", this.getView().byId(this.INPUT_ID).getDomRef());
                $input.on("blur", function (oEvent) {
                    this.getView().byId(this.INPUT_ID).focus();
                }.bind(this));

                //Count Pallet Amount
                var aPalletList =  this.shareModel.getProperty("/PalletList");
                var ScreenModel = this.getView().getModel(this.C_MODEL_SCREEN);
                ScreenModel.setProperty("/PalletAmt", aPalletList.length);
            },
            resetScreenModel: function () {
                this.getView().getModel(this.C_MODEL_SCREEN).setData({
                    PalletID: "",
                    PalletAmt: 0
                });
            },
            onCheckPallet: function (oEvent) {
                var ScreenModel     = this.getView().getModel(this.C_MODEL_SCREEN);
                var sPalletID       = ScreenModel.getProperty("/PalletID");
                var aPalletList     = this.shareModel.getContext("/PalletList").getObject()
                var oSelPallet      = aPalletList.find(function(oPallet) {
                    return oPallet.PalletId === sPalletID;
                }.bind(this));
                if(oSelPallet === undefined){
                    MessageToast.show(this.I18N.getText("Toast.NotFound"));
                    return;
                }
                oSelPallet.Status = true;
                aPalletList.sort(function(oPallet_A, oPallet_B) {
                    return (oPallet_A.Status === oPallet_B.Status)? 0 : oPallet_A.Status? -1 : 1;
                }.bind(this));
                this.shareModel.refresh();
                //Reset Pallet ID
                ScreenModel.setProperty("/PalletID", "");

                MessageToast.show(this.I18N.getText("Toast.Success"));
            },
            onConfirmPress: function (oEvent) {
                var sTransferID  = this.shareModel.getProperty("/TransferID");
                var aPalletList  = this.shareModel.getContext("/PalletList").getObject();
                var oFalsePallet = aPalletList.find(function(oPallet) {
                    return oPallet.Status === false;
                }.bind(this));
                if(oFalsePallet !== undefined){
                    MessageToast.show(this.I18N.getText("Toast.UnCheckPallet"));
                    return;
                }
                this._removeConfirmTO(sTransferID);
                this.shareModel.refresh();
                this.turnOffListenScan();
                MessageBox.success(this.I18N.getText("MessageBox.Success"), {
                    actions: [MessageBox.Action.OK],
                    emphasizedAction: MessageBox.Action.OK,
                    onClose: function (sAction) {
                        if(sAction === "OK"){
                            this.onNavBack();
                        }
                    }.bind(this)
                });
            },
            _removeConfirmTO: function (sTransferID) {
                var aTOList     = this.shareModel.getContext("/TOList").getObject();
                for(var i in aTOList){
                    if(aTOList[i].TransferId === sTransferID){
                        aTOList.splice(i, 1);
                        break;
                    }
                }
            },
            turnOnListenScan: function () {
                var $input = $("input", this.getView().byId(this.INPUT_ID).getDomRef());
                $input.on("blur", function (oEvent) {
                    this.focusScanner();
                }.bind(this));

                this.focusScanner();
            },
            turnOffListenScan: function () {
                var $input = $("input", this.getView().byId(this.INPUT_ID).getDomRef());
                $input.off("blur");
            },
            onNavBack: function (oEvent) {
                var oHistory = sap.ui.core.routing.History.getInstance();
                var sPreviousHash = oHistory.getPreviousHash();
                if (sPreviousHash !== undefined || !oCrossAppNavigator.isInitialNavigation()) {
                    history.go(-1);
                } else {
                    var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                    oCrossAppNavigator.toExternal({
                        target: { shellHash: "#Shell-home" }
                    });
                }
            }
		});
	});
// @ts-nocheck
sap.ui.define([
        "sap/ui/core/mvc/Controller",
        "sap/m/MessageBox",
        "sap/m/MessageToast"
	],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
	function (Controller, MessageBox, MessageToast) {
		"use strict";
		return Controller.extend("com.iam.ziwmreceivestockto.controller.TOList", {
            C_MODEL_SCREEN: "ScreenModel",
            C_MODEL_MOCK: "MockModel",      //Not Use
            C_MODEL_LIST: "ListModel",      //From Mock Data Directly

            INPUT_ID: "idTransferInput",
            shareModel: null,

			onInit: function () {
                this.getView().addStyleClass("sapUiSizeCompact");
                this.I18N = this.getOwnerComponent().getModel("i18n").getResourceBundle();
                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getTarget("TOList").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
                this.initModel();
            },
            initModel: function () {
                //init share model & context
                this.shareModel = this.getOwnerComponent().getShareModel();

                this.getView().setModel(new sap.ui.model.json.JSONModel(), this.C_MODEL_SCREEN);
                this.getView().setModel(new sap.ui.model.json.JSONModel(), this.C_MODEL_MOCK);
                this.getView().setModel(new sap.ui.model.json.JSONModel(), this.C_MODEL_LIST);
                this.resetScreenModel();
                this.resetMockModel();
                this.resetListModel();
            },
            handleRouteMatched: function (oEvent) {
                //Stay Focus on Pallet ID input
                var $input = $("input", this.getView().byId(this.INPUT_ID).getDomRef());
                $input.on("blur", function (oEvent) {
                    this.getView().byId(this.INPUT_ID).focus();
                }.bind(this));

                //Count TO Amount
                var aTOList =  this.shareModel.getProperty("/TOList");
                var ScreenModel = this.getView().getModel(this.C_MODEL_SCREEN);
                ScreenModel.setProperty("/TransferAmt", aTOList.length);
            },
            resetScreenModel: function () {
                this.getView().getModel(this.C_MODEL_SCREEN).setData({
                    TransferID: "",
                    TransferAmt: 0,
                    WarehouseNo: "",
                    WarehouseNoDialog: ""
                });
            },
            resetMockModel: function () {
                var MockModel = this.getView().getModel(this.C_MODEL_MOCK);
			    MockModel.loadData(jQuery.sap.getModulePath("com.iam.ziwmreceivestockto", "/MockData/TranOrder.json"), {}, false);
            },
            resetListModel: function () {
                var ListModel = this.getView().getModel(this.C_MODEL_LIST);
			    ListModel.loadData(jQuery.sap.getModulePath("com.iam.ziwmreceivestockto", "/MockData/TranOrder.json"), {}, false);
                this.shareModel.setProperty("/TOList", ListModel.getData());
            },
            onListItemPress: function (oEvent) {
                var sPath           = oEvent.getSource().getBindingContextPath()
                var oSelTO          = this.shareModel.getContext(sPath).getObject();
                var ScreenModel     = this.getView().getModel(this.C_MODEL_SCREEN);
                ScreenModel.setProperty("/TransferID", oSelTO.TransferId);
                this.onCheckTO();
            },
            onCheckTO: function (oEvent) {
                var ScreenModel = this.getView().getModel(this.C_MODEL_SCREEN);
                var sTransferID = ScreenModel.getProperty("/TransferID");
                var aTOList     = this.shareModel.getContext("/TOList").getObject()
                var oSelTO = aTOList.find(function(oTO) {
                    return oTO.TransferId === sTransferID;
                }.bind(this));
                this.shareModel.setProperty("/PalletList", oSelTO.PalletList);
                var sWarehouseNo = ScreenModel.getProperty("/WarehouseNo");
                this.shareModel.setProperty("/SelWarehouse", sWarehouseNo);
                this.shareModel.setProperty("/TransferID", sTransferID);
                ScreenModel.setProperty("/TransferID", "");

                if(sWarehouseNo !== ""){
                    this.oRouter.navTo("PalletList");
                }else{
                    MessageBox.error(this.I18N.getText("MessageBox.Error"));
                }
            },
            onPressEditSelWarehouse: function (oEvent) {
                this.openDialogSelectWarehouse();
            },
            openDialogSelectWarehouse: function () {
                if (!this.oSelectWarehouseDialog) {
                    this.oSelectWarehouseDialog = sap.ui.xmlfragment("com.iam.ziwmreceivestockto.view.fragment.DialogSelectWarehouse", this);
                    this.getView().addDependent(this.oSelectWarehouseDialog);
                    this.oSelectWarehouseDialog.attachAfterClose(function (oEvent) {
                        this.isPopupOpening = false;
                        this.turnOnListenScan();
                    }.bind(this));
                    this.oSelectWarehouseDialog.attachBeforeOpen(function (oEvent) {
                        this.isPopupOpening = true;
                        setTimeout(function () {
                            sap.ui.getCore().byId("idSelWarehouseNo").focus();
                        }.bind(this), 500);
                    }.bind(this));
                }
                
                var ScreenModel = this.getView().getModel(this.C_MODEL_SCREEN);
                var sWarehouseNo = ScreenModel.getProperty("/WarehouseNo");
                ScreenModel.setProperty("/WarehouseNoDialog", sWarehouseNo);
                this.oSelectWarehouseDialog.open();
                this.turnOffListenScan();
            },
            turnOnListenScan: function () {
                var $input = $("input", this.getView().byId(this.INPUT_ID).getDomRef());
                $input.on("blur", function (oEvent) {
                    this.focusScanner();
                }.bind(this));

                this.focusScanner();
            },
            turnOffListenScan: function () {
                var $input = $("input", this.getView().byId(this.INPUT_ID).getDomRef());
                $input.off("blur");
            },
            focusScanner: function () {
                this.getView().byId(this.INPUT_ID).focus();
            },
            onPressCancelSelectWarehouse: function (oEvent) {
                this.oSelectWarehouseDialog.close();
            },
            onPressOKSelectWarehouse: function () {
                var ScreenModel = this.getView().getModel(this.C_MODEL_SCREEN);
                var sWarehouseNoDialog = ScreenModel.getProperty("/WarehouseNoDialog");
                ScreenModel.setProperty("/WarehouseNo", sWarehouseNoDialog);
                this.oSelectWarehouseDialog.close();
            },
            onNavBack: function () {
                var oHistory = sap.ui.core.routing.History.getInstance();
                var sPreviousHash = oHistory.getPreviousHash();
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                if (sPreviousHash !== undefined || !oCrossAppNavigator.isInitialNavigation()) {
                    history.go(-1);
                } else {
                    oCrossAppNavigator.toExternal({
                        target: { shellHash: "#Shell-home" }
                    });
                }
            }
		});
	});
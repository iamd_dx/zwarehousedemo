/*global QUnit*/

sap.ui.define([
	"com/iam/ziwmpickinglistforpp/controller/SelectPP.controller"
], function (Controller) {
	"use strict";

	QUnit.module("SelectPP Controller");

	QUnit.test("I should test the SelectPP controller", function (assert) {
		var oAppController = new Controller();
		oAppController.onInit();
		assert.ok(oAppController);
	});

});

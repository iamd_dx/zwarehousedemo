// @ts-nocheck
sap.ui.define([
        "sap/ui/core/mvc/Controller",
        "sap/m/MessageBox",
        "sap/m/MessageToast"
	],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
	function (Controller, MessageBox, MessageToast) {
		"use strict";
		return Controller.extend("com.iam.ziwmpickinglistforpp.controller.PalletPick", {
            C_MODEL_SCREEN: "ScreenModel",
            C_MODEL_MOCK: "MockModel",  //MatPallet.json
            C_MODEL_LIST: "ListModel",

            INPUT_ID: "idPalletInput",
            shareModel: null,
            firstPageShareModel: null,
            prevPageShareModel: null,
            PickAmtDialog: null,
            PackAreaDialog: null,

			onInit: function () {
                this.getView().addStyleClass("sapUiSizeCompact");
                this.I18N = this.getOwnerComponent().getModel("i18n").getResourceBundle();
                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getTarget("PalletPick").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
                this.initModel();
            },
            initModel: function (oEvent) {
                //init share model & context
                this.shareModel = this.getOwnerComponent().getShareModel();
                this.firstPageShareModel = this.shareModel.getContext("/PagePPList");
                this.prevPageShareModel = this.shareModel.getContext("/PageSummaryPickList");
                // debugger;

                this.getView().setModel(new sap.ui.model.json.JSONModel(), this.C_MODEL_SCREEN);
                this.getView().setModel(new sap.ui.model.json.JSONModel(), this.C_MODEL_MOCK);
                this.getView().setModel(new sap.ui.model.json.JSONModel(), this.C_MODEL_LIST);
                this.resetScreenModel();
                this.resetMockModel();
                this.resetListModel();
            },
            handleRouteMatched: function (oEvent) {
                //Stay Focus on Pallet ID input
                var $input = $("input", this.getView().byId(this.INPUT_ID).getDomRef());
                $input.on("blur", function (oEvent) {
                    this.getView().byId(this.INPUT_ID).focus();
                }.bind(this));

                var aMatPalletList = this.MatPalletList();
                this.getView().getModel(this.C_MODEL_LIST).setData(aMatPalletList);
            },
            MatPalletList: function () {
                var oShereModel = this.prevPageShareModel.getObject();
                var sMatID      = oShereModel.SelMat.MatId;
                var nReqQty     = oShereModel.SelMat.ReqQty;
                var aMockModel = this.getView().getModel(this.C_MODEL_MOCK).getData();

                var oMatPallet  = aMockModel.find(function(oPallet) {
                    return oPallet.MatId === sMatID;
                }.bind(this));

                var aMatPalletList = [];
                aMatPalletList = this.AddPalletList(aMatPalletList, oMatPallet, nReqQty);

                this._setScreenModelfromShare(oShereModel, oMatPallet);
                return aMatPalletList;
            },
            _setScreenModelfromShare: function (oShereModel, oMatPallet) {
                var nReqQty     = oShereModel.SelMat.ReqQty;
                var nGetQty     = oShereModel.SelMat.GetQty;
                var sBaseUnit   = oShereModel.SelMat.BaseUnit;

                var ScreenModel = this.getView().getModel(this.C_MODEL_SCREEN);
                ScreenModel.setProperty("/WarehouseNo", this.firstPageShareModel.getObject().SelWarehouse);
                ScreenModel.setProperty("/MatId", oMatPallet.MatId);
                ScreenModel.setProperty("/MatDesc", oMatPallet.MatDesc);
                ScreenModel.setProperty("/ReqQty", nReqQty);
                ScreenModel.setProperty("/GetQty", nGetQty);
                ScreenModel.setProperty("/BaseUnit", sBaseUnit);
            },
            AddPalletList: function (aMatPalletList, oMatPallet, nTotalReqQty) {
                var nTotalGetQty = 0;
                for(var i in oMatPallet.PalletList){
                    var oItem = jQuery.extend(true, {}, oMatPallet.PalletList[i]);
                    if(nTotalGetQty < nTotalReqQty){    //(nTotalReqQty - nTotalGetQty) Alway > 0
                        //No Logic to sort oItem before add
                        oItem.GetQty = 0;
                        oItem.ReqQty = this._GetReqQty(nTotalGetQty, nTotalReqQty, parseInt(oItem.Qty, 10));
                        oItem.Status = false;
                        aMatPalletList.push(oItem);
                        nTotalGetQty = parseInt(nTotalGetQty) + parseInt(oItem.ReqQty);
                    }else{
                        break;
                    }
                }
                //Check Not enough Mat
                if(nTotalGetQty < nTotalReqQty){
                    this._notEnoughMat();
                }
                return aMatPalletList;
            },
            _GetReqQty: function (nTotalGetQty, nTotalReqQty, nQty) {
                var nReqQty = nTotalReqQty - nTotalGetQty;
                if(nReqQty > nQty){
                    nReqQty = nQty;
                }
                return nReqQty;
            },
            _notEnoughMat: function () {
                MessageBox.error(this.I18N.getText("MessageBox.NotEnoughMat"), {
                    actions: [MessageBox.Action.CLOSE],
                    onClose: function (sAction) {
                        if(sAction === "CLOSE"){
                            this.onNavBack();
                        }
                    }.bind(this)
                });
            },
            resetScreenModel: function () {
                this.getView().getModel(this.C_MODEL_SCREEN).setData({
                    WarehouseNo: "",
                    PalletNo: "",
                    MatId: "",
                    MatDesc: "",
                    ReqQty: 0,
                    GetQty: 0,
                    BaseUnit: "",
                    DialogQty: 0,
                    DialogArea: ""
                });
            },
            resetMockModel: function () {
                var MockModel = this.getView().getModel(this.C_MODEL_MOCK);
			    MockModel.loadData(jQuery.sap.getModulePath("com.iam.ziwmpickinglistforpp", "/MockData/MatPallet.json"), {}, false);
            },
            resetListModel: function () {
                this.getView().getModel(this.C_MODEL_LIST).setData(
                    []
                );
            },
            onScanPallet: function (oEvent) {
                var PickAmtDialog = this._getPickAmtDialog();
                var ScreenModel = this.getView().getModel(this.C_MODEL_SCREEN);
                var sPalletNo = ScreenModel.getProperty("/PalletNo");
                var oSelPallet = jQuery.extend(true, {}, this._getPalletFromList(sPalletNo));
                if(jQuery.isEmptyObject(oSelPallet)){
                    MessageBox.error(this.I18N.getText("MessageBox.PalletNotFound"));
                    return;
                }
                var nReqQty = oSelPallet.ReqQty;
                ScreenModel.setProperty("/DialogQty", nReqQty);
                PickAmtDialog.open();
                this.turnOffListenScan();
            },
            _getPickAmtDialog: function () {
                if (!this.PickAmtDialog) {
                    this.PickAmtDialog = sap.ui.xmlfragment("com.iam.ziwmpickinglistforpp.view.fragment.DialogPickAmt", this);
                    this.getView().addDependent(this.PickAmtDialog);
                    this.PickAmtDialog.attachAfterClose(function (oEvent) {
                        this.isPopupOpening = false;
                        this.turnOnListenScan();
                    }.bind(this));
                    this.PickAmtDialog.attachBeforeOpen(function (oEvent) {
                        this.isPopupOpening = true;
                        setTimeout(function () {
                            sap.ui.getCore().byId("idPickAmt").focus();
                        }.bind(this), 500);
                    }.bind(this));
                }
                return this.PickAmtDialog;
            },
            _getPalletFromList: function (sPalletNo) {
                var aListModel = this.getView().getModel(this.C_MODEL_LIST).getData();
                var oSelPallet  = aListModel.find(function(oPalletList) {
                    return oPalletList.PalletId === sPalletNo;
                }.bind(this));
                // var nReqQty = oSelPallet.ReqQty;
                return oSelPallet;
            },
            turnOnListenScan: function () {
                var $input = $("input", this.getView().byId(this.INPUT_ID).getDomRef());
                $input.on("blur", function (oEvent) {
                    this.focusScanner();
                }.bind(this));

                this.focusScanner();
            },
            turnOffListenScan: function () {
                var $input = $("input", this.getView().byId(this.INPUT_ID).getDomRef());
                $input.off("blur");
            },
            focusScanner: function () {
                this.getView().byId(this.INPUT_ID).focus();
            },
            onPressCancelPickAmt: function (oEvent) {
                this.PickAmtDialog.close();
            },
            onPressOKPickAmt: function () {
                var ScreenModel     = this.getView().getModel(this.C_MODEL_SCREEN);
                var ListModel       = this.getView().getModel(this.C_MODEL_LIST);
                var nDialogQty      = ScreenModel.getProperty("/DialogQty");
                var nScrGetQty      = ScreenModel.getProperty("/GetQty");
                var sPalletNo       = ScreenModel.getProperty("/PalletNo");
                var oSelPallet      = this._getPalletFromList(sPalletNo);
                oSelPallet.GetQty   = nDialogQty;
                if(nDialogQty >= oSelPallet.ReqQty){
                    oSelPallet.Status   = true;
                }else{
                    oSelPallet.Status   = false;
                }
                ScreenModel.setProperty("/GetQty", parseInt(nScrGetQty) + parseInt(nDialogQty));
                //Reset Pallet ID
                ScreenModel.setProperty("/PalletNo", "");
                this._updateShareSelMat();
                ListModel.refresh();
                this.PickAmtDialog.close();
            },
            _updateShareSelMat: function () {
                var oShereModel     = this.prevPageShareModel.getObject();
                var ScreenModel     = this.getView().getModel(this.C_MODEL_SCREEN);
                var nScrGetQty      = ScreenModel.getProperty("/GetQty");
                var nScrReqQty      = ScreenModel.getProperty("/ReqQty");

                oShereModel.SelMat.GetQty = nScrGetQty;
                if(nScrGetQty >= nScrReqQty){
                    oShereModel.SelMat.Status   = true;
                }else{
                    oShereModel.SelMat.Status   = false;
                }
            },
            onSubmitPress: function () {
                var PackAreaDialog = this._getPackAreaDialog();
                var ScreenModel    = this.getView().getModel(this.C_MODEL_SCREEN);
                // var aListModel     = this.getView().getModel(this.C_MODEL_LIST).getData();
                var checkGetQty    = this._checkGetQtyFull()
                if(checkGetQty){
                    PackAreaDialog.open();
                    this.turnOffListenScan();
                }else{
                    MessageBox.warning(this.I18N.getText("MessageBox.Warning"), {
                        actions: [MessageBox.Action.OK, MessageBox.Action.CANCEL],
                        emphasizedAction: MessageBox.Action.OK,
                        onClose: function (sAction) {
                            if(sAction === "OK"){
                                PackAreaDialog.open();
                                this.turnOffListenScan();
                            }
                        }.bind(this)
                    });
                }
            },
            _checkGetQtyFull: function () {
                var aListModel  = this.getView().getModel(this.C_MODEL_LIST).getData();
                var result      = true;
                for(var i in aListModel){
                    if(aListModel[i].Status === false){
                        result  = false;
                        break;
                    }
                }
                return result;
            },
            _getPackAreaDialog: function () {
                if (!this.PackAreaDialog) {
                    this.PackAreaDialog = sap.ui.xmlfragment("com.iam.ziwmpickinglistforpp.view.fragment.DialogConfirmPackArea", this);
                    this.getView().addDependent(this.PackAreaDialog);
                    this.PackAreaDialog.attachAfterClose(function (oEvent) {
                        this.isPopupOpening = false;
                        this.turnOnListenScan();
                    }.bind(this));
                    this.PackAreaDialog.attachBeforeOpen(function (oEvent) {
                        this.isPopupOpening = true;
                        setTimeout(function () {
                            sap.ui.getCore().byId("idPackArea").focus();
                        }.bind(this), 500);
                    }.bind(this));
                }
                return this.PackAreaDialog;
            },
            onPressCancelPackArea: function (oEvent) {
                this.PackAreaDialog.close();
            },
            onPressOKPackArea: function () {
                var oShereModel = this.prevPageShareModel.getObject();
                var ScreenModel = this.getView().getModel(this.C_MODEL_SCREEN);
                var aMatList    = oShereModel.MatList;
                // debugger;
                var IsListComplete = this._checkListComplete(aMatList);

                this.PackAreaDialog.close();
                //Reset Pallet ID
                ScreenModel.setProperty("/DialogArea", "");
                
                if(IsListComplete){
                    MessageBox.success(this.I18N.getText("MessageBox.Success"), {
                        actions: [MessageBox.Action.OK],
                        emphasizedAction: MessageBox.Action.OK,
                        onClose: function (sAction) {
                            if(sAction === "OK"){
                                this.getOwnerComponent().initShareModel();
                                this.oRouter.navTo("");
                            }
                        }.bind(this)
                    });
                }else{
                    this.oRouter.navTo("SummaryPick");
                }
            },
            _checkListComplete: function (aMatList) {
                var result = true;
                var oUnFinishList  = aMatList.find(function(oPalletList) {
                    return oPalletList.Status === false;
                }.bind(this));
                if(!(jQuery.isEmptyObject(oUnFinishList))){
                    result = false;
                }
                return result;
            },
            onNavBack: function (oEvent) {
                var oHistory = sap.ui.core.routing.History.getInstance();
                var sPreviousHash = oHistory.getPreviousHash();
                if (sPreviousHash !== undefined || !oCrossAppNavigator.isInitialNavigation()) {
                    history.go(-1);
                } else {
                    var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                    oCrossAppNavigator.toExternal({
                        target: { shellHash: "#Shell-home" }
                    });
                }
            }
		});
	});
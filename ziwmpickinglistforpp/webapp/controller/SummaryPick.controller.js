// @ts-nocheck
sap.ui.define([
        "sap/ui/core/mvc/Controller",
        "sap/m/MessageBox",
        "sap/m/MessageToast"
	],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
	function (Controller, MessageBox, MessageToast) {
		"use strict";
		return Controller.extend("com.iam.ziwmpickinglistforpp.controller.SummaryPick", {
            C_MODEL_SCREEN: "ScreenModel",
            C_MODEL_MOCK: "MockModel",  //MatConversion.json
            C_MODEL_LIST: "ListModel",

            shareModel: null,
            prevPageShareModel: null,
            pageShareModel: null,

			onInit: function () {
                this.getView().addStyleClass("sapUiSizeCompact");
                this.I18N = this.getOwnerComponent().getModel("i18n").getResourceBundle();
                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getTarget("SummaryPick").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
                this.initModel();
            },
            initModel: function (oEvent) {
                //init share model & context
                this.shareModel = this.getOwnerComponent().getShareModel();
                this.prevPageShareModel = this.shareModel.getContext("/PagePPList");
                this.pageShareModel = this.shareModel.getContext("/PageSummaryPickList");
                // debugger;

                this.getView().setModel(new sap.ui.model.json.JSONModel(), this.C_MODEL_SCREEN);
                this.getView().setModel(new sap.ui.model.json.JSONModel(), this.C_MODEL_MOCK);
                this.getView().setModel(new sap.ui.model.json.JSONModel(), this.C_MODEL_LIST);
                this.resetScreenModel();
                this.resetMockModel();
                this.resetListModel();
            },
            handleRouteMatched: function (oEvent) {
                // debugger;
                var oSelMat = this.pageShareModel.getObject().SelMat;
                var aMatList = this.pageShareModel.getObject().MatList;
                if(jQuery.isEmptyObject(oSelMat)){
                    var aSummaryReqMatList = this.SummaryReqMatList();
                    aMatList = this.ConversionMatList(aSummaryReqMatList);
                }
                this.getView().getModel(this.C_MODEL_LIST).setData(aMatList);
            },
            SummaryReqMatList: function () {
                var oShereModel = this.prevPageShareModel.getObject();
                var aPPList = oShereModel.PPList;
                var aSumReqMat = [];
                for(var i in aPPList){
                    aSumReqMat = this.AddReqMat(aSumReqMat, aPPList[i].ReqMatList);
                }
                var ScreenModel = this.getView().getModel(this.C_MODEL_SCREEN);
                ScreenModel.setProperty("/WarehouseNo", oShereModel.SelWarehouse);
                ScreenModel.setProperty("/MatAmt", aSumReqMat.length);
                return aSumReqMat;
            },
            AddReqMat: function (aSumReqMat, aReqMatList) {
                for(var i in aReqMatList){
                    if(aReqMatList[i].MatType === "RAW"){
                        var oItem = jQuery.extend(true, {}, aReqMatList[i]);
                        var aExitingMat = aSumReqMat.filter(function(oMat) {
                            return oMat.MatId === oItem.MatId;
                        }.bind(this));
                        if (aExitingMat.length > 0) {
                            aExitingMat[0].Qty = parseInt(aExitingMat[0].Qty, 10) + parseInt(oItem.Qty, 10);
                        }else{
                            aSumReqMat.push(oItem);
                        }
                    }
                }
                return aSumReqMat;
            },
            ConversionMatList: function (aMatList) {
                var aMockModel = this.getView().getModel(this.C_MODEL_MOCK).getData();
                for(var i in aMatList){
                    var oItem = aMatList[i];
                    var oConversion = aMockModel.find(function(oMat) {
                        return oMat.MatId === oItem.MatId;
                    }.bind(this));
                    if (oConversion !== undefined) {
                        var nRate       = this.getConvRate(oConversion.ConversionList, oItem);
                        oItem.BaseUnit  = oConversion.BaseUnit;
                        oItem.ConRate   = nRate;
                        oItem.GetQty    = 0.00;
                        oItem.ReqQty    = Math.ceil(oItem.Qty/nRate);
                        // oItem.ReqQty    = parseFloat(oItem.ReqQty.toFixed(2));
                        oItem.Status    = false;
                    }else{
                        oItem.BaseUnit  = null;
                        oItem.ConRate   = null;
                        oItem.GetQty    = 0.00;
                        oItem.ReqQty    = 0.00;
                    }
                }
                return aMatList;
            },
            getConvRate: function (aConversionList, oItem) {
                var nRate = 1;
                var oRateDetail = aConversionList.find(function(oConv) {
                    return oConv.Unit === oItem.Unit;
                }.bind(this));
                if (oRateDetail !== undefined) {
                    nRate = parseInt(oRateDetail.Rate, 10);
                }
                return nRate;
            },
            resetScreenModel: function () {
                this.getView().getModel(this.C_MODEL_SCREEN).setData({
                    MatAmt: 0,
                    WarehouseNo: ""
                });
            },
            resetMockModel: function () {
                var MockModel = this.getView().getModel(this.C_MODEL_MOCK);
			    MockModel.loadData(jQuery.sap.getModulePath("com.iam.ziwmpickinglistforpp", "/MockData/MatConversion.json"), {}, false);
            },
            resetListModel: function () {
                this.getView().getModel(this.C_MODEL_LIST).setData(
                    []
                );
            },
            onListItemPress: function (oEvent) {
                var ScreenModel     = this.getView().getModel(this.C_MODEL_SCREEN);
                var sPath           = oEvent.getSource().getBindingContextPath()
                var ListModel       = this.getView().getModel(this.C_MODEL_LIST);
                var aListModel      = ListModel.getData();
                var oSelMat         = ListModel.getProperty(sPath);

                var oShareModel = this.pageShareModel.getObject();
                oShareModel.SelMat = oSelMat;
                oShareModel.MatList = aListModel;
                
                this.oRouter.navTo("PalletPick");
            },
            onNavBack: function (oEvent) {
                var oHistory = sap.ui.core.routing.History.getInstance();
                var sPreviousHash = oHistory.getPreviousHash();
                if (sPreviousHash !== undefined || !oCrossAppNavigator.isInitialNavigation()) {
                    history.go(-1);
                } else {
                    var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                    oCrossAppNavigator.toExternal({
                        target: { shellHash: "#Shell-home" }
                    });
                }
            }
		});
	});
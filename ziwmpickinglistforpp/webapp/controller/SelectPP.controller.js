// @ts-nocheck
sap.ui.define([
        "sap/ui/core/mvc/Controller",
        "sap/m/MessageBox",
        "sap/m/MessageToast"
	],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
	function (Controller, MessageBox, MessageToast) {
		"use strict";
		return Controller.extend("com.iam.ziwmpickinglistforpp.controller.SelectPP", {
            C_MODEL_SCREEN: "ScreenModel",
            C_MODEL_MOCK: "MockModel",  //PPOrder.json
            C_MODEL_LIST: "ListModel",
            
            INPUT_ID: "idPPInput",
            shareModel: null,
            pageShareModel: null,

			onInit: function () {
                this.getView().addStyleClass("sapUiSizeCompact");
                this.I18N = this.getOwnerComponent().getModel("i18n").getResourceBundle();
                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getTarget("SelectPP").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
                this.initModel();
            },
            initModel: function (oEvent) {
                //init share model & context
                // this.shareModel = this.getOwnerComponent().getShareModel();
                // this.pageShareModel = this.shareModel.getContext("/PagePPList");

                this.getView().setModel(new sap.ui.model.json.JSONModel(), this.C_MODEL_SCREEN);
                this.getView().setModel(new sap.ui.model.json.JSONModel(), this.C_MODEL_MOCK);
                this.getView().setModel(new sap.ui.model.json.JSONModel(), this.C_MODEL_LIST);
                this.resetScreenModel();
                this.resetMockModel();
                this.resetListModel();
            },
            handleRouteMatched: function (oEvent) {
                //Stay Focus on PP order input
                var $input = $("input", this.getView().byId(this.INPUT_ID).getDomRef());
                $input.on("blur", function (oEvent) {
                    this.getView().byId(this.INPUT_ID).focus();
                }.bind(this));

                //init share model & context
                this.shareModel = this.getOwnerComponent().getShareModel();
                this.pageShareModel = this.shareModel.getContext("/PagePPList");

                var oShareModel = this.pageShareModel.getObject();
                var ListModel = this.getView().getModel(this.C_MODEL_LIST);
                ListModel.setData(oShareModel.PPList);
                //Count Pallet Amount
                var ScreenModel = this.getView().getModel(this.C_MODEL_SCREEN);
                ScreenModel.setProperty("/PPAmt", oShareModel.PPList.length);
            },
            resetScreenModel: function () {
                this.getView().getModel(this.C_MODEL_SCREEN).setData({
                    PPNo: "",
                    PPAmt: 0,
                    WarehouseNo: "",
                    WarehouseNoDialog: ""
                });
            },
            resetMockModel: function () {
                var MockModel = this.getView().getModel(this.C_MODEL_MOCK);
			    MockModel.loadData(jQuery.sap.getModulePath("com.iam.ziwmpickinglistforpp", "/MockData/PPOrder.json"), {}, false);
            },
            resetListModel: function () {
                this.getView().getModel(this.C_MODEL_LIST).setData(
                    []
                );
            },
            onCheckPP: function (oEvent) {
                var ScreenModel = this.getView().getModel(this.C_MODEL_SCREEN);
                var aMockModel = this.getView().getModel(this.C_MODEL_MOCK).getData();
                var ListModel = this.getView().getModel(this.C_MODEL_LIST);
                var aListModel = ListModel.getData();
                // debugger;
                var sPPNo = ScreenModel.getProperty("/PPNo");
                var BeforeAddLen = aListModel.length;
                
                //Check Duplicate
                for(var i in aListModel){
                    if(aListModel[i].PPNo === sPPNo){
                        MessageToast.show(this.I18N.getText("Toast.Duplicate"));
                        return;
                    }
                }

                //Add Pallet
                for(var i in aMockModel){
                    if(aMockModel[i].PPNo === sPPNo){
                        aListModel.push(aMockModel[i]);
                        ListModel.refresh();
                        MessageToast.show(this.I18N.getText("Toast.AddSuccess"));
                        break;
                    }
                }

                //Check Not Found
                if(BeforeAddLen === aListModel.length){
                    MessageToast.show(this.I18N.getText("Toast.NotFound"));
                }
                //Reset Pallet ID
                ScreenModel.setProperty("/PPNo", "");
                //Count Pallet Amount
                ScreenModel.setProperty("/PPAmt", aListModel.length);
            },
            onPressEditSelWarehouse: function (oEvent) {
                this.openDialogSelectWarehouse();
            },
            openDialogSelectWarehouse: function () {
                if (!this.oSelectWarehouseDialog) {
                    this.oSelectWarehouseDialog = sap.ui.xmlfragment("com.iam.ziwmpickinglistforpp.view.fragment.DialogSelectWarehouse", this);
                    this.getView().addDependent(this.oSelectWarehouseDialog);
                    this.oSelectWarehouseDialog.attachAfterClose(function (oEvent) {
                        this.isPopupOpening = false;
                        this.turnOnListenScan();
                    }.bind(this));
                    this.oSelectWarehouseDialog.attachBeforeOpen(function (oEvent) {
                        this.isPopupOpening = true;
                        setTimeout(function () {
                            sap.ui.getCore().byId("idSelWarehouseNo").focus();
                        }.bind(this), 500);
                    }.bind(this));
                }
                
                var ScreenModel = this.getView().getModel(this.C_MODEL_SCREEN);
                var sWarehouseNo = ScreenModel.getProperty("/WarehouseNo");
                ScreenModel.setProperty("/WarehouseNoDialog", sWarehouseNo);
                this.oSelectWarehouseDialog.open();
                this.turnOffListenScan();
            },
            turnOnListenScan: function () {
                var $input = $("input", this.getView().byId(this.INPUT_ID).getDomRef());
                $input.on("blur", function (oEvent) {
                    this.focusScanner();
                }.bind(this));

                this.focusScanner();
            },
            turnOffListenScan: function () {
                var $input = $("input", this.getView().byId(this.INPUT_ID).getDomRef());
                $input.off("blur");
            },
            focusScanner: function () {
                this.getView().byId(this.INPUT_ID).focus();
            },
            onPressCancelSelectWarehouse: function (oEvent) {
                this.oSelectWarehouseDialog.close();
            },
            onPressOKSelectWarehouse: function () {
                var ScreenModel = this.getView().getModel(this.C_MODEL_SCREEN);
                var sWarehouseNoDialog = ScreenModel.getProperty("/WarehouseNoDialog");
                ScreenModel.setProperty("/WarehouseNo", sWarehouseNoDialog);
                this.oSelectWarehouseDialog.close();
            },
            onDeleteList: function (oEvent) {
                var ScreenModel = this.getView().getModel(this.C_MODEL_SCREEN);
                var ListModel = this.getView().getModel(this.C_MODEL_LIST);
                var aListModel = ListModel.getData();

                // debugger;

                var loItem  = oEvent.getParameter("listItem");
                var sPath   = loItem.getBindingContext("ListModel").getPath();
                var lvIndex = sPath.split("/")[sPath.split("/").length - 1];
                
                aListModel.splice(lvIndex, 1);
                //Count Pallet Amount
                ScreenModel.setProperty("/PPAmt", aListModel.length);
                ListModel.refresh();
            },
            onNextPress: function (oEvent) {
                var ScreenModel = this.getView().getModel(this.C_MODEL_SCREEN);
                var sWarehouseNo = ScreenModel.getProperty("/WarehouseNo");
                var ListModel = this.getView().getModel(this.C_MODEL_LIST);
                var aListModel = ListModel.getData();

                var oShareModel = this.pageShareModel.getObject();
                oShareModel.SelWarehouse = sWarehouseNo;
                oShareModel.PPList = aListModel;

                if(sWarehouseNo !== "" && aListModel.length > 0){
                    this.oRouter.navTo("SummaryPick");
                }else{
                    MessageBox.error(this.I18N.getText("MessageBox.Error"));
                }
            },
            onNavBack: function () {
                var oHistory = sap.ui.core.routing.History.getInstance();
                var sPreviousHash = oHistory.getPreviousHash();
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                if (sPreviousHash !== undefined || !oCrossAppNavigator.isInitialNavigation()) {
                    history.go(-1);
                } else {
                    oCrossAppNavigator.toExternal({
                        target: { shellHash: "#Shell-home" }
                    });
                }
            }
		});
	});